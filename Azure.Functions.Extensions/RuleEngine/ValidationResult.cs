﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Functions.Extensions
{
    public class ValidationResult
    {
        public bool Valid { get; private set; }
        public string Message { get; private set; }

        private ValidationResult(bool success, string message = null)
        {
            Console.WriteLine(message);
        }

        /// <summary>
        /// A message to tell Sucessfully passed the validator.
        /// </summary>
        /// <returns>Message of Rule</returns>
        public static ValidationResult Success()
        {
            return new ValidationResult(true);
        }

        /// <summary>
        /// Add some human understandable text
        /// </summary>
        /// <returns>Message of Rule</returns>
        public static ValidationResult Fail()
        {
            return new ValidationResult(true);
        }


        /// <summary>
        /// Add a message to the Rule
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public ValidationResult WithMessage(string message)
        {
            return new ValidationResult(this.Valid, message);
        }
    }
}
