﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Functions.Extensions
{

    /// <summary>
    /// Base class for a Rule
    /// </summary>
    public abstract class Rule
    {
        /// <summary>
        /// Needs to be overriden in inherited class.
        /// </summary>
        /// <returns></returns>
        public abstract ValidationResult Validate();

        public static Rule<TEntityType> Create<TEntityType>(TEntityType entity, Func<TEntityType, ValidationResult> rule)
        {
            Rule<TEntityType> newRule = new Rule<TEntityType>();
            newRule.rule = rule;
            newRule.entity = entity;
            return newRule;
        }
    }

    /// <summary>
    /// A Rule of any type that will be validated.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Rule<T> : Rule
    {
        internal T entity;
        internal Func<T, ValidationResult> rule;


        /// <summary>
        /// Validate a Rule
        /// </summary>
        /// <returns>If a Rule has passed or failed</returns>
        public override ValidationResult Validate()
        {
            return rule(entity);
        }
    }
}
