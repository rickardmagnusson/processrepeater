﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Functions.Extensions
{
    public class ValidationContext
    {
        private List<Rule> rules = new List<Rule>();


        /// <summary>
        /// Pass these Rules to the validator.
        /// </summary>
        /// <param name="rules">A list of rules to pass into the validator.</param>
        public ValidationContext(params Rule[] rules)
        {
            this.rules = rules.ToList();
        }


        /// <summary>
        /// Get the result for of all Rules provided.  
        /// </summary>
        /// <returns></returns>
        public List<ValidationResult> GetResults()
        {
            var results = new List<ValidationResult>();
            foreach (var rule in rules)
            {
                results.Add(rule.Validate());
            }
            return results;
        }


        /// <summary>
        /// Add a new Rule to collection
        /// </summary>
        /// <param name="r"></param>
        public void AddRule(Rule r)
        {
            this.rules.Add(r);
        }
    }

}
