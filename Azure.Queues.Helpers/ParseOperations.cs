﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Azure.Queues.Helpers.Models;
using Microsoft.ServiceBus.Messaging;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using Microsoft.ServiceBus;
using System.Configuration;

namespace Azure.Queues.Helpers
{
    public static class ParseOperations
    {
        /// <summary>
        /// Parse the incomming Json to a InboxMessage.
        /// Internally converts the Message param to an Xml
        /// </summary>
        /// <param name="json">The string of Json sent</param>
        /// <param name="log">Logging</param>
        /// <returns>A InboxMessage</returns>
        public static InboxMessage Parse(this string json, StringBuilder log)
        {
            InboxMessage _inboxMessage = null;

            try {
                _inboxMessage = JsonConvert.DeserializeObject<InboxMessage>(json);
                log.AppendLine($"Type: {_inboxMessage.Type}");
                object typeWrapper = null;

                switch (_inboxMessage.Type.ToLower())
                {
                    case "kunderegister":
                        typeWrapper = new { KundeRegister = _inboxMessage };
                        break;
                    case "kundehendelse":
                        typeWrapper = new { KundeHendelse = _inboxMessage };
                        break;
                    case "sakshendelse":
                        typeWrapper = new { SaksHendelse = _inboxMessage };
                        break;
                }
                
                var wrapper = JsonConvert.SerializeObject(typeWrapper);
                _inboxMessage.Message = JsonConvert.DeserializeXmlNode(wrapper).OuterXml;

            } catch(Exception ex)
            {
                log.AppendLine($"ParseError: {ex} : {ex.InnerException} : {ex.Message}");
            }
            return _inboxMessage;
        }
    }
}
