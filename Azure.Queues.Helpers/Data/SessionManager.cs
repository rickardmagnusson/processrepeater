﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Azure.Queues.Helpers.Filters;

namespace Azure.Queues.Helpers.Data
{
    public class SessionManager
    {
        private static StringBuilder Logger;
        private static SessionManager _instance;


        private SessionManager() { }


        private static SessionManager Instance()
        {
            if (_instance == null)
                _instance = new SessionManager();
            return _instance;
        }



        /// <summary>
        /// Get Current ISessionFactory
        /// </summary>
        private static ISessionFactory CurrentFactory
        {
            get
            {
                return Instance().CreateConfig();
            }
        }


        public static ISession Open(StringBuilder logger)
        {
            Logger = logger;
            return CurrentFactory.OpenSession();
        }


        /// <summary>
        /// Creates the ISessionFactory configuration.
        /// </summary>
        /// <returns>ISessionFactory of current configuation.</returns>
        private ISessionFactory CreateConfig()
        {
            try { 
                return FluentConfiguration
                    .BuildSessionFactory();
            }
            catch (Exception ex)
            {
                Logger.AppendLine($"Error:ISessionFactory: {ex} {ex.InnerException}");
                throw ex;
            }
        }



        /// <summary>
        /// The current FluentConfiguration
        /// </summary>
        private FluentConfiguration FluentConfiguration
        {
            get
            {
                try { 
                    return Fluently
                      .Configure()
                         .Database(DatabaseConfiguration)
                            .Mappings(MapAssemblies);
                        //.ExposeConfiguration(BuildSchema);
                        //  .ExposeConfiguration(c => c.SetInterceptor(new SqlPrintInterceptor()));
                }
                catch (Exception ex)
                {
                    Logger.AppendLine($"Error:FluentConfiguration: {ex} {ex.InnerException}");
                    throw ex;
                }
            }
        }


        /// <summary>
        /// Creates the configuration for FluentnHibernate
        /// </summary>
        /// <returns>IPersistenceConfigurer config</returns>
        internal IPersistenceConfigurer DatabaseConfiguration()
        {
            try {
                return MsSqlConfiguration.MsSql2008
                        .UseOuterJoin()
                        .ConnectionString(Environment.GetEnvironmentVariable("ConnectionString"));
                        //.ShowSql();

            }catch(Exception ex)
            {
                Logger.AppendLine($"Error:IPersistenceConfigurer: {ex} {ex.InnerException}");
                throw ex;
            }



        }


        /// <summary>
        /// Searches all assemblies for IEntity's
        /// </summary>
        /// <param name="fmc">MappingConfiguration</param>
        private void MapAssemblies(MappingConfiguration fmc)
        {
            try { 
                (from a in AppDomain.CurrentDomain.GetAssemblies()
                 select a
                     into assemblies
                         select assemblies)
                                    .ToList()
                                        .ForEach(a => {
                                            fmc.AutoMappings.Add(AutoMap.Assembly(a)
                                               .OverrideAll(p => {
                                                   p.IgnoreProperty(typeof(NoProperty));
                                               }).Conventions.Add<StringColumnLengthConvention>()
                                                 .Where(IsEntity));
                                        });
            }
            catch (Exception ex)
            {
                Logger.AppendLine($"Error:MapAssemblies: {ex} {ex.InnerException}");
                //throw ex;
            }
        }



        /// <summary>
        /// The Entity to look for
        /// </summary>
        /// <param name="t">The type to compare</param>
        /// <returns>If type is Entity</returns>
        private static bool IsEntity(Type t)
        {
            try { 
                return typeof(IEntity).IsAssignableFrom(t);
            }
            catch (Exception ex)
            {
                Logger.AppendLine($"Error:IsEntity: {ex} {ex.InnerException}");
                return false;
            }
        }


        /// <summary>
        /// Builds the configuration for FluentnHibernate/nHibernate
        /// </summary>
        /// <param name="config"></param>
        private void BuildSchema(NHibernate.Cfg.Configuration config)
        {
           
        }
    }
}
