﻿using Azure.Queues.Helpers.Data;
using FluentNHibernate.Mapping;

namespace Azure.Queues.Process.Data
{
    /// <summary>
    /// Entity
    /// </summary>
    /// <typeparam name="T">The type to map</typeparam>
    public abstract class Entity<T> : ClassMap<T> where T : IEntity
    {
        protected Entity()
        {
            LazyLoad();
        }
    }

}
