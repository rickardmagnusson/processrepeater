﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Azure.Queues.Helpers
{
    public static class TypeHelper
    {
        /// <summary>
        /// Parse a string to T enum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
