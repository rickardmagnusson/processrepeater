﻿using Azure.Queues.Helpers.Data;
using Azure.Queues.Helpers.Models;
using Microsoft.ServiceBus.Messaging;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Helpers
{

    /// <summary>
    /// Contains logic of processing InboxMessages
    /// </summary>
    public class Process
    {
        private static StringBuilder logger;
        private static Process _instance;
        private static List<int> items;
        private readonly string connectionstring;
        //private static ISession session;

        private Process() {
            connectionstring = Environment.GetEnvironmentVariable("ConnectionString");
        }



        private static Process GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Process();
            }
            return _instance;
        }



        /// <summary>
        /// Check if there's more items in InboxMessages
        /// </summary>
        /// <returns>True if InboxMessages has items</returns>
        public static bool HasNext()
        {
            return GetInstance().HasItems();
        }



        /// <summary>
        /// Moves items of type InboxMessage to InboxMessageCompleted
        /// </summary>
        public static void MoveItems()
        { 
            var instance = GetInstance();
            items = instance.GetAll();

            if (items.Any()) {
                var itemsToMove = items.Take(2).ToList();
                instance.MoveItems(itemsToMove);
            }
        }



        /// <summary>
        /// Insert a new MsmqInboxMessage
        /// </summary>
        /// <param name="item">The items(Json) string</param>
        /// <param name="type">Type of items to insert (EventType)</param>
        /// <param name="log">Logger</param>
        /// <returns>Logger string</returns>
        public static string Insert(string item, EventType type, StringBuilder log)
        {
            logger = log;
            var o = item.Parse(log);
            GetInstance().InsertItem(o, type);
            return log.ToString();
        }



        /// <summary>
        /// Insert a InboxItem to MsmsqInbox
        /// </summary>
        /// <param name="message">The InboxMessage item</param>
        /// <param name="type">The enum EventType type</param>
        private void InsertItem(InboxMessage message, EventType type)
        {
            SqlTransaction transaction = null;

            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                conn.Open();
                transaction = conn.BeginTransaction();
                string sql = $"INSERT INTO MsmqInbox (InboxMessage,Created,EventType,Type) VALUES ('{message.Message}','{DateTime.UtcNow}',{(int)type}, '{type.ToString()}')";
               
                var cmd = new SqlCommand(sql, conn);
                cmd.Transaction = transaction;
                try
                {
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    logger.AppendLine("Inserted row");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    logger.AppendLine($"Inserted row failed. Error: {ex}");
                }
                finally
                {
                    conn.Close();
                }
            }
        }



        /// <summary>
        /// Moves IboxMessages from MsmqInbox to MsmqInboxCompleted
        /// </summary>
        /// <param name="items"></param>
        private void MoveItems(List<int> items)
        {
            foreach (var item in items)
            {
                SqlTransaction transaction = null;

                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    string proc = $"SET IDENTITY_INSERT MsmqInboxCompleted ON;" +
                        $"DELETE FROM MsmqInbox " +
                        $"OUTPUT DELETED.* " +
                        $"INTO MsmqInboxCompleted([Id],[InboxMessage],[Created],[EventType],[Type])" +
                        $"WHERE Id = {item}" +
                        $"SET IDENTITY_INSERT MsmqInboxCompleted OFF;";

                    //logger.AppendLine(proc);

                    var cmd = new SqlCommand(proc, conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = transaction;

                    try
                    {
                        cmd.ExecuteNonQuery();
                        transaction.Commit();
                        logger.AppendLine("Moved row");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        logger.AppendLine($"Move row failed. Error: {ex}");
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }



        /// <summary>
        /// Check if the InboxMessages has new InboxMessages
        /// </summary>
        /// <returns></returns>
        private bool HasItems()
        {
            using (var conn = new SqlConnection(connectionstring))
            {
                string sql = $"SELECT * FROM MsmqInbox ORDER BY Id ASC";
                var cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                conn.Open();

                using (var objReader = cmd.ExecuteReader())
                {
                    if (objReader.HasRows)
                        return true;

                    conn.Close();
                }
            }

            return false;
        }




        /// <summary>
        /// Get all InboxMessages
        /// </summary>
        /// <returns>A List of integers (The Id for each MsmqInboxMessage)</returns>
        private List<int> GetAll()
        {
            var items = new List<int>();

            using (var conn = new SqlConnection(connectionstring))
            {
                string sql = $"SELECT * FROM MsmqInbox ORDER BY Id ASC";
                var cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                conn.Open();
                using (var objReader = cmd.ExecuteReader())
                {
                    if (objReader.HasRows)
                    {
                        while (objReader.Read())
                        {
                            var item = objReader.GetInt32(objReader.GetOrdinal("Id"));
                            items.Add(item);
                        }
                    }
                    conn.Close();
                }
            }
            return items;
        }
    }
}
