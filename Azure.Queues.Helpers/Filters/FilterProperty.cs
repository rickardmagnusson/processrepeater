﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;
using System;
using System.Collections.Generic;
using System.Text;

namespace Azure.Queues.Helpers.Filters
{
    
    public class NoProperty : Attribute { }


    /// <summary>
    /// Extension to ignore attributes
    /// </summary>
    public static class FluentIgnore
    {

        /// <summary>
        /// Ignore a single property.
        /// Property marked with this attributes will no be persisted to table.
        /// </summary>
        /// <param name="p">IPropertyIgnorer</param>
        /// <param name="propertyType">The type to ignore.</param>
        /// <returns>The property to ignore.</returns>
        public static IPropertyIgnorer IgnoreProperty(this IPropertyIgnorer p, Type propertyType)
        {
            return p.IgnoreProperties(x => x.MemberInfo.GetCustomAttributes(propertyType, false).Length > 0);
        }
    }


    /// <summary>
    /// Adds length value to string fields in database(TEXT)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class StringLength : System.Attribute
    {
        public int Length = 0;
        public StringLength(int len)
        {
            Length = len;
        }
    }


    /// <summary>
    /// Fluent dont understand Stringlength? Bug?, Anyway, this helper does the job!!
    /// </summary>
    public class StringColumnLengthConvention : IPropertyConvention, IPropertyConventionAcceptance
    {
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
        {
            criteria.Expect(x => x.Type == typeof(string)).Expect(x => x.Length == 0);
        }

        public void Apply(IPropertyInstance instance)
        {
            instance.Length(10000);
        }
    }


}
