﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Azure.Queues.Helpers
{

    /// <summary>
    /// Wish types that are used in Process
    /// </summary>
    public enum EventType
    {
        KundeRegister = 1,
        KundeHendelse = 2,
        SaksHendelse = 3
    }
}
