﻿using System;
using System.Runtime.Serialization;

namespace Azure.Queues.Helpers.Models
{
    
    /// <summary>
    /// The inbox
    /// </summary>
    [Serializable]
    public class InboxMessage
    {
        //[DataMember(Name = "Id")]
        public Guid Id { get; set; }
        //[DataMember(Name = "Message")]
        public string Message { get; set; }
        //[DataMember(Name = "EventType")]
        public EventType EventType { get; set; }
        //[DataMember(Name = "Type")]
        public string Type { get; set; }
        //[DataMember(Name = "Created")]
        public DateTime Created { get; set; } = DateTime.UtcNow;
    }
}
