﻿using Azure.Queues.Helpers.Data;
using System.ComponentModel.DataAnnotations.Schema;

namespace Azure.Queues.Helpers.Models
{
    [Table("AzureConfiguration")]
    public class AzureConfiguration : IEntity
    {
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
    }
}
