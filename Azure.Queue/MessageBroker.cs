﻿

using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Consumer
{
    public class MessageBroker : IDisposable
    {
        private TopicClient topicClient;
        private QueueClient queueClient;

        public MessageBroker(string[] args) {}

        public async Task SendMessageToQueueAsync(string queueName, string message)
        {
            this.queueClient = QueueClient.CreateFromConnectionString(ConfigurationManager.AppSettings["SB"], queueName);
            BrokeredMessage brokeredMessage = new BrokeredMessage(message);
            await queueClient.SendAsync(brokeredMessage);
        }

        public async Task SendMessageToTopicAsync(string topicName, string message)
        {
            this.topicClient = TopicClient.CreateFromConnectionString(ConfigurationManager.AppSettings["SB"], topicName);
            BrokeredMessage brokeredMessage = new BrokeredMessage(message);
            await topicClient.SendAsync(brokeredMessage);
        }


        public void Dispose()
        {
            this.queueClient.Close();
            this.topicClient.Close();
        }
    }
}
