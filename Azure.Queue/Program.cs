﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
using static System.Convert;

namespace Azure.Queue
{
    class Program
    {
        public static void Main(string[] args)
        {
            bool keepGoing = true;
            try
            {
                do
                {
                    var selection = DisplayMenu();
                    switch (selection)
                    {
                        case 1:
                            WriteLine("Enter Auto read Event Grid.(local.settings.json (Endpoint,EventType,Secret))");
                            //new MessageBroker(args).GetAwaiter().GetResult();
                            break;
                        case 2:
                            WriteLine("Bye.");
                            keepGoing = false;
                            break;
                        default:
                            throw new InvalidOperationException("You entered an invalid option. Bye.");
                    }
                } while (keepGoing);
            }
            catch (Exception ex)
            {
                WriteLine($"Exeption occurred: {ex.Message}");
                Console.ReadLine();
            }
        }

        static public int DisplayMenu()
        {
            WriteLine();
            WriteLine(" 1. Servicebus Topic");
            WriteLine(" 2. Exit");
            WriteLine(" Which would you like to trigger?  Enter '4' to exit.");
            var result = ReadLine();
            return ToInt32(result);
        }
    }

   

}
