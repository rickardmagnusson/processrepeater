

Solution description

Azure.Functions.EventProcessorQueue
Contains Azure function logic

Contains 5 Azure Function
	1. EventProcessor		(Responsible for processing InboxMessages)
	2. RequestRestart		(Sends a new eventGrid request to EventProcessor (If theres more items in database))
	3. KundeRegistrering	(Servicebus subscription)(Triggers EventProcess)
	4. KundeHendelse		(Servicebus subscription)(Triggers EventProcess)
	5. SaksHendelse			(Servicebus subscription)(Triggers EventProcess)

	EventGridSubscription
		2. -5. Subscribes to (EventGridProcessSubscription). 