using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Text;
using System;
using Microsoft.ServiceBus.Messaging;
using Azure.Queues.Process;
using Azure.Queues.Process.EventGrid;
using Microsoft.Azure.WebJobs.Extensions.EventGrid;
using System.Linq;
using Azure.Queues.Process.Helpers;
using ProcessManager;
using Common.Enums;

using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;

namespace EventProcessor
{


    /// <summary>
    /// Functions for processing InboxMessages
    /// </summary>
    public static class EventProcessorFunc
    {
        //Debug
        private static string key = TelemetryConfiguration.Active.InstrumentationKey = Environment.GetEnvironmentVariable("APPINSIGHTS_INSTRUMENTATIONKEY", EnvironmentVariableTarget.Process);
        private static TelemetryClient telemetry = new TelemetryClient() { InstrumentationKey = key };


        /// <summary>
        /// Processing InboxMessages
        /// </summary>
        /// <param name="eventGridEvent"></param>
        /// <param name="starter"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [Singleton(Mode = SingletonMode.Function)]
        [FunctionName(nameof(EventProcessor))]
        public static Task EventProcessor([EventGridTrigger]JObject eventGridEvent,
            [OrchestrationClient] DurableOrchestrationClient starter,
            ILogger log)
        {
            var Logger = new StringBuilder();
            //Action<string> Log = s => log.LogInformation(s);

            var message = eventGridEvent.GetSubject();
            Logger.AppendLine($"Durable Message: {message}");
            Logger.AppendLine("Starting Initializer");
            var SessionID = new Guid();

            Task.Run(async () => {
                await Common.Initializer.Initialize.InitSettings();
            });

            var handler = ExternalEventHandler.GetInstance(SessionID);
            Logger.AppendLine("Initializer done");

            Logger.AppendLine("Starting RunProcessBatch");
            var result = handler.RunProcessBatch();
            Logger.Append($"RunBatch result: {result.Result}");

            log.LogInformation($"Log: {Logger.ToString()}");
            
            if (Process.HasNext(Logger)) {
                Logger.Append("Request restart success.");
                Logger.Append($"Log: {Logger.ToString()}");
                log.LogInformation(Logger.ToString());
                return starter.StartNewAsync("RequestRestart", null); 
            }
            else {
                return Task.Run(()=> { Logger.Append("Done process batch.");  });
            }
        }


        /// <summary>
        /// Triggers EventGrid to rerun EventProcessor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [Singleton(Mode = SingletonMode.Listener)]
        [FunctionName(nameof(RequestRestart))]
        public static async Task RequestRestart(
           [OrchestrationTrigger] DurableOrchestrationContext context, ILogger log)
        {
            var result = await EventGridSender.Send("Restart");
            log.LogInformation($"{result.StatusCode}");
        }


        /// <summary>
        ///  Process a Servicebus message of type KundeRegister
        /// </summary>
        /// <param name="queueItem"></param>
        /// <param name="log"></param>
        [Singleton(Mode = SingletonMode.Function)]
        [FunctionName(nameof(Kunderegister))]
        public static void Kunderegister([ServiceBusTrigger("kunderegister", "kunderegistersubscription", AccessRights.Listen, Connection = "SBConn")]string queueItem, TraceWriter log)
        {
            var logger = new StringBuilder();
            log.Info($"{queueItem}");
            var loginfo = Process.Insert(queueItem, EventMessageType.KundeRegisterEvent, logger);
            var result = EventGridSender.Send("Kunderegister");
            log.Info(loginfo);
        }


        /// <summary>
        /// Process a Servicebus message of type KundeHendelse
        /// </summary>
        /// <param name="queueItem"></param>
        /// <param name="log"></param>
        [Singleton(Mode = SingletonMode.Function)]
        [FunctionName(nameof(KundeHendelse))]
        public static void KundeHendelse([ServiceBusTrigger("kundehendelse", "kundehendelsesubscription", AccessRights.Listen, Connection = "SBConn")]string queueItem, TraceWriter log)
        {
            var logger = new StringBuilder();
            log.Info($"{queueItem}");
            var loginfo = Process.Insert(queueItem, EventMessageType.KundeHendelseEvent, logger);
            var result = EventGridSender.Send("Kundehendelse");
            log.Info(loginfo);
        }


        /// <summary>
        /// Process a Servicebus message of type SaksHendelse
        /// </summary>
        /// <param name="queueItem"></param>
        /// <param name="log"></param>
        [Singleton(Mode = SingletonMode.Function)]
        [FunctionName(nameof(Sakshendelse))]
        public static void Sakshendelse([ServiceBusTrigger("sakshendelse", "sakshendelsesubscription", AccessRights.Listen, Connection = "SBConn")]string queueItem, TraceWriter log)
        {
            var logger = new StringBuilder();
            //log.Info($"{queueItem}");
            var loginfo = Process.Insert(queueItem, EventMessageType.SakHendelseEvent, logger);
            var result = EventGridSender.Send("Sakshendelse");
            log.Info($"{loginfo}");
        }
    }
}
