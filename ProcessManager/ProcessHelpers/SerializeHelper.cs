﻿using System.IO;
using System.Xml.Serialization;
using Common;
using Common.Entities;

namespace ProcessManager.ProcessHelpers
{
    public class SerializeHelper
    {
        public static T DeserializeEventMessageContent<T>(string xml) where T : class, new()
        {
            using (var reader = new StringReader(xml))
            {
                var type = typeof(T);
                return (T)new XmlSerializer(typeof(T)).Deserialize(reader);
            }
        }
    }
}