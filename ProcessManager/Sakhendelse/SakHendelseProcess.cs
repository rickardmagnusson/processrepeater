﻿using System.Collections.Generic;
using Common.Adapters;
using Common.Entities;
using Common.Initializer;
using DataAccess;
using ProcessManager.ProcessHelpers;
using DataAccess.Models;

namespace ProcessManager.Sakhendelse
{
    public class SakHendelseProcess
    {
        public static void ProcessMessages(List<MsmqMessageInbox> items, DataWriter dataWriter, QueryCrm query)
        {
            EtlDatabaseHelper.SetMessagesToIgnore(items, "SakHendelse is not ready to be processed yet.", dataWriter);
        }



        #region Private Methods

        private static List<ProcessedCustomerResponse> ProcessEvents(List<MsmqMessageInbox> items, QueryCrm queryToUse)
        {
            var result = new List<ProcessedCustomerResponse>();

            using (var query = new QueryExpressionHelper())
            {
                var validBanksToRun = Initialize.GetBankToRunForSakHendelse();
                Initialize.LoadProductCodes();
                Initialize.LoadAppNames();


                foreach (var item in items)
                {
                    var response = new ProcessedCustomerResponse { MsmqInboxId = item.Id, RetryCount = item.RetryCount };
                    var evt = SerializeHelper.DeserializeEventMessageContent<SakHendelse>(item.MessageContent);

                    if (validBanksToRun.Contains(evt.Bankregnr))
                    {
                        SakHendelseManager.HandleSakHendelseEvent(evt, response, query);
                    }
                    else
                    {
                        response.ErrorMessage = $"Skipping SakHendelse for bank '{evt.Bankregnr}.'";
                        response.IsIgnored = true;
                    }

                    result.Add(response);
                }
            }
            return result;
        }

        #endregion
    }
}
