![Azure.png](https://bitbucket.org/repo/9pa6d5o/images/328555937-Azure.png)

```
$ git clone https://rickardmagnusson@bitbucket.org/rickardmagnusson/processrepeater.git/wiki
```

# Azure.EventProcessorFuncs
### Solution description

Azure.EventProcessor
Contains Azure functions and all logic for processing InboxMessages and events for CRM.

***Using .NET Version 4.6.1 for all projects.***



### Azure.Function.EventProcessQueue

##### Contains 5 Azure Function

  1. EventProcessor	(Responsible for processing InboxMessages) (Need subscription to EventGrid. Current is EventGridTriggerStartTopic(Has to be created manually))
  2. ReRun	(Post a GridEvent to EventProcessor (If theres more items in database to process))
  3. KundeRegistrering	(Servicebus subscription)(Triggers EventGrid)
  4. KundeHendelse		(Servicebus subscription)(Triggers EventGrid)
  5. SaksHendelse		(Servicebus subscription)(Triggers EventGrid)


#### EventGridSubscription

	2. -5. Subscribes to (EventGridProcessSubscription). 
	When running Consumer/Azure.Queues.Servicebus, it will automatically add Topics and Subscriptions in Servicebus.
    Creates: KundeRegister, KundeHendelse and SaksHendelse Topics and Subscriptions.



## Azure.Queues.Process

	Contains logic for processing InboxMessages. 
    The SessionManager, the creation of ISessions for nHibernate is included in Data folder.
    Data/Filters: Contains filter for Ignore a single property.

## Crm.Services.Extensions

    Extensions for CRM. Has in this moment IOrganizationService that automatically connects to the CRM from AppSettings values.
    These values also has to be defined in Azure AppSettings
    ida:appKey
    ida:ClientId
    ida:ResourceToCRM
    ida:AADInstance
    ida:TenantId


### Solution folders

    Consumers: Contains a Mockup application that send Messages thru Servicebus
	Dependencies: All dependencies that not to be found on NuGet
	Deprecated: Contain deprecated projects, but still may have som useful code.
    Extensions: Contains Extensions for CRM, also called helper classes that simplify tasks.
    SolutionItems: Contains this Help instructions

## Configuration 

	 Configurationsettings example: (local.settings.json) "Also needs to be set in Azure Appsettings"
    "SBConn": "Endpoint=sb://eventsender.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey={Key in Servicebus}",
    "ConnectionString": "Server=tcp:azservices.database.windows.net,1433;Initial Catalog=ServiceDB;Persist Security Info=False;User ID=crmadmin;Password={Sql password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;",
    "Endpoint": "https://eventgridtriggerstarttopic.northeurope-1.eventgrid.azure.net/api/events",
    "EventType": "EventGridTriggerStartTopic",
    "Secret": "{Secret found in EventGrid in Azure}"


### POC.Tests
    
    Contains connection test for SessionManager (nHibernate) and CRM(Connection)


### NHibernate

Current version contains NHibernate, wich make is easy to read, write and create changes thru models against a database.
To create a Nhibernate model, just inherit ITable, IEntity. Be sure that you set an Id.

Every property must be virtual, even functions.
To ignore a property decorate it with the [IgnoreProperty()] attribute.
It is not nescesary to drop a table when adding a new Property, but need to be dropped when removing, or manually remove the field in tha database.


```
#!c#

    [Table("ConfigurationSettings")]
    public class ConfigurationSettings : ITable , IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }

        [IgnoreProperty()]
        public virtual string ValuePair() { 
           return $"{Name}:{Value}";
        }
    }
```



## Business logic dependencies
### Common

    Contains models and logic for events that is sent to CRM. 
    Enity framework is now managed by nHibernate. 


### DataAccess (To be deleted: Moved into Azure.Queue.Process)
  
  *Status*: 

    All dependencies to EF is replaced by *nHibernate**

    DataReader.cs
    DataWriter.cs
    MsmqMessageInbox.cs(Rewriteln the classes to inherit ITable, IEntity)
    


### ProcessManager (To be deleted: Moved into Azure.Queue.Process)

  
*Status*: 

    All dependencies to EF is replaced by *nHibernate**
    Ive been thrue all the classes.



## Helpers
### nHibernate requires an Id in tables
##### Below is a Sql Script that creates Id in tables that doesnt have an id, due to nHibernate requires an Id

     WITH T
     AS (SELECT ISNULL((SELECT MAX(Id) FROM ETL_Config), 0) + 
                    ROW_NUMBER() OVER (ORDER BY Id) AS New_ID,
         Id
         FROM   ETL_Config
         WHERE  Id IS NULL)
         UPDATE T
         SET    Id = New_ID 
