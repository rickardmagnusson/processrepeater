﻿using Common.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Crm.Services.Extensions;
using Microsoft.Xrm.Sdk;
using System.Configuration;
using Microsoft.Xrm.Sdk.Query;
using Common;
using Common.Adapters;
using Newtonsoft.Json;

namespace Azure.Servicebus.Sender
{
    public partial class Form1 : Form
    {
        private KundeHendelseWrapper khw;
        private KundeRegisterWrapper krw;
        private SaksHendelseWrapper shw;
        private IOrganizationService service;

        public Form1()
        {
            InitializeComponent();
            try
            {
                lblConnection.Text = "Connecting to CRM..."; // Just not to be shown...
                service = OrganizationServiceHelper.GetIOrganizationService();
                string _crmUrl = ConfigurationManager.AppSettings["ida:ResourceToCRM"];
                lblConnection.Text = $"Connected to CRM:\nUrl: {_crmUrl}";
            }
            catch
            {
               lblConnection.Text = "Check your appsettings(app.config). Applicationsettings for CRM is probably wrong.";
            }
        }


        //Load all combinations
        private void LoadComponentsData()
        {
            EventTypes et = new EventTypes();
            
            et.Events.AddRange(new List<EventMessageType> { EventMessageType.Undefined, EventMessageType.KundeRegisterEvent, EventMessageType.KundeHendelseEvent, EventMessageType.SakHendelseEvent });
            AddToCombo<EventMessageType>(cmbEventMessageTypes, et.Events);
            cmbEventMessageTypes.Text = "Velg (Event)type";

            LoadKundeHendelse();
        }


        private void LoadKundeHendelse()
        {
            KundeHendelse kh = new KundeHendelse();

            khw = new KundeHendelseWrapper();

            //Load all possible values from enums

            foreach (ProcessArea pa in (ProcessArea[])Enum.GetValues(typeof(ProcessArea)))
                khw.ProcessArea.Add(pa);

            foreach (KundeHendelsePhase p in (KundeHendelsePhase[])Enum.GetValues(typeof(KundeHendelsePhase)))
                khw.Fase.Add(p);

            foreach (Fagsystem p in (Fagsystem[])Enum.GetValues(typeof(Fagsystem)))
                khw.Fagsystem.Add(p);
        }


        private void AddToCombo<T>(ComboBox cb, List<T> items)
        {
            foreach (var item in items)
                cb.Items.Add(item);

            cb.Items.RemoveAt(0);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            LoadComponentsData();
        }


        public class EventTypes
        {
            public List<EventMessageType> Events { get; set; } = new List<EventMessageType>();
        }


        public class KundeHendelseWrapper
        {
            public List<ProcessArea> ProcessArea { get; set; } = new List<ProcessArea>();
            public List<KundeHendelsePhase> Fase { get; set; } = new List<KundeHendelsePhase>();

            public List<Fagsystem> Fagsystem { get; set; } = new List<Fagsystem>();

            public List<string> ProduktNavn
            {
                get
                {
                    //GetOpportunityProducts();
                    return null;
                }
            }
        }


        public void GetOpportunityProducts(Guid opportunityid)
        {
            var list = new QueryExpressionHelper().GetOpportunityProducts(opportunityid, new ProcessedCustomerResponse());
        }


        public class KundeRegisterWrapper
        {
        }

        public class SaksHendelseWrapper
        {

        }


        private void cmbEventMessageTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sel = ((ComboBox)sender).SelectedItem;
            EventMessageType type = (EventMessageType)Enum.Parse(typeof(EventMessageType), sel.ToString());
            clearAll();

            switch (type)
            {
                case EventMessageType.KundeRegisterEvent:

                    break;
                case EventMessageType.KundeHendelseEvent:
                    AddToCombo(cmbProcessArea, khw.ProcessArea);
                    cmbProcessArea.Text = "Velg ProcessArea";
                    break;
                case EventMessageType.SakHendelseEvent:
                    break;
            }
        }


        private void clearAll()
        {
            cmbProcessArea.Items.Clear();
            cmbProcessArea.ResetText();
            cmbFagsystem.Items.Clear();
            cmbFagsystem.ResetText();
            cmbKundeHendelsePhase.Items.Clear();
            cmbKundeHendelsePhase.ResetText();
        }


        private void cmbProcessArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbKundeHendelsePhase.Items.Clear();
            AddToCombo(cmbKundeHendelsePhase, khw.Fase);

            cmbFagsystem.Items.Clear();
            AddToCombo(cmbFagsystem, khw.Fagsystem);
            cmbFagsystem.Text = "Velg fagsystem";
        }

        private void cmbKundeHendelsePhase_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbProduktNavn_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Get the current values and create a json post. 
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };

            var sel = cmbEventMessageTypes.SelectedItem;
            EventMessageType Topic = (EventMessageType)Enum.Parse(typeof(EventMessageType), sel.ToString());
            EventType evt = Helpers.FromTopic(Topic.ToString());

            object typeWrapper = null;

            string log = Topic.ToString();

            var message = new
            {
                Id = Guid.NewGuid(),
                Message = "m",
                EventType = evt,
                Type = Topic,
                Created = DateTime.UtcNow
            };

            switch (Topic)
            {
                case EventMessageType.KundeRegisterEvent:
                    typeWrapper = new { KundeRegister = message };
                    break;
                case EventMessageType.KundeHendelseEvent:
                    typeWrapper = new { KundeHendelse = message };
                    break;
                case EventMessageType.SakHendelseEvent:
                    typeWrapper = new { SaksHendelse = message };
                    break;
            }

            var json = JsonConvert.SerializeObject(typeWrapper);

            log = log + json;
            //lblLog.Text = log;
        }


        public static string kundeHendelse = @"
        {
            'uuid': '1ad4e171-f43b-4e15-98cc-bf3d38b8d0f5',
            'applikasjon' : 'SPARING_ADVENT_TRADEX_WS',
            'prosessomraade' : 'Sparing',
            'fagsystem' : 'TRADEX',
            'fase' : 'ENDRET',
            'transaksjonstyp' : 'SPAREAVTALE',
            'produktnavn' : 'Sparekonto',
            'produktkode' : 'CRM_027',
            'handling' : 'UTVIDET',
            'kundeid': '30016919345',
            'brukersted': '3290',
            'channel': 'MOBILBANK',
            'tidspunkt': '2019-02-02T20:03:51.65Z',
            'verdi':'900',
            'tidligereverdi': '700',
            'annet' : 'This is a test of inserting text of type kundehendelse into crm.'
        }";
    }

    public static class Helpers
    {
        public static EventType FromTopic(string topic)
        {
            topic = topic.ToLower();
            switch (topic)
            {
                case "kunderegister":
                    return EventType.KundeRegister;
                case "kundehendelse":
                    return EventType.KundeHendelse;
                case "sakshendelse":
                    return EventType.KundeHendelse;
                default:
                    return EventType.Undefined;
            }
        }
    }

}


