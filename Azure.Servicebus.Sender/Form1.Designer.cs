﻿namespace Azure.Servicebus.Sender
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbEventMessageTypes = new System.Windows.Forms.ComboBox();
            this.cmbProcessArea = new System.Windows.Forms.ComboBox();
            this.cmbKundeHendelsePhase = new System.Windows.Forms.ComboBox();
            this.cmbFagsystem = new System.Windows.Forms.ComboBox();
            this.lblConnection = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLog = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbEventMessageTypes
            // 
            this.cmbEventMessageTypes.FormattingEnabled = true;
            this.cmbEventMessageTypes.Location = new System.Drawing.Point(42, 100);
            this.cmbEventMessageTypes.Name = "cmbEventMessageTypes";
            this.cmbEventMessageTypes.Size = new System.Drawing.Size(303, 21);
            this.cmbEventMessageTypes.TabIndex = 0;
            this.cmbEventMessageTypes.SelectedIndexChanged += new System.EventHandler(this.cmbEventMessageTypes_SelectedIndexChanged);
            // 
            // cmbProcessArea
            // 
            this.cmbProcessArea.FormattingEnabled = true;
            this.cmbProcessArea.Location = new System.Drawing.Point(42, 140);
            this.cmbProcessArea.Name = "cmbProcessArea";
            this.cmbProcessArea.Size = new System.Drawing.Size(303, 21);
            this.cmbProcessArea.TabIndex = 1;
            this.cmbProcessArea.SelectedIndexChanged += new System.EventHandler(this.cmbProcessArea_SelectedIndexChanged);
            // 
            // cmbKundeHendelsePhase
            // 
            this.cmbKundeHendelsePhase.FormattingEnabled = true;
            this.cmbKundeHendelsePhase.Location = new System.Drawing.Point(42, 177);
            this.cmbKundeHendelsePhase.Name = "cmbKundeHendelsePhase";
            this.cmbKundeHendelsePhase.Size = new System.Drawing.Size(303, 21);
            this.cmbKundeHendelsePhase.TabIndex = 2;
            this.cmbKundeHendelsePhase.SelectedIndexChanged += new System.EventHandler(this.cmbKundeHendelsePhase_SelectedIndexChanged);
            // 
            // cmbFagsystem
            // 
            this.cmbFagsystem.FormattingEnabled = true;
            this.cmbFagsystem.Location = new System.Drawing.Point(42, 216);
            this.cmbFagsystem.Name = "cmbFagsystem";
            this.cmbFagsystem.Size = new System.Drawing.Size(303, 21);
            this.cmbFagsystem.TabIndex = 3;
            this.cmbFagsystem.SelectedIndexChanged += new System.EventHandler(this.cmbProduktNavn_SelectedIndexChanged);
            // 
            // lblConnection
            // 
            this.lblConnection.AutoSize = true;
            this.lblConnection.Location = new System.Drawing.Point(403, 53);
            this.lblConnection.Name = "lblConnection";
            this.lblConnection.Size = new System.Drawing.Size(86, 13);
            this.lblConnection.TabIndex = 4;
            this.lblConnection.Text = "ConnectionState";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(205, 389);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Send event";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(261, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Test tool for events to CRM";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblLog);
            this.panel1.Location = new System.Drawing.Point(406, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(364, 312);
            this.panel1.TabIndex = 7;
            // 
            // lblLog
            // 
            this.lblLog.AutoSize = true;
            this.lblLog.Location = new System.Drawing.Point(21, 19);
            this.lblLog.Name = "lblLog";
            this.lblLog.Size = new System.Drawing.Size(35, 13);
            this.lblLog.TabIndex = 0;
            this.lblLog.Text = "lblLog";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblConnection);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmbFagsystem);
            this.Controls.Add(this.cmbKundeHendelsePhase);
            this.Controls.Add(this.cmbProcessArea);
            this.Controls.Add(this.cmbEventMessageTypes);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbEventMessageTypes;
        private System.Windows.Forms.ComboBox cmbProcessArea;
        private System.Windows.Forms.ComboBox cmbKundeHendelsePhase;
        private System.Windows.Forms.ComboBox cmbFagsystem;
        private System.Windows.Forms.Label lblConnection;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblLog;
    }
}

