﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace SerializeHelpers
{
    public class TypeConverter
    {
        
        private TypeConverter(){}

        /// <summary>
        /// Creates a XmlDocument from a json string.
        /// </summary>
        /// <typeparam name="T">The type to convert into</typeparam>
        /// <param name="json">The json</param>
        /// <returns>Xml string</returns>
        public static string SerializeJsonToXml<T>(string json) where T : class
        {
            var typeName = typeof(T).Name;
            var o = JObject.Parse(json);
            var so = JsonConvert.SerializeObject(o);
            var xml = JsonConvert.DeserializeXmlNode(so, typeName).OuterXml;

            return SerializeToValidXml<T>(xml);
        }


        /// <summary>
        /// A function that will validate the xml created and also add declaration.
        /// </summary>
        /// <typeparam name="T">A model of type T</typeparam>
        /// <param name="xml">The xml string</param>
        /// <returns>A valid xml document as string</returns>
        public static string SerializeToValidXml<T>(string xml)
        {
            var serializer = new XmlSerializer(typeof(T));
            var settings = new XmlWriterSettings{
                Encoding = new UnicodeEncoding(false, false),
                Indent = true,
                OmitXmlDeclaration = true
            };

            var stringReader = new StringReader(xml);
            var nobj = serializer.Deserialize(stringReader);

            using (var textWriter = new StringWriter()) {
                using (var xmlWriter = XmlWriter.Create(textWriter, settings)) {
                    serializer.Serialize(xmlWriter, nobj);
                }
                return textWriter.ToString();
            }
        }
    }




    /// <summary>
    /// Contains functions for validating Json fields against a model properties.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonFieldsValidator<T> where T : class
    {
        private static string _temp;
        private readonly Dictionary<string, JValue> _fields;
        public IEnumerable<KeyValuePair<string, JValue>> GetAllFields() => _fields;


        /// <summary>
        /// Validate json fields with model properties. 
        /// </summary>
        /// <param name="token">A token to validate</param>
        /// <param name="json">A json document</param>
        public JsonFieldsValidator(JToken token, string json)
        {
            _temp = JsonConvert.DeserializeObject(json).ToString();
            _fields = new Dictionary<string, JValue>();
            CollectFields(token);
        }


        /// <summary>
        /// Returns the validated JSON 
        /// </summary>
        /// <returns>Valid Json string</returns>
        public override string ToString()
        {
            return _temp;
        }


        private void CollectFields(JToken jToken) 
        {
            switch (jToken.Type)
            {
                case JTokenType.Object:
                    foreach (var child in jToken.Children<JProperty>())
                    {
                        var propertyName = Validate(typeof(T), child.Name);
                        SetValue(child.Name, propertyName);
                        CollectFields(child);
                    }
                    break;
                case JTokenType.Array:
                    foreach (var child in jToken.Children())
                        CollectFields(child);
                    break;
                case JTokenType.Property:
                    CollectFields(((JProperty)jToken).Value);
                    break;
                default:
                    _fields.Add(jToken.Path, (JValue)jToken);
                    break;
            }
        }


        /// <summary>
        /// Compares the values in json with the properties in the class.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="propName"></param>
        /// <returns></returns>
        internal static string Validate(Type t, string propertyName)
        {
            var props = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var type in props)
            {
                if (!IsPrimitiveType(type.PropertyType))
                {
                    if (type.GetType().IsClass)
                        foreach (var a in props)
                            propertyName = Validate(type.PropertyType, propertyName);
                }
                if (propertyName.Equals(type.Name, StringComparison.InvariantCultureIgnoreCase))
                    return type.Name;
            }
            return propertyName;
        }


        //Internal helper: Dont list properties if its a simple type. Only validate classes.
        internal static bool IsPrimitiveType(Type type)
        {
            return
                type.IsPrimitive ||
                new Type[] {
                typeof(String),
                typeof(Decimal),
                typeof(DateTime),
                typeof(DateTimeOffset),
                typeof(TimeSpan),
                typeof(Guid)
                }.Contains(type) ||
                Convert.GetTypeCode(type) != TypeCode.Object ||
                (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>) && IsPrimitiveType(type.GetGenericArguments()[0]));
        }


        internal static void SetValue(string key, string newKey)
        {
            if (_temp.IndexOf(key) > 0) {
                _temp = _temp.Replace($"\"{key}\":", $"\"{newKey}\":");
            }
        }
    }





    class ValidationResult
    {
        public bool Valid { get; private set; }
        public string Message { get; private set; }

        private ValidationResult(bool success, string message = null)
        {
            Console.WriteLine(message);
        }

        public static ValidationResult Success()
        {
            return new ValidationResult(true);
        }

        /// <summary>
        /// Add some human understandable text
        /// </summary>
        /// <returns></returns>
        public static ValidationResult Fail()
        {
            return new ValidationResult(true);
        }

        public ValidationResult WithMessage(string message)
        {
            return new ValidationResult(this.Valid, message);
        }
    }

    class ValidationContext
    {
        List<Rule> rules = new List<Rule>();

        public ValidationContext(params Rule[] rules)
        {
            this.rules = rules.ToList();
        }

        public List<ValidationResult> GetResults()
        {
            var results = new List<ValidationResult>();
            foreach (var rule in rules){
                results.Add(rule.Validate());
            }
            return results;
        }

        public void AddRule(Rule r)
        {
            this.rules.Add(r);
        }
    }



    abstract class Rule
    {
        public abstract ValidationResult Validate();

        public static Rule<TEntityType> Create<TEntityType>(TEntityType entity, Func<TEntityType, ValidationResult> rule)
        {
            Rule<TEntityType> newRule = new Rule<TEntityType>();
            newRule.rule = rule;
            newRule.entity = entity;
            return newRule;
        }
    }

    class Rule<T> : Rule
    {
        internal T entity;
        internal Func<T, ValidationResult> rule;

        public override ValidationResult Validate()
        {
            return rule(entity);
        }
    }

    public static class ValidationHelpers
    {
        public static bool IsSource(this Source source)
        {
            return !source.Equals(Source.Undefined);
        }
    }



    class Program
    {
        public static string kundeRegister = @"
        {
	        'eventtype' : 'Insert',
	        'customereventsent': '2019-01-22T23:44:00Z',
	        'lastupdated': '0001-01-01T00:00:00',
	        'createdon': '0001-01-01T00:00:00',
	        'customertype': 'Privat',
	        'source': 'Tradex',
	        'customernumber': '13055920669',
	        'distributorbanknumber': '3290',
	        'customerinformation': {
		        'firstname': 'Rickard',
		        'lastname': 'Magnusson',
		        'address1': {
			        'addressline1': 'Pipistrel House, Finsthwaite Lane',
                    'addressline2': 'Another House, Finsthwaite Lane',
			        'postalcode': 'Cumbria LA12 8QD'
		        }
	        },
	        'insurancetype': 'Undefined',
	        'ownerusername': 'D99999',
	        'ownerbanknumber': '0501',
            'customersourceid': '12'
        }";


        static void Main(string[] args)
        {

            //This will be generic: typeof(T).
            var jsonToken = JToken.Parse(kundeRegister);
            var result = new JsonFieldsValidator<KundeRegisterEvent>(jsonToken, kundeRegister);
            var xml = TypeConverter.SerializeJsonToXml<KundeRegisterEvent>(result.ToString());
            Console.Write(xml);

            var serializer = new XmlSerializer(typeof(KundeRegisterEvent)); 
            var stringReader = new StringReader(xml);
            var evt = (KundeRegisterEvent)serializer.Deserialize(stringReader);


            //Rule engine 
            //Example rules
            var FirstNameRule = Rule.Create(evt, e => !string.IsNullOrEmpty(e.CustomerInformation.FirstName) ?
                                               ValidationResult.Success().WithMessage("Passed NeedFirstNameRule") :
                                               ValidationResult.Fail().WithMessage("Rule failed. FirstName must be defined"));

            var BankNumberRule = Rule.Create(evt, e => !string.IsNullOrEmpty(e.DistributorBankNumber) ?
                                               ValidationResult.Success().WithMessage("Passed BankNumberRule") :
                                               ValidationResult.Fail().WithMessage("Rule failed, missing BankNumber"));

            var SourceRule = Rule.Create(evt, e => !e.Source.Equals(Source.Undefined) ?
                                               ValidationResult.Success().WithMessage("Passed SourceRule") :
                                               ValidationResult.Fail().WithMessage($"Rule failed, missing Source"));

            var  SourceExternalFunctionRule = Rule.Create(evt, e => e.Source.IsSource() ?
                                               ValidationResult.Success().WithMessage("Passed SourceRule") :
                                               ValidationResult.Fail().WithMessage($"Rule failed, missing Source"));
            //Define rules for KundeRegister
            Rule[] rulesToPassForKundeRegister = {
                FirstNameRule,
                BankNumberRule,
                SourceRule,
                SourceExternalFunctionRule
            };
            //Compile rules
            var Validation = new ValidationContext(rulesToPassForKundeRegister).GetResults();
          
            Console.ReadLine();
        }
    }



    [AttributeUsage(AttributeTargets.Enum)]
    public class ConvertEnumAttribute : Attribute
    {
        public string In { get; set; }
        public string Out { get; set; }
    }


    [AttributeUsage(AttributeTargets.Property)]
    public class ConvertAttribute : Attribute
    {
        public string In { get; set; }
        public string Out { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SaksHendelse
    {
        [XmlElement(IsNullable = true)]
        public Guid? Uuid { get; set; }

        [XmlElement(IsNullable = true)]
        public App App { get; set; }

        [XmlElement(IsNullable = true)]
        public Channel Fagsystem { get; set; }

        [XmlElement(IsNullable = true)]
        public Channel Channel { get; set; }

        [XmlElement(IsNullable = true)]
        public Brukersted Brukersted { get; set; }
    }

    [DataContract]
    [Serializable]
    public class App
    {
        [XmlElement(IsNullable = true)]
        public string Prosessomraade { get; set; }

        [XmlElement(IsNullable = true)]
        public int? InternId { get; set; }

        [XmlElement(IsNullable = true)]
        public string Navn { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Brukersted
    {
        [XmlElement(IsNullable = true)]
        public string Value { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Channel
    {
        [XmlElement(IsNullable = true)]
        public string Name { get; set; }
    }


    [DataContract]
    [Serializable]
    [XmlType]
    public class KundeRegisterEvent
    {
        [DataMember]
        [XmlElement(ElementName = "EventType")]
        public EventType EventType { get; set; }

        [DataMember]
        [XmlElement(ElementName = "EventId")]
        public string EventId { get; set; }

        [DataMember]
        [XmlElement(ElementName = "CustomerEventSent")]
        public DateTime CustomerEventSent { get; set; }

        [DataMember]
        [Convert(In = "lastupdated", Out = "LastUpdated")]
        [XmlElement(ElementName = "LastUpdated")]
        public DateTime LastUpdated { get; set; }

        [DataMember]
        [Convert(In = "createdon", Out = "CreatedOn")]
        [XmlElement(ElementName = "CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [DataMember]
        [Convert(In = "customertype", Out = "CustomerType")]
        [XmlElement(ElementName = "CustomerType")]
        public CustomerType CustomerType { get; set; }

        [Convert(In = "source", Out = "Source")]
        [XmlElement(ElementName = "Source")]
        public Source Source { get; set; }

        [Convert(In = "customernumber", Out = "CustomerNumber")]
        [XmlElement(ElementName = "CustomerNumber")]
        public string CustomerNumber { get; set; }

        [Convert(In = "distributorbanknumber", Out = "DistributorBankNumber")]
        [XmlElement(ElementName = "DistributorBankNumber")]
        public string DistributorBankNumber { get; set; }

        [Convert(In = "ownerbanknumber", Out = "OwnerBankNumber")]
        [XmlElement(ElementName = "OwnerBankNumber")]
        public string OwnerBankNumber { get; set; }

        [Convert(In = "customersourceid", Out = "CustomerSourceId")]
        [XmlElement(ElementName = "CustomerSourceId")]
        public string CustomerSourceId { get; set; }

        [Convert(In = "ownername", Out = "OwnerName")]
        [XmlElement(ElementName = "OwnerName")]
        public string OwnerName { get; set; }

        [Convert(In = "ownerusername", Out = "OwnerUsername")]
        [XmlElement(ElementName = "OwnerUsername")]
        public string OwnerUsername { get; set; }

        [Convert(In = "departmentnumber", Out = "DepartmentNumber")]
        [XmlElement(ElementName = "DepartmentNumber")]
        public string DepartmentNumber { get; set; }

        [Convert(In = "departmentname", Out = "DepartmentName")]
        [XmlElement(ElementName = "DepartmentName")]
        public string DepartmentName { get; set; }

        [Convert(In = "customerinformation", Out = "CustomerInformation")]
        [XmlElement(ElementName = "CustomerInformation")]
        public CustomerInfo CustomerInformation { get; set; }

        [Convert(In = "insurancetype", Out = "InsuranceType")]
        [XmlElement(ElementName = "InsuranceType")]
        public InsuranceType InsuranceType { get; set; }
    }


    [Serializable]
    [XmlType]
    [DataContract]
    public class CustomerInfo
    {

        public string FirstName { get; set; }


        public string LastName { get; set; }


        public Address Address1 { get; set; }


        public Address Address2 { get; set; }


        public Address Address3 { get; set; }


        public string Phone1 { get; set; }

        public string Phone2 { get; set; }



        public string Email1 { get; set; }


        public string Email2 { get; set; }


        public string Status { get; set; }

        public DateTime TimeOfDeath { get; set; }


        public string Segment { get; set; }


        public string IndustrialCode { get; set; }


        public string IndustrialName { get; set; }


        public string SectorCode { get; set; }


        public string SectorName { get; set; }


        public bool ShieldedCustomer { get; set; }
    }


    [DataContract]
    [Serializable]
    [XmlType]
    public class Address
    {
        [Convert(In = "type", Out = "Type")]
        [XmlElement(ElementName = "Type")]
        public string Type { get; set; }

        [Convert(In = "addressline1", Out = "AddressLine1")]
        [XmlElement(ElementName = "AddressLine1")]
        public string AddressLine1 { get; set; }

        [Convert(In = "addressline2", Out = "AddressLine2")]
        [XmlElement(ElementName = "AddressLine2")]
        public string AddressLine2 { get; set; }

        [Convert(In = "addressline3", Out = "AddressLine3")]
        [XmlElement(ElementName = "AddressLine3")]
        public string AddressLine3 { get; set; }

        [Convert(In = "addressline4", Out = "AddressLine4")]
        [XmlElement(ElementName = "FirstName")]
        public string AddressLine4 { get; set; }

        [Convert(In = "addressLine5", Out = "AddressLine5")]
        [XmlElement(ElementName = "AddressLine5")]
        public string AddressLine5 { get; set; }

        [Convert(In = "postalcode", Out = "PostalCode")]
        [XmlElement(ElementName = "PostalCode")]
        public string PostalCode { get; set; }

        [Convert(In = "state", Out = "State")]
        [XmlElement(ElementName = "State")]
        public string State { get; set; }

        [Convert(In = "county", Out = "County")]
        [XmlElement(ElementName = "County")]
        public string County { get; set; }

        [Convert(In = "city", Out = "City")]
        [XmlElement(ElementName = "City")]
        public string City { get; set; }

        [Convert(In = "country", Out = "Country")]
        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }

        [Convert(In = "countryCode", Out = "CountryCode")]
        [XmlElement(ElementName = "CountryCode")]
        public string CountryCode { get; set; }
    }


    [DataContract]
    public enum InsuranceType
    {
        Undefined = 0,
        Skade = 1,
        Person = 2
    }
    [DataContract]
    public enum EventType
    {
        Undefined = 0,
        Insert = 1,
        Delta = 2,
        Delete = 3,
        Key = 4
    }
    [DataContract]
    public enum CustomerType
    {
        Undefined = 0,
        Privat = 1,
        Landbruk = 2,
        Naering = 3
    }
    [DataContract]
    public enum Source
    {
        Undefined = 0,
        NiceSkade = 1,
        NicePerson = 2,
        Kerne = 3,
        Tradex = 4,
        BanQsoft = 5
    }

}
