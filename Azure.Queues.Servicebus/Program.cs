﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
using static System.Convert;

using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Microsoft.ServiceBus;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Configuration;
using System.Linq.Expressions;
using Azure.Queues.Process.Incoming;
using Azure.Functions.Extensions;
using Common.Enums;
using System.Reflection;
using Azure.Queues.Process.Kundehendelse;
using Common.RequestBuilders;

namespace Azure.Queues.Servicebus
{
    class Program
    {
        private static string _serviceBusConn = "";  
        private static string _serviceBustopic = "";
        private static TopicClient topicClient;

        private static string saksHendelse = @"
                    { 'uuid': '8ad4e171-f43b-4e15-98cc-bf3d38b8d0f5',
                    'app': {
                    'prosessomraade': 'FELLES',
                    'internId': 239,
                    'navn': 'FELLES_SPRING_APPLICATION'
                    },
                    'fagsystem': {
                    'name': 'TRADEX'
                    },
                    'channel': {
                    'name': 'INTRAWEB'
                    },
                    'brukersted': {
                    'value': '2050'
                    },
                    'raadgiverId': {
                    'value': 'Rickard Magnusson'
                    },
                    'sakId': '4944',
                    'sakUuid': null,
                    'sakHendelseType': {
                    'name': 'NY_SAK_OPPRETTET'
                    },
                    'sakOppdatering': null,
                    'sakStatus': 'Ufullstendig',
                    'sakType': 'Sparing',
                    'interessent': {
                    'kundeId': {
                        'value': '26037541469'
                    },
                    'fornavn': null,
                    'etternavn': null,
                    'rolle': 'Ukjent'
                    },
                    'tidspunkt': {
                    'epochSecond': 1534414639,
                    'nano': 750000000
                    },
                    'applikasjonVersjon': {
                    'versjon': '3.36.1'
                    },
                    'attributter': {
                    'Telefon': '12345678'
                    }
                }";


        public static string kundeHendelse = @"
        {
            'uuid': '1ad4e171-f43b-4e15-98cc-bf3d38b8d0f5',
            'applikasjon' : 'SPARING_ADVENT_TRADEX_WS',
            'prosessomraade' : 'Sparing',
            'fagsystem' : 'TRADEX',
            'fase' : 'ENDRET',
            'transaksjonstyp' : 'SPAREAVTALE',
            'produktnavn' : 'Sparekonto',
            'produktkode' : 'CRM_027',
            'handling' : 'UTVIDET',
            'kundeid': '30016919345',
            'brukersted': '3290',
            'channel': 'MOBILBANK',
            'tidspunkt': '2019-02-02T20:03:51.65Z',
            'verdi':'900',
            'tidligereverdi': '700',
            'annet' : 'This is a test of inserting text of type kundehendelse into crm.'
        }";


        public static string kundeRegister = @"
        {
	        'eventtype' : 'Insert',
	        'customereventsent': '2019-01-22T23:44:00Z',
	        'lastupdated': '0001-01-01T00:00:00',
	        'createdon': '0001-01-01T00:00:00',
	        'customertype': 'Privat',
	        'source': 'Tradex',
	        'customernumber': '30016919345',
	        'distributorbanknumber': '3290',
	        'customerinformation': {
		        'firstname': 'Jackie',
		        'lastname': 'Chan',
		        'address1': {
			        'addressline1': 'My first street one',
                    'addressline2': 'Another House',
			        'postalcode': '0841'
		        }
	        },
	        'insurancetype': 'Undefined',
	        'ownerusername': 'D99999',
	        'ownerbanknumber': '0501',
            'customersourceid': '12'
        }";

        string s = @"
        <EventType>Insert</EventType>
  <CustomerEventSent>2018-05-23T12:55:57.38Z</CustomerEventSent>
  <LastUpdated>2017-05-11T22:00:00Z</LastUpdated>
  <CreatedOn>0001-01-01T00:00:00</CreatedOn>
  <CustomerType>Privat</CustomerType>
  <Source>Tradex</Source>
  <CustomerNumber>13055920669</CustomerNumber>
  <DistributorBankNumber>9999</DistributorBankNumber>
  <OwnerBankNumber>0501</OwnerBankNumber>
  <CustomerSourceId>12</CustomerSourceId>
  <OwnerUsername>D99999 D99999</OwnerUsername>
  <CustomerInformation>
    <FirstName>MICHELLE RAMAGE</FirstName>
    <LastName>GOLDSMITH</LastName>
    <Address1>
      <AddressLine1>Pipistrel House, Finsthwaite Lane,</AddressLine1>
      <AddressLine2>Backbarrow</AddressLine2>
      <PostalCode>Cumbria LA12 8QD</PostalCode>
      <State />
      <Country>Storbritannia</Country>
    </Address1>
    <Phone1 />
    <Phone2>Norway</Phone2>
    <Email1 />
  </CustomerInformation>
  <InsuranceType>Undefined</InsuranceType>
    ValidateCustomerEssentials failed. The event is missing either sourceid, customer number or bank number.
";





    public static void Main(string[] args)
        {

            _serviceBusConn = ConfigurationManager.AppSettings["SbEndpoint"];

            bool keepGoing = true;
            try
            {
                do
                {
                    var selection = DisplayMenu();
                    switch (selection)
                    {
                        case 1:
                            WriteLine("Installing Topics and subscriptions...");
                            MainInstallEventGridTopicsAsync(args).GetAwaiter().GetResult();
                            break;
                        case 2:
                            WriteLine("Enter Auto read Event Grid.(local.settings.json, Appsettings (Endpoint,EventType,Secret))");
                            MainEventGridAutoTriggerAsync(args).GetAwaiter().GetResult();
                            break;
                        case 3:
                            RunRules(args).GetAwaiter().GetResult();
                            break;
                        case 4:
                            ShowMethods(args).GetAwaiter().GetResult();
                            break;
                        case 5:
                            WriteLine("Bye.");
                            keepGoing = false;
                            break;
                        default:
                            throw new InvalidOperationException("You entered an invalid option. Bye.");
                    }
                } while (keepGoing);
            }
            catch (Exception ex)
            {
                WriteLine($"Exeption occurred: {ex.Message}");
                Console.ReadLine();
            }
        }

        static public int DisplayMenu()
        {
            WriteLine();
            WriteLine(" 1. Install Servicebus Topics and subscriptions");
            WriteLine(" 2. Service Bus Topic");
            WriteLine(" 3. RulesTest");
            WriteLine(" 4. Extract methods in class");
            WriteLine(" 5. Exit");
            WriteLine(" Which would you like to trigger?  Enter '4' to exit.");
            var result = ReadLine();
            return ToInt32(result);
        }



        public static async Task ShowMethods(string[] args)
        {
            WriteLine("Enter classname:");
            var typeName = ReadLine();
            while (typeName.Length == 0)
            {
                WriteLine("Try again, this value must have a length > 0");
                WriteLine("Enter classname:");
                typeName = ReadLine();
            }

            //Assembly asm = typeof(KundeHendelseManager).Assembly;
            //Type type = asm.GetType(typeName);

            Type type = typeof(RequestBuilder);

            await Task.Run(() => {
                foreach (var method in type.GetMethods())
                {
                    var parameters = method.GetParameters();
                    var parameterDescriptions = string.Join
                        (", ", method.GetParameters()
                                     .Select(x => x.ParameterType.Name + " " + x.Name)
                                     .ToArray());

                    Console.WriteLine("public {0} {1} ({2}){{}}",
                                      method.ReturnType.Name.ToLower(),
                                      method.Name,
                                      parameterDescriptions);
                }
            });
        }

        public static async Task RunRules(string[] args)
        {
            await Task.Run(() => {
                var o = JObject.Parse(kundeRegister);
                var typeConverter = new TypeConverter();
                KundeRegisterEvent kr = typeConverter.ToObject<KundeRegisterEvent>(o.ToString());

                new PrecompiledRules().RunTest(kr);
            });
        }


        private static async Task MainInstallEventGridTopicsAsync(string[] args)
        {
            await Task.Run(() => {
                NameSpaceManagement.CreateRequiredTopics();
                WriteLine($"Topics installation done.");
            });
        }



        private static async Task MainEventGridAutoTriggerAsync(string[] args)   
        {
            WriteLine("Enter Topic key:");
            var Topic = ReadLine();
            while (Topic.Length == 0)
            {
                WriteLine("Try again, this value must have a length > 0");
                WriteLine("Enter Topic key:");
                Topic = ReadLine();
            }

            _serviceBustopic = Topic;


            WriteLine("Enter number of messages to send.");
            int ServiceBusMessagesToSend = 1;

            while (!int.TryParse(ReadLine(), out ServiceBusMessagesToSend)){
                WriteLine("Try again, this value must be numeric.");
            }

            topicClient = TopicClient.CreateFromConnectionString(_serviceBusConn, Topic);

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings{
                Formatting = Formatting.Indented
            };

            var topicItems = new List<object>();

            object typeWrapper = null;

            for (var i = 1; i <= ServiceBusMessagesToSend; i++)
            {
                string messageBody = string.Empty;

                switch (Topic)
                {
                    case "kunderegister":
                        messageBody = kundeRegister.Replace("\r\n", "");
                        break;
                    case "kundehendelse":
                        messageBody = kundeHendelse.Replace("\r\n", "");
                        break;
                    case "sakshendelse":
                        messageBody = saksHendelse.Replace("\r\n", "");
                        break;
                }

               

                EventType evt = Helpers.FromTopic(Topic);
                DateTime created = DateTime.UtcNow;

                var message = new 
                {
                    Id = Guid.NewGuid(),
                    Message = messageBody,
                    EventType = evt,
                    Type = Topic,
                    Created = created
                };


                switch (Topic)
                {
                    case "kunderegister":
                        typeWrapper = new { KundeRegister = message };
                        break;
                    case "kundehendelse":
                        typeWrapper = new { KundeHendelse = message };
                        break;
                    case "sakshendelse":
                        typeWrapper = new { SaksHendelse = message };
                        break;
                }

                topicItems.Add(message);
            }
            try
            {
                BrokeredMessage msg = null;

                foreach (var item in topicItems)
                {
                    var json = JsonConvert.SerializeObject(item);

                    WriteLine($"{json}");

                    msg = new BrokeredMessage(new MemoryStream(Encoding.UTF8.GetBytes(json)));
                    msg.MessageId = JObject.Parse(json)["Id"].ToString();
                    msg.SessionId = Guid.NewGuid().ToString();

                    await topicClient.SendAsync(msg);
                    await Task.Delay(20);
                }
            }           
            catch (Exception ex)
            {
                WriteLine($"{DateTime.Now} > Exception: {ex.Message}");
            }

            WriteLine($"{ServiceBusMessagesToSend} messages sent.");
        }  
  

        [Obsolete("Going to be removed.")]
        private static void ReadMessage()  
        {  
            var subClient = SubscriptionClient.CreateFromConnectionString(_serviceBusConn, _serviceBustopic, $"{_serviceBustopic}subscription");
            
            subClient.OnMessage(m =>  
            {  
                WriteLine($"Body: {m.GetBody<string>()}");
                m.Complete();
            });
        }  
    }




    public class PrecompiledRules
    {

        public void RunTest(KundeRegisterEvent evt)
        {

            List<KundeRegisterEvent> list = new List<KundeRegisterEvent>();
            list.Add(evt);

            Console.WriteLine(evt.Source.ToString());


            List<Rule> rules = new List<Rule>
            {
                    // Create some rules using LINQ.ExpressionTypes for the comparison operators
                    new Rule ( "DistributorBankNumber", ExpressionType.Equal, "3290"),
                    new Rule ( "Source", ExpressionType.IsTrue, "Undefined")
            };

            var kundeRegisterRules = CompileRule(list, rules);

            list.ForEach(c => {
                if (kundeRegisterRules.TakeWhile(rule => rule(c)).Count() > 0)
                {
                    WriteLine(string.Concat("Distnumber: ", c.DistributorBankNumber, " Passed the compiled rules engine check!"));
                }
                else
                {
                    WriteLine(string.Concat("Distnumber: ", c.DistributorBankNumber, " Failed the compiled rules engine check!"));
                }
            });
        }


        ///
        /// A method used to precompile rules for a provided type
        /// 
        public static List<Func<T, bool>> CompileRule<T>(List<T> targetEntity, List<Rule> rules)
        {
            var compiledRules = new List<Func<T, bool>>();

            // Loop through the rules and compile them against the properties of the supplied shallow object 
            rules.ForEach(rule =>
            {
                var genericType = Expression.Parameter(typeof(T));
                var key = MemberExpression.Property(genericType, rule.ComparisonPredicate);
                var propertyType = typeof(T).GetProperty(rule.ComparisonPredicate).PropertyType;
                var value = Expression.Constant(Convert.ChangeType(rule.ComparisonValue, propertyType));
                var binaryExpression = Expression.MakeBinary(rule.ComparisonOperator, key, value);

                compiledRules.Add(Expression.Lambda<Func<T, bool>>(binaryExpression, genericType).Compile());
            });

            // Return the compiled rules to the caller
            return compiledRules;
        }


        /// 
        public class Rule
        {
            ///
            /// Denotes the rules predictate (e.g. Name); comparison operator(e.g. ExpressionType.GreaterThan); value (e.g. "Cole")
            /// 
            public string ComparisonPredicate { get; set; }
            public ExpressionType ComparisonOperator { get; set; }
            public string ComparisonValue { get; set; }

            /// 
            /// The rule method that 
            /// 
            public Rule(string comparisonPredicate, ExpressionType comparisonOperator, string comparisonValue)
            {
                ComparisonPredicate = comparisonPredicate;
                ComparisonOperator = comparisonOperator;
                ComparisonValue = comparisonValue;
            }
        }
    }


}  
