﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Common.Entities;

namespace Common.Helpers
{
    [Serializable]
    [XmlType]
    [DataContract]
    public class CustomerInfo
    {
        [XmlElement]
        [DataMember]
        public string FirstName { get; set; }

        [XmlElement]
        [DataMember]
        public string LastName { get; set; }

        [XmlElement]
        [DataMember]
        public Address Address1 { get; set; }

        [XmlElement]
        [DataMember]
        public Address Address2 { get; set; }

        [XmlElement]
        [DataMember]
        public Address Address3 { get; set; }

        [XmlElement]
        [DataMember]
        public string Phone1 { get; set; }

        [XmlElement]
        [DataMember]
        public string Phone2 { get; set; }

        [XmlElement]
        [DataMember]
        public string Email1 { get; set; }

        [XmlElement]
        [DataMember]
        public string Email2 { get; set; }

        [XmlElement]
        [DataMember]
        public string Status { get; set; }

        [XmlElement]
        [DataMember]
        public DateTime TimeOfDeath { get; set; }

        [XmlElement]
        [DataMember]
        public string Segment { get; set; }

        [XmlElement]
        [DataMember]
        public string IndustrialCode { get; set; }

        [XmlElement]
        [DataMember]
        public string IndustrialName { get; set; }

        [XmlElement]
        [DataMember]
        public string SectorCode { get; set; }

        [XmlElement]
        [DataMember]
        public string SectorName { get; set; }
        
        [XmlElement]
        [DataMember]
        public bool ShieldedCustomer { get; set; }
    }
}
