﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Common.Entities
{
    [Serializable]
    [XmlType]
    public class SakHendelse
    {
        [XmlElement]
        //[JsonProperty(PropertyName = "uuid")]
        public string Uuid { get; set; }

        [XmlElement]
        //[JsonProperty(PropertyName = "app")]
        public string Applikasjon { get; set; }

        [XmlElement]
        //[JsonProperty(PropertyName = "applikasjonVersjon")]
        public string ApplikasjonVersjon { get; set; }

        [XmlElement]
        //[JsonProperty(PropertyName = "fagsystem")]
        public string FagSystem { get; set; }

        [XmlElement]
        //[JsonProperty(PropertyName = "channel")]
        public string Channel { get; set; }

        [XmlElement]
        //[JsonProperty(PropertyName = "prosessomraade")]
        public ProcessArea Prosessomraade { get; set; }

        [XmlElement]
        //[JsonProperty(PropertyName = "brukersted")]
        public string Bankregnr { get; set; }

        [XmlElement]
        //[JsonProperty(PropertyName = "raadgiverId")]
        public string RaadgiverId { get; set; }

        //[XmlElement]
        [JsonProperty(PropertyName = "brukersted")]
        public string RaadgiverName { get; set; }

        [XmlElement]
        public string SakUuid { get; set; }

        [XmlElement]
        public string SakId { get; set; }

        [XmlElement]
        public string SakHendelsesType { get; set; }

        [XmlElement]
        public string SakStatus { get; set; }
        [XmlElement]
        public string SakType { get; set; }
        [XmlElement]
        public string SakOppdatering { get; set; }
        [XmlElement]
        public Interessent Interessent { get; set; }
        [XmlElement]
        public DateTime Tidspunkt { get; set; }
        [XmlElement]
        public List<Attributt> Attributter { get; set; }

        [XmlElement]
        public string ProduktNavn { get; set; }
        [XmlElement]
        public string ProduktKode { get; set; }
        [XmlElement]
        public string FagsystemKundenummer { get; set; }
        [XmlElement]
        public string Verdi { get; set; }
        [XmlElement]
        public string TidligereVerdi { get; set; }
    }



    [Serializable]
    [XmlType]
    public class Interessent
    {
        [XmlElement]
        public string OffentligId { get; set; }
        [XmlElement]
        public string Rolle { get; set; }
        [XmlElement]
        public string Fornavn { get; set; }
        [XmlElement]
        public string Etternavn { get; set; }
    }

    [Serializable]
    [XmlType]
    public class Attributt
    {
        [XmlElement]
        public string Key { get; set; }
        [XmlElement]
        public string Value { get; set; }
    }

}
