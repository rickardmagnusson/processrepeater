﻿namespace Common.Enums
{
    public enum InsuranceType
    {
        Undefined = 0,
        Skade = 1,
        Person = 2
    }
}
