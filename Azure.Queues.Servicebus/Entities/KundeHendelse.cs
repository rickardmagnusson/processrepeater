﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Common.Entities
{
    [Serializable]
    [XmlType]
    public class KundeHendelse
    {
        [XmlElement]
        public string Uuid { get; set; }
        [XmlElement]
        public string Applikasjon { get; set; }
        [XmlElement]
        public string ApplikasjonVersjon { get; set; }
        [XmlElement]
        public ProcessArea ProsessOmraade { get; set; }
        [XmlElement]
        public string FagSystem { get; set; }
        [XmlElement]
        public string Fase { get; set; }
        [XmlElement]
        public string TransaksjonsType { get; set; }
        [XmlElement]
        public string ProduktNavn { get; set; }
        [XmlElement]
        public string ProduktKode { get; set; }
        [XmlElement]
        public string Handling { get; set; }
        [XmlElement]
        public string AvtaleId { get; set; }
        [XmlElement]
        public string KundeId { get; set; } // offentligid
        [XmlElement]
        public string FagsystemKundenummer { get; set; } // kildeid
        [XmlElement]
        public string Brukersted { get; set; } // bankregnr
        [XmlElement]
        public string Channel { get; set; }
        [XmlElement]
        public string InnloggetRaadgiver { get; set; }
        [XmlElement]
        public DateTime Tidspunkt { get; set; }
        [XmlElement]
        public long Verdi { get; set; }
        [XmlElement]
        public long TidligereVerdi { get; set; }
        [XmlElement]
        public string Annet { get; set; }
    }
}
