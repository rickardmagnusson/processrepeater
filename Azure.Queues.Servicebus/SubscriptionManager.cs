﻿using Microsoft.Azure.Management.EventGrid.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Servicebus
{
    public static class SubscriptionManager
    {
        private static EventGridSubscription Create(string subscriptionName, params string[] eventTypes)
        {
            var subscription = new EventGridSubscription
            {
                Name = subscriptionName,
                Prefix = "prefix",   // Subject Begins With
                Postfix = "postfix",    // Subject Ends With
                EventTypes = eventTypes
            };

            return subscription;
        }
    }

    public class EventGridSubscription
    {
        private readonly List<string> _eventTypes;

        public EventGridSubscription()
        {
            _eventTypes = new List<string>();
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("eventTypes")]
        public string[] EventTypes { get; set; }

        [JsonProperty("subjectBeginsWith")]
        public string Prefix { get; set; }

        [JsonProperty("subjectEndsWith")]
        public string Postfix { get; set; }
    }
}
