﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Servicebus
{

    /// <summary>
    /// Needs to be updated to real EventTypes to be useful.
    /// </summary>
    public enum EventType
    {
        KundeRegister = 1,
        KundeHendelse = 2,
        SaksHendelse = 3
    }


    /// <summary>
    /// Needs to be updated to real ImboxMessage to be useful.
    /// </summary>
    [DataContract]
    [Serializable]
    public class InboxMessage
    {
        public Guid Id { get; set; }
        public string MessageId { get; set; }
        public string Message { get; set; }
        public EventType EventType { get; set; }
        public string Type { get; set; }
        public DateTime Created { get; set; } = DateTime.UtcNow;


    }
}
