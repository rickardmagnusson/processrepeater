﻿using Microsoft.ServiceBus;
using System;
using System.Configuration;

namespace Azure.Queues.Servicebus
{
    public class NameSpaceManagement
    {
        public readonly string KundeRegister = "kunderegister";
        public readonly string KundeHendelse = "kundehendelse";
        public readonly string SaksHendelse = "sakshendelse";
        public readonly string EventSender = "RootManageSharedAccessKey";
        private readonly string Strategy = "subscription";
        private static string connectionString;
        private static NameSpaceManagement _instance;


        /// <summary>
        /// Singleton implementation.
        /// </summary>
        private NameSpaceManagement() { }


        /// <summary>
        /// Returns a NameSpaceManagement instance.
        /// </summary>
        /// <returns></returns>
        private static NameSpaceManagement GetInstance()
        {
            if (_instance == null)
                _instance = new NameSpaceManagement();
            return _instance;
        }


        /// <summary>
        /// Create Topics and subscriptions if there not already exists.
        /// </summary>
        public static void CreateRequiredTopics()
        {
            connectionString = ConfigurationManager.AppSettings["SbEndpoint"];
            GetInstance().CreateTopicsIfNotExist();
        }


        /// <summary>
        /// Creates topics and Subscriptuion in Servicebus
        /// </summary>
        private void CreateTopicsIfNotExist()
        {
            try {
                var namespaceManager = NamespaceManager.CreateFromConnectionString(connectionString);

                if (!namespaceManager.TopicExists(KundeRegister))
                    namespaceManager.CreateTopic(KundeRegister);
                if (!namespaceManager.SubscriptionExists(KundeRegister, $"{KundeRegister}{Strategy}"))
                    namespaceManager.CreateSubscription(KundeRegister, $"{KundeRegister}{Strategy}");
              
                if (!namespaceManager.TopicExists(KundeHendelse))
                    namespaceManager.CreateTopic(KundeHendelse);
                if (!namespaceManager.SubscriptionExists(KundeHendelse, $"{KundeHendelse}{Strategy}"))
                    namespaceManager.CreateSubscription(KundeHendelse, $"{KundeHendelse}{Strategy}");
               
                if (!namespaceManager.TopicExists(SaksHendelse))
                    namespaceManager.CreateTopic(SaksHendelse);
                if (!namespaceManager.SubscriptionExists(SaksHendelse, $"{SaksHendelse}{Strategy}"))
                    namespaceManager.CreateSubscription(SaksHendelse, $"{SaksHendelse}{Strategy}");
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
