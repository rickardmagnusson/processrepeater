﻿

namespace Azure.Queues.Servicebus
{
    public static class Helpers
    {
        public static EventType FromTopic(string topic)
        {
            topic = topic.ToLower();
            switch (topic)
            {
                case "kunderegister":
                    return EventType.KundeRegister;
                case "kundehendelse":
                    return EventType.KundeHendelse;
                default:
                    return EventType.SaksHendelse;
            }
        }
    }
}
