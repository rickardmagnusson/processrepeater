﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Azure.Queues.Process.Incoming
{
    [Serializable]
    [XmlType]
    public enum ProcessArea
    {
        Undefined = 0,
        Infra = 1,
        Dagligbank = 2,
        Eiendom = 3,
        Forsikring = 4,
        Kreditt = 5,
        Kredittbank = 6,
        Marked = 7,
        Sparing = 8,
        Virksomhet = 9,
        Felles = 10
    }
}
