﻿using System;
using System.Xml.Serialization;
using Common.Entities;
using Common.Enums;
using Common.Helpers;

namespace Azure.Queues.Process.Incoming
{
    [Serializable]
    [XmlType]
    public class KundeRegisterEvent
    {
        [Convert(In = "eventtype", Out = "EventType")]
        [XmlElement(ElementName = "EventType")]
        public EventType EventType { get; set; }

        [Convert(In = "eventid", Out = "EventId")]
        [XmlElement(ElementName = "EventId")]
        public string EventId { get; set; }

        [Convert(In = "customereventsent", Out = "CustomerEventSent")]
        [XmlElement(ElementName = "CustomerEventSent")]
        public DateTime CustomerEventSent { get; set; }

        [Convert(In = "lastupdated", Out = "LastUpdated")]
        [XmlElement(ElementName = "LastUpdated")]
        public DateTime LastUpdated { get; set; }

        [Convert(In = "createdon", Out = "CreatedOn")]
        [XmlElement(ElementName = "CreatedOn")]
        public DateTime CreatedOn { get; set; }


        [Convert(In = "customertype", Out = "CustomerType")]
        [XmlElement(ElementName = "CustomerType")]
        public CustomerType CustomerType { get; set; }

        [Convert(In = "source", Out = "Source")]
        [XmlElement(ElementName = "Source")]
        public Source Source { get; set; }

        [Convert(In = "customernumber", Out = "CustomerNumber")]
        [XmlElement(ElementName = "CustomerNumber")]
        public string CustomerNumber { get; set; }

        [Convert(In = "distributorbanknumber", Out = "DistributorBankNumber")]
        [XmlElement(ElementName = "DistributorBankNumber")]
        public string DistributorBankNumber { get; set; }

        [Convert(In = "ownerbanknumber", Out = "OwnerBankNumber")]
        [XmlElement(ElementName = "OwnerBankNumber")]
        public string OwnerBankNumber { get; set; }

        [Convert(In = "customersid", Out = "CustomerSourceId")]
        [XmlElement(ElementName = "CustomerSourceId")]
        public string CustomerSourceId { get; set; }

        [Convert(In = "ownername", Out = "OwnerName")]
        [XmlElement(ElementName = "OwnerName")]
        public string OwnerName { get; set; }

        [Convert(In = "ownerusername", Out = "OwnerUsername")]
        [XmlElement(ElementName = "OwnerUsername")]
        public string OwnerUsername { get; set; }

        [Convert(In = "departmentnumber", Out = "DepartmentNumber")]
        [XmlElement(ElementName = "DepartmentNumber")]
        public string DepartmentNumber { get; set; }

        [Convert(In = "departmentname", Out = "DepartmentName")]
        [XmlElement(ElementName = "DepartmentName")]
        public string DepartmentName { get; set; }

        [Convert(In = "customerinformation", Out = "CustomerInformation")]
        [XmlElement(ElementName = "CustomerInformation")]
        public CustomerInfo CustomerInformation { get; set; }

        [Convert(In = "insurancetype", Out = "InsuranceType")]
        [XmlElement(ElementName = "InsuranceType")]
        public InsuranceType InsuranceType { get; set; }
    }

}
