﻿using Common.Entities;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Azure.Queues.Process.Incoming
{


    [Serializable]
    [XmlType]
    public class KundeHendelse
    {
        [XmlElement(IsNullable = true)]
        [Convert(In = "uuid", Out = "Uuid")]
        public Guid? Uuid { get; set; }

        [Convert(In = "applikasjon", Out = "Applikasjon")]
        [XmlElement(ElementName = "Applikasjon")]
        public string Applikasjon { get; set; }


        [Convert(In = "applikasjonVersjon", Out = "ApplikasjonVersjon")]
        [XmlElement(ElementName = "ApplikasjonVersjon")]
        public string ApplikasjonVersjon { get; set; }


        [Convert(In = "prosessomraade", Out = "ProsessOmraade")]
        [XmlElement(ElementName = "ProsessOmraade")]
        public ProcessArea ProsessOmraade { get; set; }

        [Convert(In = "fagsystem", Out = "FagSystem")]
        [XmlElement(ElementName = "FagSystem")]
        public string FagSystem { get; set; }

        [Convert(In = "fase", Out = "Fase")]
        [XmlElement(ElementName = "Fase")]
        public string Fase { get; set; }

        [Convert(In = "transaksjonstyp", Out = "TransaksjonsType")]
        [XmlElement(ElementName = "TransaksjonsType")]
        public string TransaksjonsType { get; set; }

        [Convert(In = "produktnavn", Out = "ProduktNavn")]
        [XmlElement(ElementName = "ProduktNavn")]
        public string ProduktNavn { get; set; }

        [Convert(In = "produktkode", Out = "ProduktKode")]
        [XmlElement(ElementName = "ProduktKode")]
        public string ProduktKode { get; set; }

        [Convert(In = "handling", Out = "Handling")]
        [XmlElement(ElementName = "Handling")]
        public string Handling { get; set; }

        [Convert(In = "avtaleid", Out = "AvtaleId")]
        [XmlElement(ElementName = "AvtaleId")]
        public string AvtaleId { get; set; }

        [Convert(In = "kundeid", Out = "KundeId")]
        [XmlElement(ElementName = "KundeId")]
        public string KundeId { get; set; } // offentligid

        [Convert(In = "fagsystemkundenummer", Out = "FagsystemKundenummer")]
        [XmlElement(ElementName = "FagsystemKundenummer")]
        public string FagsystemKundenummer { get; set; } // kildeid

        [Convert(In = "brukersted", Out = "Brukersted")]
        [XmlElement(ElementName = "Brukersted")]
        public string Brukersted { get; set; } // bankregnr

        [Convert(In = "channel", Out = "Channel")]
        [XmlElement(ElementName = "Channel")]
        public string Channel { get; set; }

        [Convert(In = "innloggetraadgiver", Out = "InnloggetRaadgiver")]
        [XmlElement(ElementName = "InnloggetRaadgiver")]
        public string InnloggetRaadgiver { get; set; }

        [Convert(In = "tidspunkt", Out = "Tidspunkt")]
        [XmlElement(ElementName = "Tidspunkt")]
        public DateTime Tidspunkt { get; set; }

        [Convert(In = "verdi", Out = "Verdi")]
        [XmlElement(ElementName = "Verdi")]
        public long Verdi { get; set; }

        [Convert(In = "tidligereverdi", Out = "TidligereVerdi")]
        [XmlElement(ElementName = "TidligereVerdi")]
        public long TidligereVerdi { get; set; }

        [Convert(In = "annet", Out = "Annet")]
        [XmlElement(ElementName = "Annet")]
        public string Annet { get; set; }
    }
}
