﻿using Azure.Queues.Process.Data.Filters;
using Common.Entities;
using FluentNHibernate.Mapping;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Azure.Queues.Process.Incoming
{
    [Serializable()]
    public partial class SaksHendelse
    {
        [XmlElement(IsNullable = true)]
        [Convert(In = "uuid", Out = "Uuid")]
        public Guid? Uuid { get; set; }

        [Convert(In = "app", Out = "App")]
        [XmlElement(ElementName = "App")]
        public App App { get; set; }

        [Convert(In = "fagsystem", Out = "Fagsystem")]
        [XmlElement(ElementName = "Fagsystem")]
        [JsonProperty("fagsystem", NullValueHandling = NullValueHandling.Ignore)]
        public Channel Fagsystem { get; set; }

        [Convert(In = "channel", Out = "Channel")]
        [XmlElement(ElementName = "Channel")]
        [JsonProperty("channel", NullValueHandling = NullValueHandling.Ignore)]
        public Channel Channel { get; set; }

        [Convert(In = "brukersted", Out = "Brukersted")]
        [XmlElement(ElementName = "Brukersted")]
        [JsonProperty("brukersted", NullValueHandling = NullValueHandling.Ignore)]
        public Brukersted Brukersted { get; set; }

        [Convert(In = "raadgiverId", Out = "RaadgiverId")]
        [XmlElement(ElementName = "RaadgiverId")]
        [JsonProperty("raadgiverId", NullValueHandling = NullValueHandling.Ignore)]
        public Brukersted RaadgiverId { get; set; }

        [Convert(In = "sakId", Out = "SakId")]
        [XmlElement(ElementName = "SakId")]
        [JsonProperty("sakId", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? SakId { get; set; }

        [Convert(In = "sakUuid", Out = "SakUuid")]
        [XmlElement(ElementName = "SakUuid")]
        [JsonProperty("sakUuid")]
        public object SakUuid { get; set; }

        [Convert(In = "sakHendelseType", Out = "SakHendelseType")]
        [XmlElement(ElementName = "SakHendelseType")]
        [JsonProperty("sakHendelseType", NullValueHandling = NullValueHandling.Ignore)]
        public Channel SakHendelseType { get; set; }

        [Convert(In = "sakOppdatering", Out = "SakOppdatering")]
        [XmlElement(ElementName = "SakOppdatering")]
        [JsonProperty("sakOppdatering")]
        public object SakOppdatering { get; set; }

        [Convert(In = "sakStatus", Out = "SakStatus")]
        [XmlElement(ElementName = "SakStatus")]
        [JsonProperty("sakStatus", NullValueHandling = NullValueHandling.Ignore)]
        public string SakStatus { get; set; }

        [Convert(In = "sakType", Out = "SakType")]
        [XmlElement(ElementName = "SakType")]
        [JsonProperty("sakType", NullValueHandling = NullValueHandling.Ignore)]
        public string SakType { get; set; }

        [Convert(In = "interessent", Out = "Interessent")]
        [XmlElement(ElementName = "Interessent")]
        [JsonProperty("interessent", NullValueHandling = NullValueHandling.Ignore)]
        public Interessent Interessent { get; set; }

        [Convert(In = "tidspunkt", Out = "Tidspunkt")]
        [XmlElement(ElementName = "Tidspunkt")]
        [JsonProperty("tidspunkt", NullValueHandling = NullValueHandling.Ignore)]
        public Tidspunkt Tidspunkt { get; set; }

        [Convert(In = "applikasjonVersjon", Out = "ApplikasjonVersjon")]
        [XmlElement(ElementName = "ApplikasjonVersjon")]
        [JsonProperty("applikasjonVersjon", NullValueHandling = NullValueHandling.Ignore)]
        public ApplikasjonVersjon ApplikasjonVersjon { get; set; }

        [Convert(In = "attributter", Out = "Attributter")]
        [XmlElement(ElementName = "Attributter")]
        [JsonProperty("attributter", NullValueHandling = NullValueHandling.Ignore)]
        public Attributter Attributter { get; set; }
    }


    [Serializable()]
    public partial class App
    {
        [Convert(In = "prosessomraade", Out = "Prosessomraade")]
        [XmlElement(ElementName = "Prosessomraade")]
        [JsonProperty("prosessomraade", NullValueHandling = NullValueHandling.Ignore)]
        public string Prosessomraade { get; set; }

        [Convert(In = "internId", Out = "InternId")]
        [XmlElement(ElementName = "InternId")]
        [JsonProperty("internId", NullValueHandling = NullValueHandling.Ignore)]
        public long? InternId { get; set; }

        [Convert(In = "navn", Out = "Navn")]
        [XmlElement(ElementName = "Navn")]
        [JsonProperty("navn", NullValueHandling = NullValueHandling.Ignore)]
        public string Navn { get; set; }
    }


    [Serializable()]
    public partial class ApplikasjonVersjon
    {
        [Convert(In = "versjon", Out = "Versjon")]
        [XmlElement(ElementName = "Versjon")]
        [JsonProperty("versjon", NullValueHandling = NullValueHandling.Ignore)]
        public string Versjon { get; set; }
    }


    [Serializable()]
    public partial class Attributter
    {
        [Convert(In = "Telefon", Out = "Telefon")]
        [XmlElement(ElementName = "Telefon")]
        [JsonProperty("Telefon", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Telefon { get; set; }

        [Convert(In = "key", Out = "Key")]
        [XmlElement(ElementName = "Key")]
        [JsonProperty("Key", NullValueHandling = NullValueHandling.Ignore)]
        public string Key { get; set; }

        [Convert(In = "value", Out = "Value")]
        [XmlElement(ElementName = "Value")]
        [JsonProperty("Value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

    }


    [Serializable()]
    public partial class Brukersted
    {
        [Convert(In = "value", Out = "Value")]
        [XmlElement(ElementName = "Value")]
        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }
    }


    [Serializable()]
    public partial class Channel
    {
        [Convert(In = "name", Out = "Name")]
        [XmlElement(ElementName = "Name")]
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
    }


    [Serializable()]
    public partial class Interessent
    {
        [Convert(In = "kundeId", Out = "KundeId")]
        [XmlElement(ElementName = "KundeId")]
        [JsonProperty("kundeId", NullValueHandling = NullValueHandling.Ignore)]
        public Brukersted KundeId { get; set; }

        [Convert(In = "fornavn", Out = "Fornavn")]
        [XmlElement(ElementName = "Fornavn")]
        [JsonProperty("fornavn")]
        public string Fornavn { get; set; }

        [Convert(In = "etternavn", Out = "Etternavn")]
        [XmlElement(ElementName = "Etternavn")]
        [JsonProperty("etternavn")]
        public string Etternavn { get; set; }

        [Convert(In = "rolle", Out = "Rolle")]
        [XmlElement(ElementName = "Rolle")]
        [JsonProperty("rolle", NullValueHandling = NullValueHandling.Ignore)]
        public string Rolle { get; set; }
    }


    [Serializable()]
    public partial class Tidspunkt
    {
        [Convert(In = "epochSecond", Out = "EpochSecond")]
        [XmlElement(ElementName = "EpochSecond")]
        [JsonProperty("epochSecond", NullValueHandling = NullValueHandling.Ignore)]
        public long? EpochSecond { get; set; }

        [Convert(In = "nano", Out = "Nano")]
        [XmlElement(ElementName = "Nano")]
        [JsonProperty("nano", NullValueHandling = NullValueHandling.Ignore)]
        public long? Nano { get; set; }
    }

}
