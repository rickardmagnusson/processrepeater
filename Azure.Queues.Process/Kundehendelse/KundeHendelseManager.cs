﻿using System;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Initializer;
using Common.Logging;
using Azure.Queues.Process.ProcessEntityHelper;
using Azure.Queues.Process.ProcessHelpers;
using System.Text;

namespace Azure.Queues.Process.Kundehendelse
{
    public class KundeHendelseManager
    {
        #region Private Members

        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("kundehendelse-manager");
        static StringBuilder Logger;

        #endregion

        #region Public Methods

        public static void HandleKundeHendelseEvent(KundeHendelse evt, ProcessedCustomerResponse response, QueryExpressionHelper query, StringBuilder logger)
        {
            Logger = logger;
            try
            {
                var cust = ValidateAndPrepare(evt, response, query);
                if (cust == null) return;


                Logger.AppendLine("Done with ValidateAndPrepare.");

                var crmCustomerCard = ProcessHelper.GetCustomerCard(evt, cust, response, query);
                if (response.FailedProcessing) return;

                Log.Debug("Done with GetCustomerCard.");

                ProcessHelper.GetOwnerIfExist(crmCustomerCard, cust);

                Logger.AppendLine("Done with GetOwnerIfExist.");

                if (cust.CustomerId == Guid.Empty)
                {
                    ProcessHelper.CreatePotentialCustomer(evt, cust, response, query);
                    if (response.FailedProcessing) return;
                }
                else ProcessHelper.UpdateCustomerSourceId(crmCustomerCard, evt.FagSystem, evt.FagsystemKundenummer, response, query, true);

                Logger.AppendLine("Done with CreatePotentialCustomer.");

                var success = ProductManager.UpsertProduct(evt, cust, response, query, Log);
                #region return failure
                if (!success)
                {
                    response.ErrorMessage = "Error during CreateOrUpdateProduct.";
                    return;
                }
                #endregion

                Logger.AppendLine("Done with CreateOrUpdateProduct.");


                //if (evt.ProsessOmraade == ProcessArea.Sparing && Helper.ApplicationIsFromRaadgiverPluss(evt.Applikasjon, Initialize.KundehendelseAppNames))
                //{
                //    ProcessHelper.CreateOutcomeForSparingAndRadgiverPluss(evt, cust, response, query, Log);
                //    if (response.FailedProcessing) return;
                //}
                //else 
                if (evt.ProsessOmraade == ProcessArea.Sparing)
                {
                    ProcessHelper.CreateOutcomeForSparing(evt, cust, response, query, Log);
                    if (response.FailedProcessing) return;
                }
                else if (evt.ProsessOmraade == ProcessArea.Forsikring)
                {
                    ProcessHelper.CreateOutcomeForInsurance(evt, cust, response, query, Log);
                    if (response.FailedProcessing) return;
                }
                else
                {
                    Helper.SetLogAndResponse($"Skipping any outcome of the 'kundehendelse' due to process area {evt.ProsessOmraade} or {evt.Applikasjon} is not yet supported.", response, LogLevel.Warn, Log, true);
                    return;
                }

                Logger.AppendLine("Done with CreateOutcome.");

                //Need to reach here...
                response.Success = true;
            }
            catch (Exception ex)
            {
                Logger.AppendLine($"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}");
            }
        }

        #endregion

        #region Private  Methods
        private static CustomerExtra ValidateAndPrepare(KundeHendelse evt, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var bankId = query.GetBankIdByBankNo(evt.Brukersted);
            if (!ProcessHelper.ValidateBank(bankId, evt.Brukersted, response, Log))
            {
                response.ErrorMessage = "Kundehendelse not valid. Can't find 'Brukersted' in Crm.";
                return null;
            }

            var cust = new CustomerExtra
            {
                BankId = bankId,
                EntityName = Helper.GetEntityName(evt),
                LoggedInUserOwnerId = query.GetUserIdByUsernameAndBankId(evt.InnloggetRaadgiver, bankId),
                DefaultTeamId = query.GetDefaultTeamIdByBankId(bankId),
                SubjectId = query.GetSubjectIdByTitle(Helper.MapSubject(evt.ProsessOmraade, evt.FagSystem)),
                PriceListId = query.GetPriceListIdByTitle(Helper.MapSubject(evt.ProsessOmraade, evt.FagSystem)),
                VendorBankNumber = Helper.GetVendorBankNumber(evt.FagSystem),
                Product = query.GetProductByCode(evt.ProduktKode)
            };

            if (cust.EntityName == EntityName.Undefined)
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Can't find a valid EntityName from 'KundeId'.", response, LogLevel.Warn, Log, true);
                Logger.AppendLine($"Kundehendelse not valid. Can't find a valid EntityName from 'KundeId'. {response}");
                return null;
            }
            if (cust.SubjectId == Guid.Empty)
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Can't find 'Subject' (ProsessOmraade) in Crm.", response, LogLevel.Warn, Log);
                Logger.AppendLine($"Kundehendelse not valid. Can't find 'Subject' (ProsessOmraade) in Crm. {response}");
                return null;
            }
            if (cust.PriceListId == Guid.Empty)
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Can't find 'Price List' (ProsessOmraade) in Crm.", response, LogLevel.Warn, Log);
                Logger.AppendLine($"Kundehendelse not valid. Can't find 'Price List' (ProsessOmraade) in Crm. {response}");
                return null;
            }
            //if (string.IsNullOrEmpty(evt.ProduktKode))
            //{
            //    Helper.SetLogAndResponse("Kundehendelse not valid. 'ProduktKode' is missing.", response, LogLevel.Warn, Log);
            //    return null;
            //}
            if (cust.Product.Id != Guid.Empty && !cust.Product.Active)
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Product is inactive in Crm.", response, LogLevel.Warn, Log);
                Logger.AppendLine($"Kundehendelse not valid. Can't find 'Price List' (ProsessOmraade) in Crm. {response}");
                return null;
            }
            if (string.IsNullOrEmpty(cust.VendorBankNumber))
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Can't find 'VendorBankNumber' given 'FagSystem'.", response, LogLevel.Warn, Log);
                Logger.AppendLine($"Kundehendelse not valid. Can't find 'VendorBankNumber' given 'FagSystem'. {response}");
                return null;
            }


            if (!Helper.ValidateKundehendelseAppNames(Initialize.KundehendelseAppNames))
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. KundehendelseAppNames in 'ETL_Config' is missing one of the Enum defined AppNames in this code.", response, LogLevel.Warn, Log);
                Logger.AppendLine($"Kundehendelse not valid. KundehendelseAppNames in 'ETL_Config' is missing one of the Enum defined AppNames in this code. {response}");
                return null;
            }


            if (!(evt.ProsessOmraade == ProcessArea.Sparing && Helper.ApplicationIsFromWeb(evt.Applikasjon, Initialize.KundehendelseAppNames)) &&
                //!(evt.ProsessOmraade == ProcessArea.Sparing && Helper.ApplicationIsFromSource(evt.Applikasjon, Initialize.KundehendelseAppNames)) &&
                //!(evt.ProsessOmraade == ProcessArea.Sparing && Helper.ApplicationIsFromRaadgiverPluss(evt.Applikasjon, Initialize.KundehendelseAppNames)) &&
                !(evt.ProsessOmraade == ProcessArea.Forsikring && Helper.ApplicationIsFromWeb(evt.Applikasjon, Initialize.KundehendelseAppNames)))
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. 'ProsessOmraade' and 'Applikasjon' is not within the defined values.", response, LogLevel.Warn, Log);
                Logger.AppendLine($"Kundehendelse not valid. 'ProsessOmraade' and 'Applikasjon' is not within the defined values. {response}");
                return null;
            }

            Logger.AppendLine($"ValidateAndPrepare complete. {response}");
            Helper.SetLogAndResponse("ValidateAndPrepare complete.", response, LogLevel.Warn, Log);

            return cust;
        }
        #endregion

    }
}
