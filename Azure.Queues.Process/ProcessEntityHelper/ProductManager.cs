﻿using System;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Helpers;
using Common.Initializer;
using Common.Logging;
using Common.RequestBuilders;
using Microsoft.Xrm.Sdk;

namespace Azure.Queues.Process.ProcessEntityHelper
{
    public class ProductManager
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("product-manager");

        public static bool UpsertProduct(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, MsmqLogger log)
        {
            if (string.IsNullOrEmpty(evt.ProduktKode)) return true;

            var result = true;
            //var product = query.GetProductByCode(evt.ProduktKode);
            cust.VendorBankId = query.GetBankIdByBankNo(cust.VendorBankNumber);
            if (cust.Product.Id == Guid.Empty)
            {
                result = PerformCreateProduct(evt, cust, response, query, log);
            }
            else
            {
                if (cust.Product.VendorId != cust.VendorBankId)
                {
                    evt.ProduktKode = $"{evt.FagSystem}_{evt.ProduktKode}";
                    log.Warn($"There is a duplicate of product code between product companies. Will try to use: '{evt.ProduktKode}' instead.");

                    cust.Product = query.GetProductByCode(evt.ProduktKode);
                    if (cust.Product.Id == Guid.Empty)
                    {
                        result = PerformCreateProduct(evt, cust, response, query, log);
                    }
                    else if (CriteriaToUpdateProduct(evt, cust.Product))
                    {
                        result = PerformUpdateProduct(evt, cust, response, query, cust.Product.Id);
                    }
                }
                else if (CriteriaToUpdateProduct(evt, cust.Product))
                {
                    result = PerformUpdateProduct(evt, cust, response, query, cust.Product.Id);
                }
            }

            if (CriteriaToInsertPriceList(cust, query))
                result = PerformInsertPriceListItem(evt, cust, response, query);

            return result;
        }
        //public static bool UpsertProduct(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, MsmqLogger log)
        //{
        //    if(string.IsNullOrEmpty(evt.ProduktKode)) return true;

        //    var result = true;
        //    //var product = query.GetProductByCode(evt.ProduktKode);
        //    cust.VendorBankId = query.GetBankIdByBankNo(cust.VendorBankNumber);
        //    if (cust.Product.Id == Guid.Empty)
        //    {
        //        result = PerformCreateProduct(evt, cust, response, query, log);
        //    }
        //    else
        //    {
        //        if (cust.Product.VendorId != cust.VendorBankId)
        //        {
        //            evt.ProduktKode = $"{evt.FagSystem}_{evt.ProduktKode}";
        //            log.Warn($"There is a duplicate of product code between product companies. Will try to use: '{evt.ProduktKode}' instead.");

        //            cust.Product = query.GetProductByCode(evt.ProduktKode);
        //            if (cust.Product.Id == Guid.Empty)
        //            {
        //                result = PerformCreateProduct(evt, cust, response, query, log);
        //            }
        //            else if (CriteriaToUpdateProduct(evt, cust.Product))
        //            {
        //                result = PerformUpdateProduct(evt, cust, response, query, cust.Product.Id);
        //            }
        //        }
        //        else if (CriteriaToUpdateProduct(evt, cust.Product))
        //        {
        //            result = PerformUpdateProduct(evt, cust, response, query, cust.Product.Id);
        //        }
        //    }

        //    if (CriteriaToInsertPriceList(cust, query))
        //        result = PerformInsertPriceListItem(evt, cust, response, query);

        //    return result;
        //}

        public static ProductInfo GetFamilyProductByVendorBankNumber(string vendorBankNumber, QueryExpressionHelper query)
        {
            var familyProductCode = Initialize.ProductCodesByBank[vendorBankNumber];
            return query.GetProductByCode(familyProductCode);
        }

        private static bool PerformCreateProduct(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, MsmqLogger log)
        {
            if (!Initialize.ProductCodesByBank.ContainsKey(cust.VendorBankNumber))
            {
                log.Error($"Family product code is missing in ETL_Config for vendor bank number: {cust.VendorBankNumber}. Dismissing event.");
                response.IsIgnored = true;
                return false;
            }

            var familyProductCode = Initialize.ProductCodesByBank[cust.VendorBankNumber];

            var familyProduct = query.GetProductByCode(familyProductCode);
            if (familyProduct.Id == Guid.Empty)
            {
                log.Warn($"Family product code is not existing in CRM: {familyProductCode}. Gonna retry event.");
                return false;
            }

            var unitGroupId = query.GetDefaultUnitGroupId();
            var primaryUnitId = query.GetPrimaryUnitId();

            cust.Product.Name = Helper.CapitalizeFirstLetter(evt.ProduktNavn);
            cust.Product.Number = evt.ProduktKode;
            var ent = RequestBuilder.BuildCreateProduct(evt, cust, familyProduct.Id, unitGroupId, primaryUnitId);

            cust.Product.Id = query.CreateEntity(ent, response);
            cust.Product.PrimaryUomId = primaryUnitId;

            if (cust.Product.Id != Guid.Empty) log.Info($"Created product for '{evt.ProsessOmraade}'. Product: '{evt.ProduktNavn}' ({evt.ProduktKode})");

            var priceListItem = RequestBuilder.BuildCreateProductPriceLevel(evt, cust, cust.Product.Id, unitGroupId, primaryUnitId);
            query.CreateEntity(priceListItem, response);

            ent.Id = cust.Product.Id;
            ent["statecode"] = new OptionSetValue(0);

            return query.UpdateEntity(ent, response);
        }
        private static bool PerformUpdateProduct(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, Guid productId)
        {
            return true;

            //var ent = RequestBuilder.BuildUpdateProduct(productId, evt.ProduktNavn);
            //var success = query.UpdateEntity(ent, response);

            //if (success) cust.Product.Name = evt.ProduktNavn;

            //return success;
        }

        private static bool PerformInsertPriceListItem(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var unitGroupId = query.GetDefaultUnitGroupId();
            var primaryUnitId = query.GetPrimaryUnitId();

            var priceListItem = RequestBuilder.BuildCreateProductPriceLevel(evt, cust, cust.Product.Id, unitGroupId, primaryUnitId);
            Log.Debug($"{Util.GetMethodName()}. Creating.");
            var priceListItemId = query.CreateEntity(priceListItem, response);

            return (priceListItemId != Guid.Empty);
        }

        private static bool CriteriaToUpdateProduct(KundeHendelse evt, ProductInfo product)
        {
            return !string.IsNullOrEmpty(evt.ProduktNavn) && !product.Name.Equals(evt.ProduktNavn);
        }

        private static bool CriteriaToInsertPriceList(CustomerExtra cust, QueryExpressionHelper query)
        {
            var priceListItemId = query.GetPriceListItemId(cust.PriceListId, cust.Product.Id);
            return priceListItemId == Guid.Empty;
        }
    }
}
