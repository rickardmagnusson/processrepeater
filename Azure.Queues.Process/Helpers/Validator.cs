﻿using Azure.Functions.Extensions;
using Common.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Process.Helpers
{
    public class Validator
    {
        public static ValidationMessage Validate(KundeRegisterEvent evt)
        {

            var FirstNameRule = Rule.Create(evt, e => !string.IsNullOrEmpty(e.CustomerInformation.FirstName) ?
                                               ValidationResult.Success().WithMessage("Passed NeedFirstNameRule") :
                                               ValidationResult.Fail().WithMessage("Rule failed. FirstName must be defined"));

            var BankNumberRule = Rule.Create(evt, e => !string.IsNullOrEmpty(e.DistributorBankNumber) ?
                                               ValidationResult.Success().WithMessage("Passed BankNumberRule") :
                                               ValidationResult.Fail().WithMessage("Rule failed, missing BankNumber"));

            var SourceRule = Rule.Create(evt, e => !e.Source.IsSource() ?
                                               ValidationResult.Success().WithMessage("Passed SourceRule") :
                                               ValidationResult.Fail().WithMessage($"Rule failed, missing Source"));


            //Define rules for KundeRegister
            Rule[] rulesToPassForKundeRegister = {
                FirstNameRule,
                BankNumberRule,
                SourceRule
            };

            //Compile rules
            var Validation = new ValidationContext(rulesToPassForKundeRegister).GetResults();
            foreach(var rule in Validation)
            {
                if (!rule.Valid)
                    return new ValidationMessage { Result = false, Message = rule.Message };
            }

            return null;
        }
    }

    public static class Helpers
    {
        public static bool IsSource(this Source source)
        {
            return !source.Equals(Source.Undefined);
        }
    }


    public class ValidationMessage
    {
        public bool Result { get; set; }
        public string Message { get; set; }
    }

}
