﻿using System;
using System.Text;
using Common.Enums;
using Common.Models;
using Newtonsoft.Json.Linq;
using Azure.Functions.Extensions;
using Common.Entities;

namespace Azure.Queues.Process.Helpers
{
    public static class EventHandler
    {
        /// <summary>
        /// Parse the incomming Json to a InboxMessage.
        /// Internally converts the Message param to an Xml
        /// dependent of the json sent.
        /// </summary>
        /// <param name="json">The string of Json sent</param>
        /// <param name="log">Logging</param>
        /// <returns>A InboxMessage</returns>
        public static MsmqMessageInbox CreateEvent(this string json, StringBuilder log, EventMessageType eventType)
        {
            MsmqMessageInbox inboxMessage = null;

            try { 
                var o = JObject.Parse(json);
                var typeConverter = new TypeConverter();

                var data = o.SelectToken("Message").ToString();
                var bankNumber = string.Empty;
                var customerNumber = string.Empty;
                var subType = 0;
                var sourceId = string.Empty;
                var source = string.Empty;

                switch (eventType)
                {
                    case EventMessageType.KundeRegisterEvent:

                        KundeRegisterEvent kundeRegister = typeConverter.ToObject<KundeRegisterEvent>(data);
                        bankNumber = kundeRegister.DistributorBankNumber;
                        customerNumber = kundeRegister.CustomerNumber;
                        subType = (int)kundeRegister.EventType;
                        sourceId = kundeRegister.CustomerSourceId;
                        source = kundeRegister.Source.ToString();

                        break;

                    case EventMessageType.KundeHendelseEvent:

                        var kundeHendelse = typeConverter.ToObject<KundeHendelse>(data);
                        bankNumber = kundeHendelse.Brukersted;
                        customerNumber = kundeHendelse.KundeId;
                        subType = 1;
                        sourceId = kundeHendelse.FagsystemKundenummer;
                        source = kundeHendelse.FagSystem;
                        break;

                    case EventMessageType.SakHendelseEvent:

                        SakHendelse sakHendelse = typeConverter.ToObject<SakHendelse>(data);
                        bankNumber = sakHendelse.Bankregnr;
                        customerNumber = sakHendelse.Interessent.OffentligId != null ? sakHendelse.Interessent.OffentligId : string.Empty;
                        sourceId = sakHendelse.FagSystem ?? string.Empty;
                        source = sakHendelse.FagSystem;
                        subType = 1;
                        break;
                 }

                log.AppendLine($"{bankNumber}, {customerNumber}, {eventType}, {sourceId}, {source} \n {typeConverter.Xml}");

                inboxMessage = new MsmqMessageInbox(bankNumber, customerNumber, typeConverter.Xml, eventType, subType, sourceId, source);
            }
            catch (Exception ex)
            {
                log.AppendLine($"Failed to create InboxMessage: {ex}");
            }
            return inboxMessage;
        }


        /// <summary>
        /// Get a subject from Json object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>string Subject</returns>
        public static string GetSubject(this JObject obj)
        {
            var subject = JObject.Parse(obj.ToString());
            return Convert.ToString(subject["subject"]);
        }
    }
}
