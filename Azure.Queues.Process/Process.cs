﻿using System;
using System.Text;
using Azure.Queues.Process.Helpers;
using Azure.Queues.Process.Data;
using Common.Models;
using Common.Enums;

namespace Azure.Queues.Process
{
    /// <summary>
    /// Contains logic for processing InboxMessages
    public class Process
    {

        /// <summary>
        /// Check if there's more items to process in InboxMessages table.
        /// </summary>EventProcessorFunc
        /// <returns>True if InboxMessages has items</returns>
        public static bool HasNext(StringBuilder log)
        {
            var session = SessionManager.Open(log);
            if (session == null){
                throw new Exception($"SessionManager is not ready: {session.IsOpen}");
            }

            var inbox = session.Rows<MsmqMessageInbox>();

            log.AppendLine($"Current count in inbox: {inbox.ToString()}");
            return inbox > 0;
        }


        /// <summary>
        /// Insert a new InboxMessage
        /// </summary>
        /// <param name="item">The items(Json) string</param>
        /// <param name="type">Type of items to insert (EventType)</param>
        /// <param name="log">Logger</param>
        /// <returns>Logger string</returns>
        public static string Insert(string item, EventMessageType type, StringBuilder log)
        {
            var inboxMessage = item.CreateEvent(log, type);
            var session = SessionManager.Open(log);

            if (inboxMessage != null)
            {
                log.AppendLine($"Trying to add new {type}");

                session.WithTransaction(s =>
                {
                    s.Save(inboxMessage);
                    s.Flush();
                    log.AppendLine($"Insterted row: {inboxMessage.Id}");
                }, log);
            }

            return log.ToString();
        }

        /// <summary>
        /// Moves items of type InboxMessage to 1. InboxMessageCompleted or InboxMessageError if failed.
        /// 1. (If the current item is created in CRM)
        /// </summary>
        //public static void MoveItems(StringBuilder log)
        //{
        //    var session = SessionManager.Open(log);
        //    if (session != null) { 
        //        log.AppendLine("Connected to session...");
        //        throw new Exception($"SessionManager is not ready: {session.IsOpen}");
        //    }
        //    var items = session
        //        .All<MsmqMessageInbox>()
        //        .OrderBy(o => o.Id)
        //        .Take((int)BatchSize.Default);

        //    if (items.Any())
        //    {
        //        //Here Im going to hook onto runbatch
        //        var crmProcess = new CrmProcess();

        //        foreach (var inboxItem in items)
        //        {
        //            if (crmProcess.RequestInsert(inboxItem)) // If this item is added in CRM successfully. (A random bool)
        //            {
        //                var completedItem = inboxItem.Cast<MsmqCompletedMessages>(); //Copy values

        //                //Insert success 
        //                session.WithTransaction(s =>
        //                {
        //                    s.Save<MsmqCompletedMessages>(completedItem);
        //                    s.Flush();
        //                    log.AppendLine($"Save InboxMessageCompleted: {completedItem.Id}");
        //                }, log);

        //                //Delete item from Inbox
        //                session.WithTransaction(s =>
        //                {
        //                    s.Delete(inboxItem);
        //                    s.Flush();
        //                }, log);
        //            }
        //            else
        //            {
        //                if (inboxItem.RetryCount > (int)RetryCounter.Default) //Could occur if some service is down.
        //                {
        //                    var errorItem = inboxItem.Cast<MsmqFailedMessages>(); //Copy values
        //                    errorItem.RetryCount++;

        //                    //If RetryCount > Default(5 times), move it to InboxMessageErrors
        //                    session.WithTransaction(s =>
        //                    {
        //                        s.Save<MsmqFailedMessages>(errorItem);
        //                        s.Flush();
        //                        log.AppendLine($"Save from InboxErrorMessage: {errorItem.Id}");
        //                    }, log);

        //                    //Delete from Inbox
        //                    session.WithTransaction(s =>
        //                    {
        //                        log.AppendLine($"Delete from Inbox: {errorItem.Id}");
        //                        s.Delete(inboxItem);
        //                        s.Flush();
        //                    }, log);
        //                }
        //                else
        //                {
        //                    session.WithTransaction(s =>
        //                    {
        //                        //Update RetryCount.
        //                        inboxItem.RetryCount++;
        //                        s.Update<MsmqMessageInbox>(inboxItem);
        //                        s.Flush();
        //                        log.AppendLine($"RetryCount: {inboxItem.Id} : {inboxItem.RetryCount}");
        //                    }, log);
        //                }
        //            }
        //        }
        //    }
        //}



    }
}
