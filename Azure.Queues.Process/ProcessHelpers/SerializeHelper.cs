﻿using System.IO;
using System.Xml.Serialization;


namespace Azure.Queues.Process.ProcessHelpers
{
    public class SerializeHelper
    {
        public static T DeserializeEventMessageContent<T>(string xml) where T : class, new()
        {
            using (var reader = new StringReader(xml))
            {
                var type = typeof(T);
                return (T)new XmlSerializer(typeof(T)).Deserialize(reader);
            }
        }
    }
}