﻿using System;
using Common.Enums;
using DataAccess.Models;

namespace DataAccess.Models
{
    public static class MsmqMessageInboxExtensions
    {

        /// <summary>
        /// Cast a MsmqFailedMessages to new MsmqFailedMessages
        /// </summary>
        /// <returns>MsmqFailedMessages message</returns>
        public static MsmqFailedMessages ToFailedMessage(this MsmqMessageInbox m)
        {
            return new MsmqFailedMessages
            {
                Id = m.Id,
                BankId = m.BankId,
                EventType = m.EventType,
                ActionType = m.ActionType,
                LastUpdated = m.LastUpdated,
                MessageContent = m.MessageContent,
                Received = m.Received,
                RetryCount = m.RetryCount,
                Status = m.Status,
                CustomerNumber = m.CustomerNumber,
                ErrorMessage = m.ErrorMessage,
                SourceId = m.SourceId,
                Source = m.Source
            };
        }


        /// <summary>
        /// MsmqCompletedMessages to new MsmqCompletedMessages
        /// </summary>
        /// <returns></returns>
        public static MsmqCompletedMessages ToCompletedMessage(this MsmqMessageInbox m)
        {
            return new MsmqCompletedMessages
            {
                Id = m.Id,
                BankId = m.BankId,
                EventType = m.EventType,
                ActionType = m.ActionType,
                LastUpdated = m.LastUpdated,
                MessageContent = m.MessageContent,
                Received = m.Received,
                RetryCount = m.RetryCount,
                Status = m.Status,
                CustomerNumber = m.CustomerNumber,
                ErrorMessage = m.ErrorMessage,
                SourceId = m.SourceId,
                Source = m.Source
            };
        }



        public static MsmqCompletedMessages ToCompletedMessage(this MsmqFailedMessages m)
        {
            return new MsmqCompletedMessages
            {
                Id = m.Id,
                BankId = m.BankId,
                EventType = m.EventType,
                ActionType = m.ActionType,
                LastUpdated = m.LastUpdated,
                MessageContent = m.MessageContent,
                Received = m.Received,
                RetryCount = m.RetryCount,
                Status = m.Status,
                CustomerNumber = m.CustomerNumber,
                ErrorMessage = m.ErrorMessage,
                SourceId = m.SourceId,
                Source = m.Source
            };
        }
    }
}
