﻿using System;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using Azure.Queues.Process.Data;
using Common.Enums;
using Common.Logging;
using DataAccess.Models;

namespace DataAccess
{

    /// <summary>
    /// Contains logic for creating InboxMessages
    /// </summary>
    public class DataWriter
    {
        private static StringBuilder Logger;

        private DataWriter()
        {
        }


        /// <summary>
        /// Create a new instance of DataWriter
        /// </summary>
        /// <param name="logger"></param>
        public DataWriter(StringBuilder logger)
        {
            Logger = logger;
        }


        /// <summary>
        /// Insert a new Message to inbox
        /// </summary>
        /// <param name="message"></param>
        /// <param name="Log"></param>
        public void InsertMessage(MsmqMessageInbox message, MsmqLogger Log)
        {
            using (var db = SessionManager.Open(Logger))
            {
                try
                {
                    db.WithTransaction(s=> s.Save(message));
                }
                catch (Exception ex)
                {
                    Log.Error($"Failed during insert: {ex.Message}");
                }
            }
        }


        /// <summary>
        /// Set status for th current message
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedStatus"></param>
        /// <param name="errorMessage"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        public bool SetStatusForMessage(long id, MessageInboxStatus updatedStatus, string errorMessage, EventOperation operation = EventOperation.Import)
        {
            using (var db = SessionManager.Open(Logger))
            {
                var result = false;

                using (var transaction = db.BeginTransaction())
                {
                    try
                    {
                        switch (operation)
                        {
                            case EventOperation.Import:
                                result = UpdateInboxMessage(id, updatedStatus, errorMessage);
                                break;
                            case EventOperation.Retry:
                                result = UpdateRetryMessage(id, updatedStatus);
                                break;
                        }
                        transaction.Commit();

                        return result;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }


        /// <summary>
        /// If something goes wrong dont delete the message just update the Retrycount
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedStatus"></param>
        /// <returns></returns>
        private static bool UpdateRetryMessage(long id, MessageInboxStatus updatedStatus)
        {
            var db = SessionManager.Open(Logger);

            var failedEntry = db.All<MsmqFailedMessages>().SingleOrDefault(m => m.Id == id);

            if (failedEntry != null)
            {
                failedEntry.LastUpdated = DateTime.Now;
                failedEntry.Status = (int)updatedStatus;

                if (updatedStatus == MessageInboxStatus.Failed)
                {
                    failedEntry.RetryCount++;
                }
                else
                {
                    db.All<MsmqCompletedMessages>().Add(failedEntry.ToCompletedMessage());
                    db.All<MsmqFailedMessages>().Remove(failedEntry);
                }

                db.Flush();
               return true;
            }

            return false;
        }


        /// <summary>
        /// Update the status for a Message. 
        /// Contains logic for Retry, insert and delete.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedStatus"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private static bool UpdateInboxMessage(long id, MessageInboxStatus updatedStatus, string errorMessage)
        {
            var db = SessionManager.Open(Logger);
            var entry = db.All<MsmqMessageInbox>().SingleOrDefault(m => m.Id == id);
            var keepInInbox = false;

            if (entry != null)
            {
                entry.LastUpdated = DateTime.Now;
                entry.Status = (int)updatedStatus;
                entry.ErrorMessage = errorMessage;

                if (updatedStatus == MessageInboxStatus.Failed)
                {
                    entry.RetryCount++;
                    if (entry.RetryCount >= 5)
                    {
                        db.All<MsmqFailedMessages>().Add(entry.ToFailedMessage());
                    }
                    else
                    {
                        entry.Status = (int)MessageInboxStatus.Received;
                        keepInInbox = true;
                    }
                }
                else if (updatedStatus == MessageInboxStatus.Ignored)
                {
                    db.All<MsmqFailedMessages>().Add(entry.ToFailedMessage());
                }
                else
                {
                    db.All<MsmqCompletedMessages>().Add(entry.ToCompletedMessage());
                }

                if(!keepInInbox) db.All<MsmqMessageInbox>().Remove(entry);

                db.Flush();

                return true;
            }

            return false;
        }
    }
}
