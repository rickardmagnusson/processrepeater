﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    /// <summary>
    /// Needs to check if these are to be filled with values or not. If not remove this class
    /// </summary>
    public class InboxMessages
    {
        public List<MsmqMessageInbox> ToProcess { get; set; }
        public List<MsmqMessageInbox> Duplicates { get; set; }
    }
}
