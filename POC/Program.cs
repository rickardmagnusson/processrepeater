﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using NHibernate.Linq;
using System.Text;

//using System.Threading.Tasks;
//using Crm.Services.Extensions;
//using Microsoft.Crm.Sdk.Messages;
//using Microsoft.Xrm.Sdk;
//using Microsoft.Xrm.Sdk.Query;
//using Microsoft.Xrm.Sdk.Client;
//using Azure.Queues.Process.Models;
//using NHibernate;
using Azure.Queues.Process.Data;
using System.Collections;
using System.Linq;
using NHibernate;
using FluentNHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Automapping;
using NHibernate.Dialect;
using FluentNHibernate.Cfg.Db;
using NHibernate.Driver;
using Common.Entities;
using Common;
using Common.Models;
using System.Linq.Expressions;
//using System.Linq;
//using Azure.Queues.Process;

namespace POC
{

    public class Manager
    {
        public Manager() {}

        public static ISession Open()
        {
            var conn = ConfigurationManager.AppSettings["Connectionstring"];

            return Fluently
                       .Configure()
                       .Database(MsSqlConfiguration.MsSql2008
                       .ConnectionString(conn))
                       .ExposeConfiguration(cfg => new SchemaExport(cfg).Execute(true, false, false))
                       .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                       .Mappings(MapAssemblies)
                       .BuildSessionFactory()
                       .OpenSession();
        }

        public static bool IsEntity(Type t)
        {
            return typeof(Common.Entities.IEntity).IsAssignableFrom(t);
        }


        internal static void MapAssemblies(MappingConfiguration fmc)
        {
            //Not sure this will work in Azure...
            (from a in AppDomain.CurrentDomain.GetAssemblies()
                select a
                   into assemblies
                      select assemblies)
                        .ToList()
                            .ForEach(a => {
                                fmc.AutoMappings.Add(AutoMap.Assembly(a)                     
                                    .Where(IsEntity));
                            });
        }

    }


    class Program
    {
        

        static void Main(string[] args)
        {
            Console.WriteLine("Running tests with defined settings");

            Console.WriteLine("Try get configuration from category....");
            ConfigurationExtensions.GetConfigurationFor("Bank");
            Console.WriteLine("");
            ConfigurationExtensions.GetConfigurationFor("CRM");

            #region Tests

            //StringBuilder log = new StringBuilder();

            //Console.WriteLine("Trying to connect to database (SessionManager)...");
            //var db = Manager.Open();
            //if (db == null)
            //    throw new Exception("Session is null");
            //Console.WriteLine("Connected with ISession");
            //Console.WriteLine("");

            //Console.WriteLine("Creating tables...");
            ////var conf = db.All<Configurations>();
            //Console.WriteLine("Success");
            //Console.WriteLine("");



            //if (!conf.Any())
            //{
            //    Console.WriteLine("Attemting to write to db...");
            //    //var env = Configurations.CurrentEnvironment();

            //    //var c = new Configurations { Env = env, Key = "Name 1", Value = "Value 1" };

            //    db.WithTransaction(s => { s.Save(c); });

            //    Console.WriteLine("Succsessfully inserted a row");
            //    Console.WriteLine("");
            //}
            //else
            //{
            //    //Console.WriteLine("Attemting to read from db...");
            //foreach(var k in conf) {
            //    Console.WriteLine($"{k.Key} : {k.Value}");
            //}
            //Console.WriteLine("Succsessfully read rows from db.");
            //Console.WriteLine("");

            //var environment = conf.First();
            //Console.WriteLine($"Environment = {environment.Env}");
            //}

            //Console.WriteLine("Trying to connect to CRM...");
            //var crm = OrganizationServiceHelper.GetIOrganizationService();

            //QueryExpression systemUsers = new QueryExpression("systemuser");
            //systemUsers.ColumnSet = new ColumnSet(true);
            //EntityCollection userCollection = crm.RetrieveMultiple(systemUsers); // this call executed based on your connection string credential.


            //// for each User we launch the query to retrieve the saved views
            //foreach (Entity systemUser in userCollection.Entities)
            //{
            //    QueryExpression query = new QueryExpression("account");
            //    query.ColumnSet = new ColumnSet(true);

            //    // we set the CallerId property to impersonate the current iteration user
            //    //serviceProxy.CallerId = systemUser.Id;
            //    EntityCollection accountCollection = crm.RetrieveMultiple(query);
            //    Console.WriteLine(accountCollection.TotalRecordCount);
            //}


            //Console.WriteLine($"Connected to CRM at {crmUrl}");
            #endregion

            Console.ReadLine();
        }
    }


    public class ConfigurationExtensions
    {
        private static ConfigurationExtensions _instance;
        private static Dictionary<string, Dictionary<string, string>> configuration;
        private ConfigurationExtensions() {}


        private static ConfigurationExtensions GetInstance()
        {
            if (_instance == null) //Only touch database once.
            {
                _instance = new ConfigurationExtensions();
                _instance.LoadConfiguration();
            }
            return _instance;
        }


        /// <summary>
        /// Get a dictionary of configuration values.
        /// </summary>
        /// <param name="category">The category to get the configuration from.</param>
        /// <returns>Return a Dictionary from the current category selected.</returns>
        public static Dictionary<string, string> GetConfigurationFor(string category)
        {
            GetInstance();
            var conf = new Dictionary<string, string>();
            configuration.TryGetValue(category, out conf);
            PrintForCategory(category, conf);
            return conf;
        }


        /// <summary>
        /// Read all configuration one time only.
        /// </summary>
        private void LoadConfiguration()
        {
            configuration = Manager.Open().All<ETL_Config>()
                .Where(c => c.IsActive == true)
                .ToList()
                .OfType<ETL_Config>()
                .GroupBy(r => r.Category)
                .ToDictionary(group => group.Key,
                    group => group.ToDictionary(d => d.Property,
                                                d => d.Value));
            //PrintDict(configuration);
        }


        private static void PrintForCategory(string category, Dictionary<string, string> val)
        {
            Console.WriteLine($"Category ---> {category} <---");
            foreach (var k in val) {
                Console.WriteLine($"{k.Key} : {k.Value.ToString()}");
            }
        }


        private void PrintDict(Dictionary<string, Dictionary<string, string>> val)
        {
            Console.WriteLine($"Found: {val.Count()}");

            foreach (var item in val){

                Console.WriteLine($"------> {item.Key} ----------------------");

                foreach (var k in item.Value) {
                    Console.WriteLine($"{k.Key} : {k.Value.ToString()}");
                }

                Console.WriteLine($"----------------------------------------");
            }
        }
    }






   
}
