﻿using Azure.Queues.Process.Data;
using Common.Entities;
using Common.Enums;
using FluentNHibernate.Mapping;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.Type;
using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Xml.Serialization;

namespace Common.Models
{
    public class MsmqMessageInboxMap : ClassMap<MsmqMessageInbox>
    {
        public MsmqMessageInboxMap()
        {
            Map(x => x).Column("MessageContent").CustomType(typeof(XmlType<SakHendelse>));
        }
    }


    /// <summary>
    /// Entity
    /// </summary>
    public class MsmqMessageInbox : ITable, IEntity
    {

        public MsmqMessageInbox() { }

        public MsmqMessageInbox(string bankNumber, string customerNumber, string messageXml, EventMessageType type, int subType, string sourceId, string source)
        {
            Received = DateTime.Now;
            LastUpdated = DateTime.Now;
            ActionType = subType;
            Status = (int)MessageInboxStatus.Received;
            BankId = bankNumber;
            RetryCount = 0;
            CustomerNumber = customerNumber;
            MessageContent = messageXml;
            EventType = (int)type;
            SourceId = sourceId;
            Source = source;
        }


        public virtual long Id { get; set; }
        public virtual System.DateTime Received { get; set; }
        public virtual string BankId { get; set; }

        [StringValidator(MinLength = 1, MaxLength = Int32.MaxValue)]
        public virtual string MessageContent { get; set; }
        public virtual int Status { get; set; }
        public virtual int RetryCount { get; set; }
        public virtual Nullable<System.DateTime> LastUpdated { get; set; }
        public virtual int EventType { get; set; }
        public virtual string CustomerNumber { get; set; }
        public virtual string ErrorMessage { get; set; }
        public virtual int ActionType { get; set; }
        public virtual string SourceId { get; set; }
        public virtual string Source { get; set; }
    }


    /// <summary>
    /// Entity
    /// </summary>
    public class MsmqCompletedMessages : ITable, IEntity
    {
        public virtual long Id { get; set; }
        public virtual System.DateTime Received { get; set; }
        public virtual string BankId { get; set; }
        public virtual string MessageContent { get; set; }
        public virtual int Status { get; set; }
        public virtual int RetryCount { get; set; }
        public virtual Nullable<System.DateTime> LastUpdated { get; set; }
        public virtual int EventType { get; set; }
        public virtual string CustomerNumber { get; set; }
        public virtual string ErrorMessage { get; set; }
        public virtual int ActionType { get; set; }
        public virtual string SourceId { get; set; }
        public virtual string Source { get; set; }
    }


    /// <summary>
    /// Entity
    /// </summary>
    public class MsmqFailedMessages : ITable, IEntity
    {
        public virtual long Id { get; set; }
        public virtual System.DateTime Received { get; set; }
        public virtual string BankId { get; set; }
        public virtual string MessageContent { get; set; }
        public virtual int Status { get; set; }
        public virtual int RetryCount { get; set; }
        public virtual Nullable<System.DateTime> LastUpdated { get; set; }
        public virtual int EventType { get; set; }
        public virtual string CustomerNumber { get; set; }
        public virtual string ErrorMessage { get; set; }
        public virtual int ActionType { get; set; }
        public virtual string SourceId { get; set; }
        public virtual string Source { get; set; }
    }

}
