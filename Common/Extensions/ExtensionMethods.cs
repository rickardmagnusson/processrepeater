﻿using System;
using Microsoft.Xrm.Sdk;

namespace Common
{
    public static class ExtensionMethods
    {
        public static void SetValueIfPresent(this Entity e, string indexer, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                e[indexer] = value;
            }
        }
        public static void SetValueIfGuidHasValue(this Entity e, string indexer, Guid value)
        {
            if (value != Guid.Empty)
            {
                e[indexer] = value;
            }
        }

        public static void SetValueIfNotNull(this Entity e, string indexer, string value)
        {
            if (value != null)
            {
                e[indexer] = value;
            }
        }
        public static void SetValueReflect(this Entity e, string indexer, string value)
        {
            if (value != null)
            {
                e[indexer] = value;
            }
            else
            {
                e[indexer] = string.Empty;
            }
        }
    }
}
