﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;
using Common.Enums;
using Microsoft.Xrm.Sdk;

namespace Common.RequestBuilders
{
    public partial class RequestBuilder
    {


        public static Entity BuildCreateIncident(KundeHendelse evt, CustomerExtra cust)
        {
            var casetypecode = evt.Fase.ToLower().Equals("avsluttet")
                ? new OptionSetValue((int)CaseTypeCode.Avslutning)
                : new OptionSetValue((int)CaseTypeCode.EndringOgAdministrasjon);

            var ent = new Entity(EntityName.incident.ToString())
            {
                ["customerid"] = new EntityReference(cust.EntityName.ToString(), cust.CustomerId),
                ["title"] = Helper.GetTitleForKundehendelse(evt.Handling, evt.TransaksjonsType, cust.Product.Name, evt.FagSystem),
                ["description"] = Helper.GetDescription(evt),
                ["subjectid"] = new EntityReference("subject", cust.SubjectId),

                ["casetypecode"] = casetypecode,
                ["cap_channel"] = Helper.GetChannel(evt.Channel),
                ["cap_source"] = Helper.GetTerritoryCode(evt.FagSystem),
                ["cap_app"] = evt.Applikasjon,
                ["ownerid"] = Helper.SetOwner(cust),
                ["cap_colloborateflag"] = true,
                ["cap_eventdate"] = evt.Tidspunkt
            };

            if (cust.Product.Id != Guid.Empty)
            {
                ent["productid"] = new EntityReference("product", cust.Product.Id);
            }

            return ent;
        }
        public static void SetIncidentOriginToWeb(Entity entity)
        {
            entity["caseorigincode"] = new OptionSetValue(3);
        }

    }
}
