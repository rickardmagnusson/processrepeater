﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;
using Microsoft.Xrm.Sdk;

namespace Common.RequestBuilders
{
    public partial class RequestBuilder
    {

        public static Entity CreateDepartment(string departmentName, string departmentNumber, Guid defaultTeamId)
        {
            return new Entity(EntityName.cap_department.ToString())
            {
                ["cap_name"] = departmentName,
                ["cap_departmentnumber"] = departmentNumber,
                ["ownerid"] = new EntityReference("team", defaultTeamId)
            };
        }
        public static Entity UpdateDepartment(string departmentName)
        {
            return new Entity(EntityName.cap_department.ToString())
            {
                ["cap_name"] = departmentName,
                ["statecode"] = new OptionSetValue(0),
                ["statuscode"] = new OptionSetValue(1)
            };
        }

    }
}
