﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Constants;
using Common.Enums;
using Microsoft.Xrm.Sdk;

namespace Common.RequestBuilders
{
    public partial class RequestBuilder
    {
        public static Entity BuildCreateConnectionEntity(BankConnection conn)
        {
            return new Entity(EntityName.connection.ToString())
            {
                ["record1id"] = new EntityReference(CrmConstants.BusinessUnitEntityName, conn.BankConnectionId),
                ["record1roleid"] = new EntityReference(CrmConstants.ConnectionRoleEntityName, conn.HasCustomerRoleId),
                ["record2id"] = new EntityReference(conn.EntityName.ToString(), conn.CustomerId),
                ["record2roleid"] = new EntityReference(CrmConstants.ConnectionRoleEntityName, conn.VendorRoleId),
                ["ownerid"] = new EntityReference(CrmConstants.TeamEntityName, conn.OwnerId)
            };
        }
    }
}
