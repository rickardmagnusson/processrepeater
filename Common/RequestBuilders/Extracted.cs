﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.RequestBuilders
{
    public class Extracted
    {
        //public Entity BuildCreateIncident(KundeHendelse evt, CustomerExtra cust) { }
        //public void SetIncidentOriginToWeb(Entity entity) { }
        //public Entity BuildCreateConnectionEntity(BankConnection conn) { }
        //public Entity CreateDepartment(String departmentName, String departmentNumber, Guid defaultTeamId) { }
        //public Entity UpdateDepartment(String departmentName) { }
        //public Entity BuildUndefinedInfoActivity(SakHendelse evt, CustomerExtra cust) { }
        //public Entity BuildInfoActivitySummaryOnSalesOpportunity(SakHendelse evt, CustomerExtra cust, Guid opportunityId, Boolean create) { }
        //public Entity BuildUndefinedInfoActivity(KundeHendelse evt, CustomerExtra cust) { }
        //public Entity BuildInfoActivitySummaryOnSalesOpportunity(KundeHendelse evt, Guid opportunityId, Guid defaultTeamId, Boolean create) { }
        //public Entity BuildCreateOpportunity(KundeHendelse evt, CustomerExtra cust, Int64 estimatedValue, OptionSetValue statuscode) { }
        //public Entity BuildCreateOpportunityRadgiverPluss(KundeHendelse evt, CustomerExtra cust, Int64 estimatedValue, OptionSetValue statuscode) { }
        //public Entity BuildCreateOpportunityKreditt(SakHendelse evt, CustomerExtra cust, Boolean customer) { }
        //public Entity BuildCreateOpportunityClose(Guid opportunityId, DateTime timestamp, Decimal value, String description) { }
        //public Entity BuildCreateOpportunityRadgiverPluss(SakHendelse evt, CustomerExtra cust, OptionSetValue statuscode) { }
        //public Entity BuildUpdateOpportunityRadgiverPluss(SalesOpportunity so, SakHendelse evt, CustomerExtra cust, OptionSetValue statuscode) { }
        //public Entity BuildUpdateOpportunityForClose(SalesOpportunity so, SakHendelse evt, Int64 estimatedValue) { }
        //public Entity BuildUpdateOpportunity(SalesOpportunity so, KundeHendelse evt, CustomerExtra cust, Int64 estimatedValue, Guid productId, OptionSetValue statuscode) { }
        //public Entity BuildUpdateOpportunityForClose(SalesOpportunity so, KundeHendelse evt, Int64 estimatedValue, Guid productId) { }
        //public Entity BuildUpdateOpportunityRadgiverPluss(SalesOpportunity so, KundeHendelse evt, CustomerExtra cust, Int64 estimatedValue, Guid productId, OptionSetValue statuscode) { }
        //public void UpdateOpportunityName(Entity ent, KundeHendelse evt, CustomerExtra cust) { }
        //public void UpdateOpportunityDescription(Entity ent, KundeHendelse evt) { }
        //public void UpdateOpportunityStatus(Entity ent, OptionSetValue status) { }
        //public void UpdateOpportunityForNySakOpprettet(SakHendelse evt, Entity opportunity, CustomerExtra cust) { }
        //public void UpdateOpportunityTitleFormal(SakHendelse evt, Entity opportunity) { }
        //public void UpdateOpportunityEventDate(SakHendelse evt, Entity opp) { }
        //public void UpdateOpportunityParent(Entity ent, Guid parentId) { }
        //public void UpdateOpportunityCustomer(CustomerExtra cust, Entity opportunity) { }
        //public Entity CreateOpportunityProductsEntity(Guid opportunityId, ProductInfo product, Decimal priceperunit, Decimal quantity) { }
        //public Entity UpdateOpportunityProductsEntity(Guid opportunityProductId, Decimal priceperunit, Decimal quantity) { }
        //public Entity BuildCreateProduct(KundeHendelse evt, CustomerExtra cust, Guid parentId, Guid unitGroupId, Guid primaryUnitId) { }
        //public Entity BuildCreateProductPriceLevel(KundeHendelse evt, CustomerExtra cust, Guid productId, Guid unitGroupId, Guid primaryUnitId) { }
        //public Entity BuildUpdateProduct(Guid productId, String name) { }
        //public void UpdateCustomerEntity(CustomerEvent request, CustomerExtra cust, Entity ent, MsmqLogger Log) { }
        //public void UpdateCustomerAddresses(CustomerEvent request, Entity ent) { }
        //public void UpdateCustomerEmails(CustomerEvent request, Entity ent) { }
        //public void UpdateCustomerPhones(CustomerEvent request, Entity ent, Boolean isContact) { }
        //public void UpdateCustomerNumber(String customerNumber, Entity ent, Boolean isContact) { }
        //public void UpdateCustomerName(CustomerEvent request, Entity ent, Boolean isContact) { }
        //public void UpdateCustomerSourceId(CustomerEvent request, Entity ent) { }
        //public void UpdateCustomerOwner(CustomerExtra cust, Entity ent) { }
        //public void UpdateCustomerDepartment(CustomerExtra cust, Entity ent) { }
        //public void UpdateTerritoryCode(CustomerEvent request, Entity ent) { }
        //public void UpdateTypeToCustomer(Entity ent) { }
        //public void UpdateCustomerEntity(KundeRegisterEvent request, CustomerExtra cust, Entity ent, MsmqLogger Log) { }
        //public void UpdateCustomerAddresses(KundeRegisterEvent request, Entity ent) { }
        //public void UpdateCustomerEmails(KundeRegisterEvent request, Entity ent) { }
        //public void UpdateCustomerPhones(KundeRegisterEvent request, Entity ent, Boolean isContact) { }
        //public void UpdateCustomerNumber(KundeRegisterEvent request, Entity ent, Boolean isContact) { }
        //public void UpdateCustomerName(KundeRegisterEvent request, Entity ent, Boolean isContact) { }
        //public void UpdateCustomerSourceId(KundeRegisterEvent request, Entity ent) { }
        //public void RemoveCustomerSourceId(KundeRegisterEvent request, Entity ent) { }
        //public void RemoveCustomerSourceId(Source source, Entity ent) { }
        //public void UpdateTerritoryCode(KundeRegisterEvent request, Entity ent) { }
        //public Entity BuildUpdateCustomerSourceId(EntityName entityName, Guid id, Boolean isSkadeInsurance, String sourceId) { }
        //public Entity BuildRemoveCustomerSourceId(EntityName entityName, Guid id, Boolean isSkadeInsurance) { }
        //public Entity BuildDeactivateEntity(EntityName entityName, Guid id) { }
        //public Entity BuildActivateEntity(String entityName, Guid id) { }
        //public void UpdateCustomerOwnerWithLoggedIn(CustomerExtra cust, Entity ent) { }
        //public void UpdateTerritoryCode(String source, Entity ent) { }
        //public void SetCustomerAsPotentialCustomer(Entity ent) { }
        //public void UpdateCustomerNameForPotentialKundeHendelse(ProcessArea processArea, Entity ent, Boolean isContact) { }
        //public void UpdateCustomerSourceId(String source, String sourceId, Entity ent) { }
        //public void UpdateCustomerSourceId(Source source, String sourceId, Entity ent) { }
        //public void ActivateEntity(Entity ent) { }
        //public void DeactivateEntity(Entity ent) { throw new NotImplementedException();  }
        //public Entity BuildPotentialCustomerForKundeHendelse(KundeHendelse evt, CustomerExtra cust) { }
        //public Entity BuildPotentialCustomerForKundeHendelse(SakHendelse evt, CustomerExtra cust) { }
    }
}
