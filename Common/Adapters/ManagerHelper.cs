﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;

using Common.Logging;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Tooling.Connector;

namespace Common.Adapters
{


    /// <summary>
    /// This is needed to be rewritten totally(Medium effort)
    /// </summary>

    public class ManagerHelper
    {
        public static string GetExceptionMessage(Exception ex)
        {
            var strBuilder = new StringBuilder(ex.Message);

            if (ex.InnerException != null)
            {
                strBuilder.Append("Inner exception: ");
                strBuilder.Append(ex.InnerException.Message);
            }

            return strBuilder.ToString();
        }

        public static string GetExceptionMessage(FaultException<OrganizationServiceFault> fex)
        {
            var strBuilder = new StringBuilder(fex.Message);

            if (fex.InnerException != null)
            {
                strBuilder.Append("Inner exception: ");
                strBuilder.Append(fex.InnerException.Message);
            }

            if (fex.Detail != null)
            {
                strBuilder.Append("FaultExceptionDetails: ");
                strBuilder.Append(string.Format(CultureInfo.InvariantCulture, "Timestamp: {0}", fex.Detail.Timestamp));
                strBuilder.Append(string.Format(CultureInfo.InvariantCulture, "Code: {0}", fex.Detail.ErrorCode));
                strBuilder.Append(string.Format(CultureInfo.InvariantCulture, "Message: {0}", fex.Detail.Message));

                if (fex.Detail.InnerFault != null)
                {
                    strBuilder.Append("Inner fault: ");
                    strBuilder.Append(fex.Detail.InnerFault.Message);
                }
            }

            return strBuilder.ToString();
        }

        public static OrganizationServiceProxy GetCrmOrganizationServiceProxy(Uri crmOrganizationUri)
        {
            Uri homeRealmUri = null;
            var credentials = new ClientCredentials();

            credentials.Windows.ClientCredential = System.Net.CredentialCache.DefaultNetworkCredentials;

            var orgServiceProxy = new OrganizationServiceProxy(crmOrganizationUri, homeRealmUri, null, null);

            return orgServiceProxy;
        }


        public static OrganizationServiceProxy GetCrmOrganizationServiceByXrmTooling(Dictionary<string, string> config, MsmqLogger Log)
        {
            try
            {
                var connectionString = config["CrmConnectionString"];
                Log.Debug($"Connecting with connectionString: '{connectionString}'");

                var conn = new CrmServiceClient(connectionString);
                return conn.OrganizationServiceProxy;

            }
            catch (Exception ex)
            {
                var msg = $"GetCrmOrganizationServiceProxyCloudByConnStr failed with error: {ex.Message}";
                Log.Fatal(msg);
            }
            return null;
        }
    }
}
