﻿using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.ServiceModel.Description;
using Common.Constants;
using Common.Entities;
using Common.Logging;
using Crm.Services.Extensions;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;

namespace Common.Adapters
{
    public class CrmWsHelper
    {
        public static ClientCredentials clientCredentials = null;
        public static ConcurrentQueue<string> CrmServers = new ConcurrentQueue<string>();
        public static string CrmDbConnString = string.Empty;
        public static int _processedRows = 0;
        public static bool useThreading = false;
        public static IOrganizationService _service = null;
        static ConcurrentDictionary<int, IOrganizationService> _services = new ConcurrentDictionary<int, IOrganizationService>();
        private static string logPrefix = "Eika::CRM::DataLoad::CrmWSHelper::";
        public static string CrmEditRecordUrl = string.Empty;
        //private static readonly ILog Log = LogManager.GetLogger("msmq-receive-logger");
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("msmq-receive-logger");

        public static IOrganizationService GetCRMService(string _crmServiceUrl)
        {
            return OrganizationServiceHelper.GetIOrganizationService();
        }

        public static IOrganizationService GetCRMThreadService(int id)
        {
            string currCrmUrl = string.Empty;
            CrmServers.TryDequeue(out currCrmUrl);

            if (string.IsNullOrEmpty(currCrmUrl))
                return _service;

            CrmServers.Enqueue(currCrmUrl);

            return GetCRMThreadService(id, currCrmUrl);
        }

        public static IOrganizationService GetCRMThreadService(int id, string _crmServiceUrl)
        {
            IOrganizationService currentService;

            // Was a service already created for this thread ID? 
            if (_services.TryGetValue(id, out currentService))
            {
                return currentService;
            }
            else
            {
                currentService = OrganizationServiceHelper.GetIOrganizationService();
                _services.GetOrAdd(id, currentService);
                return currentService;
            }
        }


        #region Helpers
        public static Guid GetTeamId(string teamName, IOrganizationService _service)
        {
            Guid _teamId = Guid.Empty;

            ConditionExpression ce1 = new ConditionExpression("name", ConditionOperator.Equal, teamName);
            FilterExpression fe1 = new FilterExpression();
            fe1.Conditions.Add(ce1);

            QueryExpression qe1 = new QueryExpression(CrmConstants.TeamEntityName);
            qe1.Criteria = fe1;
            qe1.ColumnSet = new ColumnSet("name");
            qe1.PageInfo.ReturnTotalRecordCount = true;
            EntityCollection _recColl = _service.RetrieveMultiple(qe1);

            if (_recColl.Entities.Count > 0)
            {
                _teamId = _recColl.Entities[0].Id;
                Log.Info(string.Format("{0}GetTeamId Succeeded for Team {1} with TeamId {2} ", logPrefix, teamName, _teamId));
            }
            else
            {
                Log.Info(string.Format("{0}GetTeamId failed for Team {1}", logPrefix, teamName));
            }
            return _teamId;
        }

        public static Guid GetUserId(string login, IOrganizationService _service)
        {
            Guid _systemuserId = Guid.Empty;

            ConditionExpression ce1 = new ConditionExpression("domainname", ConditionOperator.EndsWith, login);
            FilterExpression fe1 = new FilterExpression();
            fe1.Conditions.Add(ce1);

            QueryExpression qe1 = new QueryExpression("systemuser");
            qe1.Criteria = fe1;
            qe1.ColumnSet = new ColumnSet("domainname");
            qe1.PageInfo.ReturnTotalRecordCount = true;
            EntityCollection _recColl = _service.RetrieveMultiple(qe1);

            if (_recColl.Entities.Count > 0)
            {
                _systemuserId = _recColl.Entities[0].Id;
                Log.Info(string.Format("{0}GetUserId Succeeded for Login {1} with systemuserId {2} ", logPrefix, login, _systemuserId.ToString()));
            }
            else
            {
                Log.Info(string.Format("{0}GetUserId failed for Login {1}", logPrefix, login));
            }
            return _systemuserId;
        }

        public static bool AssignRecord(EntityReference record, EntityReference owner, IOrganizationService service)
        {
            AssignRequest assign = new AssignRequest
            {
                Assignee = owner,
                Target = record
            };

            try
            {
                service.Execute(assign);
                Log.Info(string.Format("Assigned successfully {0} with Id {1} to {2} with Id {3}", record.LogicalName, record.Id.ToString(), owner.LogicalName, owner.Id.ToString()));
                return true;
            }
            catch
            {
                Log.Info(string.Format("Unable to assign {0} with Id {1} to {2} with Id {3}", record.LogicalName, record.Id.ToString(), owner.LogicalName, owner.Id.ToString()));
                return false;
            }
        }

        public static bool AssignETLRecord(Guid etlMasterId, Guid teamId, IOrganizationService service)
        {
            return AssignRecord(new EntityReference(cap_etlmaster.EntityLogicalName, etlMasterId), new EntityReference(CrmConstants.TeamEntityName, teamId), service);
        }

        public static bool SetEntityStatus(Guid recordGUID, string entityName, int stateCode, int statusCode, IOrganizationService orgServiceProxy)
        {
            bool success = true;
            try
            {
                SetStateRequest setState = new SetStateRequest();
                setState.EntityMoniker = new EntityReference(entityName, recordGUID);
                setState.State = new OptionSetValue(stateCode);
                setState.Status = new OptionSetValue(statusCode);
                orgServiceProxy.Execute(setState);
                Log.Info(string.Format(CultureInfo.InvariantCulture, "{0}Setstate succeeded for {1} with Guid {2} to state {3} and status {4}", logPrefix, entityName, recordGUID, stateCode, statusCode));
            }
            catch (InvalidOperationException e)
            {
                //do nothing
                success = false;
                Log.Warn(e);
            }
            return success;
        }

        public static Guid GetBankId(string bankNo, IOrganizationService service)
        {
            Guid bankId = Guid.Empty;

            ConditionExpression ce1 = new ConditionExpression("ava_organizationnum", ConditionOperator.Equal, bankNo);
            FilterExpression fe1 = new FilterExpression(LogicalOperator.And);
            fe1.AddCondition(ce1);
            QueryExpression qe1 = new QueryExpression { EntityName = "businessunit" };
            qe1.ColumnSet.AddColumns("name");
            qe1.Criteria.AddFilter(fe1);
            EntityCollection bankColl = service.RetrieveMultiple(qe1);

            if (bankColl.Entities.Count > 0)
            {
                bankId = bankColl.Entities[0].Id;
                Log.Info(string.Format("{0}GetBankId Succeeded for Bank {1} with bankId ", logPrefix, bankNo, bankId.ToString()));
            }
            return bankId;
        }

        public static Guid GetDefaultTeamId(string bankNo, IOrganizationService service)
        {
            Guid bankId = GetBankId(bankNo, service);
            return GetDefaultTeamId(bankId, service);
        }

        public static Guid GetDefaultTeamId(Guid bankId, IOrganizationService service)
        {
            Guid teamId = Guid.Empty;
            if (bankId == Guid.Empty)
                return teamId;
            ConditionExpression ce1 = new ConditionExpression("businessunitid", ConditionOperator.Equal, bankId);
            ConditionExpression ce2 = new ConditionExpression("isdefault", ConditionOperator.Equal, true);
            FilterExpression fe1 = new FilterExpression(LogicalOperator.And);
            fe1.AddCondition(ce1);
            fe1.AddCondition(ce2);
            QueryExpression qe1 = new QueryExpression { EntityName = CrmConstants.TeamEntityName };
            qe1.ColumnSet.AddColumns("name");
            qe1.Criteria.AddFilter(fe1);
            EntityCollection teamColl = service.RetrieveMultiple(qe1);

            if (teamColl.Entities.Count > 0)
            {
                teamId = teamColl.Entities[0].Id;
                Log.Info(message: string.Format("{0}GetDefaultTeamId Succeeded for Bank {1} with default teamId ", logPrefix, bankId.ToString(), teamId.ToString()));
            }
            else
            {
                Log.Warn($"{logPrefix}GetDefaultTeamId failed to fetch TeamId for Bank {bankId.ToString()}");
            }
            return teamId;
        }

        public static EtlMaster CreateEtlMaster(EtlMaster crmEtl, IOrganizationService service)
        {
            cap_etlmaster crmGuiMaster = new cap_etlmaster();
            crmGuiMaster.cap_bankno = crmEtl.BankNo;
            crmGuiMaster.cap_entity = crmEtl.Entity;
            crmGuiMaster.cap_fromeventid = crmEtl.FromEventId;
            crmGuiMaster.cap_etl_start = crmEtl.StartTime;
            if (string.IsNullOrEmpty(crmEtl.LoadId))
                crmGuiMaster.cap_loadid = "-1";
            else
                crmGuiMaster.cap_loadid = crmEtl.LoadId;
            crmGuiMaster.cap_retrycount = 0;
            crmGuiMaster.cap_ondemand_flag = crmEtl.RunOnDemand;
            crmEtl.CRMGuid = service.Create(crmGuiMaster);
            Log.Info($"{logPrefix} ETL master record with Id {crmEtl.CRMGuid} for bank {crmEtl.BankNo} is created successfully");
            return crmEtl;
        }

        public static void UpdateEtlMaster(EtlMaster crmEtl, IOrganizationService service)
        {
            cap_etlmaster crmGuiMaster = new cap_etlmaster();
            crmGuiMaster.cap_bankno = crmEtl.BankNo;
            crmGuiMaster.cap_entity = crmEtl.Entity;
            crmGuiMaster.cap_etlmasterId = crmEtl.CRMGuid;
            if (crmEtl.EndTime.HasValue && crmEtl.EndTime.Value > DateTime.MinValue)
                crmGuiMaster.cap_etl_end = crmEtl.EndTime.Value;
            else
                crmGuiMaster.cap_etl_end = null;
            crmGuiMaster.cap_etl_start = crmEtl.StartTime;
            crmGuiMaster.cap_fromeventid = crmEtl.FromEventId;
            crmGuiMaster.cap_loadid = crmEtl.LoadId;
            crmGuiMaster.cap_numfetched = crmEtl.NumFetched;
            crmGuiMaster.cap_numcreated = crmEtl.NumCreated;
            crmGuiMaster.cap_numdeleted = crmEtl.NumDeleted;
            crmGuiMaster.cap_numerrors = crmEtl.NumError;
            crmGuiMaster.cap_numignored = crmEtl.NumIgnored;
            crmGuiMaster.cap_numupdated = crmEtl.NumUpdated;
            crmGuiMaster.cap_retrycount = crmEtl.RetryCount;
            crmGuiMaster.cap_toeventid = crmEtl.ToEventId;
            crmGuiMaster.cap_error_message = crmEtl.ErrorMessage;
            crmGuiMaster.statuscode = new OptionSetValue(crmEtl.StatusCode);

            service.Update(crmGuiMaster);
            Log.Info($"{logPrefix} ETL master record with Id {crmEtl.CRMGuid} and LoadId {crmEtl.LoadId} is updated successfully");
        }

        #endregion

    }

}
