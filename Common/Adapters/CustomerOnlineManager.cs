﻿using System;
using System.Globalization;
using System.Threading;
using Common.Entities;
using Common.Helpers;
using Common.Logging;
using log4net;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace Common.Adapters
{
    public class CustomerOnlineManager : IDisposable
    {
        #region Private variables

        private OrganizationServiceProxy _service;
        private const string logPrefix = "::CustomerOnlineManager::";
        private readonly MsmqLogger Log = MsmqLogger.CreateLogger("msmq-receive-logger");

        #endregion

        #region Constructor

        public CustomerOnlineManager(string crmOrganization)
        {
            var crmOrganizationUri = new Uri(crmOrganization);

            if (crmOrganizationUri == null)
            {
                throw new ArgumentNullException(string.Format(CultureInfo.InvariantCulture, "{0}::CustomerManager. Argument crmOrganizationUri can not be null or empty.", logPrefix));
            }

            _service = ManagerHelper.GetCrmOrganizationServiceProxy(crmOrganizationUri);
        }

        public CustomerOnlineManager(OrganizationServiceProxy organizationService)
        {
            if (organizationService == null)
            {
                throw new ArgumentNullException(string.Format(CultureInfo.InvariantCulture, "{0}::CustomerManager. Argument organizationService can not be null.", logPrefix));
            }

            // Get org service proxy 
            _service = organizationService;

            // Log
            Log.Info(string.Format(CultureInfo.InvariantCulture, "{0}CustomerManager created for ManagedThreadId '{1}'.", logPrefix, Thread.CurrentThread.ManagedThreadId));
        }

        #endregion

        #region Public methods

        public bool UpdateEntity(Entity ent)
        {
            try
            {
                _service.Update(ent);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error($"Exception while updating entity in CRM: {ManagerHelper.GetExceptionMessage(ex)}");
                return false;
            }
        }
        public bool UpdateEntity(Entity ent, ProcessedCustomerResponse response)
        {
            try
            {
                _service.Update(ent);
                return true;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Exception while updating entity in CRM: {ManagerHelper.GetExceptionMessage(ex)}";
                response.ErrorMessage = errorMsg;
                Log.Error(errorMsg);
                return false;
            }
        }

        public Guid CreateEntity(Entity ent)
        {
            try
            {
                return _service.Create(ent);
            }
            catch (Exception ex)
            {
                Log.Error($"Exception while creating entity '{ent.LogicalName}' in CRM : {ManagerHelper.GetExceptionMessage(ex)}");
                return Guid.Empty;
            }
        }
        public Guid CreateEntity(Entity ent, ProcessedCustomerResponse response)
        {
            try
            {
                return _service.Create(ent);
            }
            catch (Exception ex)
            {
                var errorMsg = $"Exception while creating entity in CRM: {ManagerHelper.GetExceptionMessage(ex)}";
                response.ErrorMessage = errorMsg;
                Log.Error(errorMsg);
                return Guid.Empty;
            }
        }


        public bool DeleteEntity(string entityName, Guid id)
        {
            try
            {
                _service.Delete(entityName, id);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error($"Exception while deleting entity in CRM with id: {id}: {ManagerHelper.GetExceptionMessage(ex)}");
                return false;
            }
        }
        public bool DeleteEntity(string entityName, Guid id, ProcessedCustomerResponse response)
        {
            try
            {
                _service.Delete(entityName, id);
                return true;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Exception while deleting entity in CRM with id: {id}: {ManagerHelper.GetExceptionMessage(ex)}";
                response.ErrorMessage = errorMsg;
                Log.Error(errorMsg);
                return false;
            }
        }




        #endregion

        #region Dispose Methods
        /// <summary>
        /// Dispose proxy.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose proxy.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                DisposeHelper.LoggedDispose(_service, Log);
            }
        }

        #endregion

    }
}
