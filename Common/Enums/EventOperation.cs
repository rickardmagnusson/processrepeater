﻿namespace Common.Enums
{
    public enum EventOperation
    {
        Import = 1,
        Retry = 2
    }
}
