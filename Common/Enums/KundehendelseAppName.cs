﻿namespace Common.Enums
{
    public enum KundehendelseAppName
    {
        tradex_source,
        nice_skade_source,
        tradex_web,
        nice_skade_web,
        tradex_raadgiver
    }
}
