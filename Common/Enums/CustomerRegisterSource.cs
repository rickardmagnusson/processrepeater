﻿namespace Common.Enums
{
    //public enum CustomerRegisterSource
    //{
    //    Undefined = 0,
    //    NiceSkade = 1,
    //    NicePerson = 2,
    //    Kerne = 3,
    //    Tradex = 4
    //}
    public enum CustomerRegisterSource
    {
        Undefined = 0,
        NiceSkade = 1,
        NicePerson = 2,
        Sdc = 3
    }
    public enum Source
    {
        Undefined = 0,
        NiceSkade = 1,
        NicePerson = 2,
        Kerne = 3,
        Tradex = 4,
        BanQsoft = 5
    }


    public enum CrmSource
    {
        Undefined = 0,
        Crm = 1,
        NicePerson = 2,
        Kerne = 3,
        Tradex = 4,
        BanQsoft = 5
    }
}
