﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum Env
    {
        DEV,
        Test,
        QA,
        PROD
    }


    public enum BatchSize
    {
        Default = 100,
        Test = 2
    }


    public enum RetryCounter
    {
        Default = 4
    }
}
