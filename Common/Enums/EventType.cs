﻿namespace Common.Enums
{
    public enum EventType
    {
        Undefined = 0,
        Insert = 1,
        Delta = 2,
        Delete = 3,
        Key = 4,
        KundeRegister = 5,
        KundeHendelse = 6,
        SaksHendelse = 7
    }
}
