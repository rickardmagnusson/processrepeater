﻿namespace Common.Enums
{
    public enum MessageInboxStatus
    {
        Undefined = 0,
        Received = 1,
        Completed = 2,
        Ignored = 3,
        Failed = 4
    }
}
