﻿using Common.Logging;
using System;
using Common.Enums;

namespace Common.Helpers
{


    public abstract class EnumClassUtils<TClass> where TClass : class
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("enum-utils");

        public static TEnum Parse<TEnum>(string value) where TEnum : struct, TClass
        {
            if(string.IsNullOrEmpty(value)) value = string.Empty;
            var status = value.ToLower();

            foreach (var st in Enum.GetValues(typeof(TEnum)))
            {
                if (status.Equals(st.ToString().ToLower()))
                {
                    return (TEnum)st;
                }
            }

            Log.Warn($"Unable to map enum '{typeof(TEnum)}' of value '{value}'");

            if (typeof(TEnum) == typeof(CapChannel))
            {
                foreach (var st in Enum.GetValues(typeof(TEnum)))
                {
                    if ("ukjent".Equals(st.ToString().ToLower()))
                    {
                        return (TEnum)st;
                    }
                }
            }


            return new TEnum();
        }
    }

    public class EnumUtils : EnumClassUtils<Enum>
    {
    }
}
