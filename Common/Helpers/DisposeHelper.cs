﻿using System;
using System.Globalization;
using Common.Logging;
using log4net;

namespace Common.Helpers
{
    public class DisposeHelper
    {
        public static void LoggedDispose(IDisposable disposable, MsmqLogger Log)
        {
            try
            {
                if (disposable == null) return;
                disposable.Dispose();
                disposable = null;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format(CultureInfo.InvariantCulture, "CleanUp Exception message: {0}, stack trace: {1}.", ex.Message, ex.StackTrace));
                throw;
            }

        }
    }
}
