﻿using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.Type;
using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Xml.Serialization;

namespace Azure.Queues.Process.Data
{

    /// <summary>
    /// nHibernate Helper, create a max length of column.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class XmlType<T> : MutableType
    {
        public XmlType()
            : base(new XmlSqlType())
        {
        }


        public XmlType(SqlType sqlType)
            : base(sqlType)
        {
        }

        public override string Name
        {
            get { return "XmlOfT"; }
        }

        public override System.Type ReturnedClass
        {
            get { return typeof(T); }
        }

        public override void Set(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            ((IDataParameter)cmd.Parameters[index]).Value = XmlUtil.ConvertToXml(value);
        }

        public override object Get(DbDataReader rs, int index, ISessionImplementor session)
        {
            string xmlString = Convert.ToString(rs.GetValue(index));
            return FromStringValue(xmlString);
        }

        public override object Get(DbDataReader rs, string name, ISessionImplementor session)
        {
            string xmlString = Convert.ToString(rs[name]);
            return FromStringValue(xmlString);
        }


        public override string ToString(object val)
        {
            return val == null ? null : XmlUtil.ConvertToXml(val);
        }

        public override object FromStringValue(string xml)
        {
            if (xml != null)
            {
                return XmlUtil.FromXml<T>(xml);
            }
            return null;
        }

        public override object DeepCopyNotNull(object value)
        {
            var original = (T)value;
            var copy = XmlUtil.FromXml<T>(XmlUtil.ConvertToXml(original));
            return copy;
        }

        public override bool IsEqual(object x, object y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                return false;
            }
            return XmlUtil.ConvertToXml(x) == XmlUtil.ConvertToXml(y);
        }


    }

    //the methods from this class are also available at: http://blog.nitriq.com/PutDownTheXmlNodeAndStepAwayFromTheStringBuilder.aspx
    public static class XmlUtil
    {
        public static string ConvertToXml(object item)
        {
            XmlSerializer xmlser = new XmlSerializer(item.GetType());
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                xmlser.Serialize(ms, item);
                UTF8Encoding textconverter = new UTF8Encoding();
                return textconverter.GetString(ms.ToArray());
            }
        }

        public static T FromXml<T>(string xml)
        {
            XmlSerializer xmlser = new XmlSerializer(typeof(T));
            using (System.IO.StringReader sr = new System.IO.StringReader(xml))
            {
                return (T)xmlser.Deserialize(sr);
            }
        }

    }

}
