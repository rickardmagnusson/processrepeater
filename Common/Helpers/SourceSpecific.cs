﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;
using Common.Enums;

namespace Common.Helpers
{
    public class SourceSpecific
    {
        public static readonly Dictionary<Source, TerritoryCode> PrioList = new Dictionary<Source, TerritoryCode>
        {
            {Source.Kerne, TerritoryCode.Kjerne},
            {Source.NiceSkade, TerritoryCode.Nice},
            {Source.NicePerson, TerritoryCode.Nice},    
            {Source.BanQsoft, TerritoryCode.BanQsoft},
            {Source.Tradex, TerritoryCode.Tradex},
        };


        public static readonly Dictionary<CustomerRegisterSource, string> NiceSourceIdMapper = new Dictionary<CustomerRegisterSource, string>
        {
            {CustomerRegisterSource.Sdc, "cap_sourceid"},
            {CustomerRegisterSource.NiceSkade, "cap_nicesourceid"},
            {CustomerRegisterSource.NicePerson, "cap_nice2sourceid"},
        };
        public static readonly Dictionary<Source, string> KunderegisterSourceIdMapper = new Dictionary<Source, string>
        {
            {Source.Kerne, "cap_sourceid"},
            {Source.NiceSkade, "cap_nicesourceid"},
            {Source.NicePerson, "cap_nice2sourceid"},
            {Source.BanQsoft, "cap_banqsoftsourceid"},
            {Source.Tradex, "cap_tradexsourceid"},
        };
        

        public static readonly Dictionary<CustomerRegisterSource, int> NiceTerritoryCodeMapper = new Dictionary<CustomerRegisterSource, int>
        {
            {CustomerRegisterSource.Sdc, (int)TerritoryCode.Kjerne},
            {CustomerRegisterSource.NiceSkade, (int)TerritoryCode.Nice},
            {CustomerRegisterSource.NicePerson, (int)TerritoryCode.Nice},
        };
        public static readonly Dictionary<Source, int> KunderegisterTerritoryCodeMapper = new Dictionary<Source, int>
        {
            {Source.Kerne, (int)TerritoryCode.Kjerne},
            {Source.NiceSkade, (int)TerritoryCode.Nice},
            {Source.NicePerson, (int)TerritoryCode.Nice},
            {Source.BanQsoft, (int)TerritoryCode.BanQsoft},
            {Source.Tradex, (int)TerritoryCode.Tradex},
        };


    }
}
