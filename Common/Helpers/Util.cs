﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Common.Helpers
{
    public class Util
    {
        public static int GetGenderCodeFromGovernmentId(string governmentid)
        {
            if (string.IsNullOrEmpty(governmentid) || governmentid.Length != 11)
            {
                return -1;
            }

            return int.Parse(governmentid[8].ToString()) % 2 == 0 ? Common.Constants.Gender.Female : Common.Constants.Gender.Male;
        }


        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetMethodName()
        {
            var st = new StackTrace(new StackFrame(1));
            return st.GetFrame(0).GetMethod().Name;
        }

    }
}
