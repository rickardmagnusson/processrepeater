﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Common.Entities;
using Common.Enums;
using Common.Logging;
using log4net;

namespace Common
{
    public class LogHelper
    {

        public static void LogDeleteOutcomes(string msg, string sourceId, MsmqLogger log)
        {
            LogWithAdditionalMessage(msg, $"Delete with SourceId: {sourceId}", LogLevel.Info, log);
        }

        public static void LogWithAdditionalMessage(string msg, string additionalMessage, LogLevel level, MsmqLogger log)
        {
            LogicalThreadContext.Properties["AdditionalData"] = additionalMessage;

            switch (level)
            {
                case LogLevel.Debug:
                    log.Debug(msg);
                    break;
                case LogLevel.Info:
                    log.Info(msg);
                    break;
                case LogLevel.Warn:
                    log.Warn(msg);
                    break;
                case LogLevel.Error:
                    log.Error(msg);
                    break;
                case LogLevel.Fatal:
                    log.Fatal(msg);
                    break;
            }

            LogicalThreadContext.Properties["AdditionalData"] = "";
        }
        public static void Msg(string msg, ProcessedCustomerResponse response, LogLevel level, MsmqLogger Log, bool isIgnored = false)
        {
            SetLogAndResponse(msg, response, level, Log, isIgnored);
        }
        public static void Error(string methodName, Exception ex, ProcessedCustomerResponse response, MsmqLogger Log)
        {
            //SetLogAndResponse($"Error while processing {methodName}. Error message: {ManagerHelper.GetExceptionMessage(ex)}", response, LogLevel.Error, Log);
            SetLogAndResponse($"Error while processing {methodName}. Error message: {ex.Message}", response, LogLevel.Error, Log);
            LogInnerExceptions(ex, Log);
            LogStackTrace(ex, Log);
        }
        public static void Error(string methodName, Exception ex, MsmqLogger Log, string infoText = "")
        {
            var currMethod = GetCurrentMethod();

            var type = ex.GetType().ToString();
            if (type.StartsWith("System")) type = type.Split(new[] { "System." }, StringSplitOptions.None)[1];

            Log.Error($"Error at method '{currMethod}'. {infoText}".TrimEnd(), new Exception($"({type}) {ex.Message}"));

            LogInnerExceptions(ex, Log);

            LogStackTrace(ex, Log);
        }


        private static void LogInnerExceptions(Exception ex, MsmqLogger Log)
        {
            foreach (var inner in GetInnerExceptions(ex)) Log.Debug("Inner Exception", new Exception(inner));
        }
        private static void LogStackTrace(Exception ex, MsmqLogger Log)
        {
            var stackTrace = ex.StackTrace.Trim().Split(new[] { "at " }, StringSplitOptions.None).ToList();
            stackTrace.RemoveAt(0);
            foreach (var stack in stackTrace) Log.Debug("Stack trace.", new Exception($"at {stack.TrimEnd()}"));
        }

        private static void SetLogAndResponse(string msg, ProcessedCustomerResponse response, LogLevel level, MsmqLogger log, bool setIgnore = false)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    log.Debug(msg);
                    break;
                case LogLevel.Info:
                    log.Info(msg);
                    break;
                case LogLevel.Warn:
                    log.Warn(msg);
                    break;
                case LogLevel.Error:
                    log.Error(msg);
                    break;
                case LogLevel.Fatal:
                    log.Fatal(msg);
                    break;
            }

            response.ErrorMessage = msg;
            response.Success = false;
            response.FailedProcessing = true; // TODO, deprecate
            if (setIgnore) response.IsIgnored = true;
        }
        private static List<string> GetInnerExceptions(Exception ex)
        {
            var list = new List<string>();

            var innerExc = ex.InnerException;
            while (true)
            {
                if (innerExc != null)
                {
                    list.Add(innerExc.Message);
                }
                else break;

                innerExc = innerExc.InnerException;
            }

            return list;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static string GetCurrentMethod()
        {
            var methodName = string.Empty;
            var next = false;
            var st = new StackTrace();

            for (var i = 0; i < st.FrameCount; i++)
            {
                var currName = st.GetFrame(i).GetMethod().Name;
                //Console.WriteLine(currName);

                if (next
                    && !currName.Equals("MoveNext", StringComparison.CurrentCultureIgnoreCase)
                    && !currName.Equals("Start", StringComparison.CurrentCultureIgnoreCase)
                    && !currName.Equals("SetStandardUnexpectedErrorToResponse", StringComparison.CurrentCultureIgnoreCase))
                {
                    methodName = currName;
                    break;
                }

                if (currName.Equals("Error", StringComparison.CurrentCultureIgnoreCase))
                {
                    next = true;
                }
            }

            return methodName;
        }




    }
}
