﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Core;
using log4net.Config;
using System.IO;
using log4net.Appender;

namespace Common.Logging
{
    public class MsmqLogger
    {
        private static Dictionary<string, MsmqLogger> _loggers = new Dictionary<string, MsmqLogger>();
        //private static bool _startUp = true;

        private ILog _log;

        private MsmqLogger() { }

        private MsmqLogger(string loggerName, string filename)
        {
            _log = LogManager.GetLogger(loggerName);
            //if (_startUp)
            //{
            //    XmlConfigurator.Configure(new FileInfo(@"Logging\log4net.config"));
            //    _startUp = false;
            //}
            if (!string.IsNullOrEmpty(filename)) {
                SetLogFile(filename);
            }
            _loggers.Add(loggerName, this);
        }

        private void SetLogFile(string filename)
        {
            var hierarchy = (log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository();

            foreach(var appender in hierarchy.Root.Appenders)
            {
                if (appender.Name == "FileAppender")
                {
                    (appender as FileAppender).File = filename;
                    (appender as FileAppender).ActivateOptions();
                    break;
                }
            }
        }

        public static MsmqLogger CreateLogger(string loggerName, string filename = null)
        {
            return _loggers.ContainsKey(loggerName) ? _loggers[loggerName] : new MsmqLogger(loggerName, filename);
        }

        public void Debug(object message, string additionalData = null)
        {
            LogByLevel(LogLevel.Debug, message, additionalData: additionalData);
        }

        public void Debug(object message, Exception exception, string additionalData = null)
        {
            LogByLevel(LogLevel.Debug, message, exception: exception, additionalData: additionalData);
        }

        public void Error(object message, string additionalData = null)
        {
            LogByLevel(LogLevel.Error, message, additionalData: additionalData);
        }

        public void Error(object message, Exception exception, string additionalData = null)
        {
            LogByLevel(LogLevel.Error, message, exception: exception, additionalData: additionalData);
        }

        public void Fatal(object message, string additionalData = null)
        {
            LogByLevel(LogLevel.Fatal, message, additionalData: additionalData);
        }

        public void Fatal(object message, Exception exception, string additionalData = null)
        {
            LogByLevel(LogLevel.Fatal, message, exception: exception, additionalData: additionalData);
        }

        public void Info(object message, string additionalData = null)
        {
            LogByLevel(LogLevel.Info, message, additionalData: additionalData);
        }

        public void Info(object message, Exception exception, string additionalData = null)
        {
            LogByLevel(LogLevel.Info, message, exception: exception, additionalData: additionalData);
        }

        public void Warn(object message, string additionalData = null)
        {
            LogByLevel(LogLevel.Warn, message, additionalData: additionalData);
        }

        public void Warn(object message, Exception exception, string additionalData = null)
        {
            LogByLevel(LogLevel.Warn, message, exception: exception, additionalData: additionalData); 
        }

        private void LogByLevel(LogLevel level, object message, Exception exception = null, string additionalData = null)
        {
            if (additionalData != null)
            {
                log4net.LogicalThreadContext.Properties["AdditionalData"] = additionalData;
            }

            switch(level)
            {
                case LogLevel.Debug:
                    if (exception != null)
                        _log.Debug(message, exception);
                    else
                        _log.Debug(message);
                    break;
                case LogLevel.Info:
                    if (exception != null)
                        _log.Info(message, exception);
                    else
                        _log.Info(message);
                    break;
                case LogLevel.Warn:
                    if (exception != null)
                        _log.Warn(message, exception);
                    else
                        _log.Warn(message);
                    break;
                case LogLevel.Error:
                    if (exception != null)
                        _log.Error(message, exception);
                    else
                        _log.Error(message);
                    break;
                case LogLevel.Fatal:
                    if (exception != null)
                        _log.Fatal(message, exception);
                    else
                        _log.Fatal(message);
                    break;
            }

            log4net.LogicalThreadContext.Properties["AdditionalData"] = null;
        }

        private enum LogLevel
        {
            Debug = 1,
            Info = 2,
            Warn = 3,
            Error = 4,
            Fatal = 5
        }
    }
}
