﻿using System;
using Common.Enums;
using Common.Interfaces;
using Microsoft.Xrm.Sdk;

namespace Common.Entities
{
    public class DataWarehouseLite : IEntityMappable
    {
        #region Fields
        public EntityName Entity;

        public Guid Id { get; set; }
        public long SourceId { get; set; }
        public string CustomerNumber { get; set; }
        public DateTime CapEventdate { get; set; }
        public Guid OwnerId { get; set; }
        public Guid OwningTeamId { get; set; }
        public Guid CapDepartmentId { get; set; }
        public bool IsIgnored { get; set; }
        public bool IsCreate { get; set; }
        public bool HasDepartment { get; set; }
        public bool ForceCustomerNumberChange { get; set; }
        public CustomerTypeCode Type { get; set; }
        public string ErrorMessage { get; set; }
        public int StateCode { get; set; }

        public OptionSetValue GenderCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string StreetName { get; set; }
        public string HouseNumber { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string StateOrProvince { get; set; }
        public string AddressCountryCode { get; set; } 
        public string TelephoneMobile { get; set; }
        public string TelephoneMobileWork { get; set; }
        public string TelephoneWork { get; set; }
        public string TelephoneHome { get; set; }
        public string EmailPrivate { get; set; }
        public string EmailWork { get; set; }
        public string CustomerAccountManager { get; set; }
        #endregion

        public Entity MapToEntity(Entity targetEntity)
        {
            targetEntity["cap_eventdate"] = CapEventdate != DateTime.MinValue ? CapEventdate : DateTime.Now;

            #region Sdc SourceId
            if (SourceId > 0)
            {
                targetEntity["cap_sourceid"] = SourceId.ToString();
            }

            #endregion
            #region Department
            if (CapDepartmentId != Guid.Empty)
            {
                if (IsCreate || Type != CustomerTypeCode.PotentialCustomer || !HasDepartment)
                {
                    targetEntity["cap_departmentid"] = new EntityReference("cap_department", CapDepartmentId);
                }
            }
            #endregion
            #region Owner
            if (OwnerId != Guid.Empty)
            {
                targetEntity["ownerid"] = new EntityReference("systemuser", OwnerId);
            }
            else
            {
                if (IsCreate || (string.IsNullOrEmpty(CustomerAccountManager) && Type != CustomerTypeCode.PotentialCustomer))
                {
                    targetEntity["ownerid"] = new EntityReference("team", OwningTeamId);
                }
            }
            #endregion

            #region Address & Email
            if (!string.IsNullOrEmpty(StreetName))
            {
                var input = $"{StreetName} {HouseNumber}";
                if (input.Length <= 250)
                {
                    targetEntity.SetValueReflect("address1_line1", input);
                    targetEntity.SetValueReflect("address1_line2", string.Empty);
                    targetEntity.SetValueReflect("address1_line3", string.Empty);
                }
                else
                {
                    targetEntity.SetValueReflect("address1_line1", input.Substring(0,250));
                    targetEntity.SetValueReflect("address1_line2", input.Substring(250));
                    targetEntity.SetValueReflect("address1_line3", string.Empty);
                }
            }
            else
            {
                targetEntity.SetValueReflect("address1_line1", string.Empty);
                targetEntity.SetValueReflect("address1_line2", string.Empty);
                targetEntity.SetValueReflect("address1_line3", string.Empty);
            }

            targetEntity.SetValueReflect("address1_postalcode", PostalCode);
            targetEntity.SetValueReflect("address1_stateorprovince", StateOrProvince);
            targetEntity.SetValueReflect("address1_country", AddressCountryCode);
            targetEntity.SetValueReflect("address1_city", City);
            targetEntity.SetValueReflect("address1_county", string.Empty);

            targetEntity.SetValueReflect("emailaddress2", string.Empty);
            #endregion

            #region Contact specific
            if (Entity == Enums.EntityName.contact)
            {
                if (IsCreate || ForceCustomerNumberChange)
                {
                    targetEntity.SetValueIfPresent("governmentid", CustomerNumber);

                    var dateOfBirth = Helper.CalculateDoB(CustomerNumber);
                    if (dateOfBirth != null) targetEntity["birthdate"] = dateOfBirth;
                }


                targetEntity.SetValueReflect("firstname", Helper.GiveContactNameCrmStandard(FirstName));
                targetEntity.SetValueReflect("lastname", Helper.GiveContactNameCrmStandard(LastName));
                //targetEntity.SetValueReflect("firstname", FirstName);
                //targetEntity.SetValueReflect("lastname", LastName);

                targetEntity.SetValueReflect("mobilephone", TelephoneMobile);

                //targetEntity.SetValueReflect("telephone1", TelephoneHome);
                //targetEntity.SetValueReflect("telephone2", TelephoneWork);

                targetEntity.SetValueReflect("emailaddress1", EmailPrivate);
                //targetEntity.SetValueReflect("emailaddress2", EmailWork);

                if (GenderCode.Value != -1) targetEntity["gendercode"] = GenderCode;
            }
            #endregion
            #region Account specific
            if (Entity == Enums.EntityName.account)
            {
                if (IsCreate || ForceCustomerNumberChange)
                {
                    targetEntity.SetValueIfPresent("accountnumber", CustomerNumber);
                }

                targetEntity.SetValueReflect("name", !string.IsNullOrEmpty(CompanyName)
                        ? Helper.GiveAccountNameCrmStandard(string.Empty, CompanyName)
                        : Helper.GiveAccountNameCrmStandard(FirstName, LastName));
                //targetEntity.SetValueReflect("name", CompanyName);


                targetEntity.SetValueReflect("telephone1", TelephoneWork);
                //targetEntity.SetValueReflect("telephone2", TelephoneHome);
                //targetEntity.SetValueReflect("telephone3", TelephoneMobile);

                targetEntity.SetValueReflect("emailaddress1", EmailWork);
                //targetEntity.SetValueReflect("emailaddress2", EmailPrivate);
            }
            #endregion

            if (Id != Guid.Empty)
            {
                targetEntity["statecode"] = new OptionSetValue(0);
                targetEntity["statuscode"] = new OptionSetValue(1);
            }

            targetEntity["customertypecode"] = new OptionSetValue((int)CustomerTypeCode.Customer);
            targetEntity["territorycode"] = new OptionSetValue(809020000);

            return targetEntity;
        }

        public string EntityName => Entity.ToString().ToLower();

    }
}
