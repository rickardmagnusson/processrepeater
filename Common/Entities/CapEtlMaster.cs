﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace Common.Entities
{
    /// <summary>
    /// ETL Master entity to maintain the data load to CRM
    /// </summary>
    [System.Runtime.Serialization.DataContractAttribute()]
    [EntityLogicalName("cap_etlmaster")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "8.0.1.7297")]
    public partial class cap_etlmaster : Entity, System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
    {

        public struct Fields
        {
            public const string cap_bankno = "cap_bankno";
            public const string cap_entity = "cap_entity";
            public const string cap_error_message = "cap_error_message";
            public const string cap_etl_end = "cap_etl_end";
            public const string cap_etl_start = "cap_etl_start";
            public const string cap_etlmasterId = "cap_etlmasterid";
            public const string Id = "cap_etlmasterid";
            public const string cap_fromeventid = "cap_fromeventid";
            public const string cap_loadid = "cap_loadid";
            public const string cap_numcreated = "cap_numcreated";
            public const string cap_numdeleted = "cap_numdeleted";
            public const string cap_numerrors = "cap_numerrors";
            public const string cap_numfetched = "cap_numfetched";
            public const string cap_numignored = "cap_numignored";
            public const string cap_numupdated = "cap_numupdated";
            public const string cap_ondemand_flag = "cap_ondemand_flag";
            public const string cap_retrycount = "cap_retrycount";
            public const string cap_toeventid = "cap_toeventid";
            public const string CreatedBy = "createdby";
            public const string CreatedOn = "createdon";
            public const string CreatedOnBehalfBy = "createdonbehalfby";
            public const string ImportSequenceNumber = "importsequencenumber";
            public const string ModifiedBy = "modifiedby";
            public const string ModifiedOn = "modifiedon";
            public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
            public const string OverriddenCreatedOn = "overriddencreatedon";
            public const string OwnerId = "ownerid";
            public const string OwningBusinessUnit = "owningbusinessunit";
            public const string OwningTeam = "owningteam";
            public const string OwningUser = "owninguser";
            public const string statecode = "statecode";
            public const string statuscode = "statuscode";
            public const string TimeZoneRuleVersionNumber = "timezoneruleversionnumber";
            public const string UTCConversionTimeZoneCode = "utcconversiontimezonecode";
            public const string VersionNumber = "versionnumber";
            public const string business_unit_cap_etlmaster = "business_unit_cap_etlmaster";
            public const string lk_cap_etlmaster_createdby = "lk_cap_etlmaster_createdby";
            public const string lk_cap_etlmaster_createdonbehalfby = "lk_cap_etlmaster_createdonbehalfby";
            public const string lk_cap_etlmaster_modifiedby = "lk_cap_etlmaster_modifiedby";
            public const string lk_cap_etlmaster_modifiedonbehalfby = "lk_cap_etlmaster_modifiedonbehalfby";
            public const string team_cap_etlmaster = "team_cap_etlmaster";
            public const string user_cap_etlmaster = "user_cap_etlmaster";
        }


        /// <summary>
        /// Default Constructor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode()]
        public cap_etlmaster() :
                base(EntityLogicalName)
        {
        }

        public const string EntityLogicalName = "cap_etlmaster";

        public const int EntityTypeCode = 10005;

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;

        [System.Diagnostics.DebuggerNonUserCode()]
        private void OnPropertyChanged(string propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        [System.Diagnostics.DebuggerNonUserCode()]
        private void OnPropertyChanging(string propertyName)
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, new System.ComponentModel.PropertyChangingEventArgs(propertyName));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_bankno")]
        public string cap_bankno
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<string>("cap_bankno");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_bankno");
                this.SetAttributeValue("cap_bankno", value);
                this.OnPropertyChanged("cap_bankno");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_entity")]
        public string cap_entity
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<string>("cap_entity");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_entity");
                this.SetAttributeValue("cap_entity", value);
                this.OnPropertyChanged("cap_entity");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_error_message")]
        public string cap_error_message
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<string>("cap_error_message");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_error_message");
                this.SetAttributeValue("cap_error_message", value);
                this.OnPropertyChanged("cap_error_message");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_etl_end")]
        public DateTime? cap_etl_end
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("cap_etl_end");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_etl_end");
                this.SetAttributeValue("cap_etl_end", value);
                this.OnPropertyChanged("cap_etl_end");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_etl_start")]
        public DateTime? cap_etl_start
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("cap_etl_start");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_etl_start");
                this.SetAttributeValue("cap_etl_start", value);
                this.OnPropertyChanged("cap_etl_start");
            }
        }

        /// <summary>
        /// Unique identifier for entity instances
        /// </summary>
        [AttributeLogicalName("cap_etlmasterid")]
        public System.Nullable<System.Guid> cap_etlmasterId
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<System.Guid>>("cap_etlmasterid");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_etlmasterId");
                this.SetAttributeValue("cap_etlmasterid", value);
                if (value.HasValue)
                {
                    base.Id = value.Value;
                }
                else
                {
                    base.Id = System.Guid.Empty;
                }
                this.OnPropertyChanged("cap_etlmasterId");
            }
        }

        [AttributeLogicalName("cap_etlmasterid")]
        public override System.Guid Id
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return base.Id;
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.cap_etlmasterId = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_fromeventid")]
        public string cap_fromeventid
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<string>("cap_fromeventid");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_fromeventid");
                this.SetAttributeValue("cap_fromeventid", value);
                this.OnPropertyChanged("cap_fromeventid");
            }
        }

        /// <summary>
        /// The name of the custom entity.
        /// </summary>
        [AttributeLogicalName("cap_loadid")]
        public string cap_loadid
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<string>("cap_loadid");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_loadid");
                this.SetAttributeValue("cap_loadid", value);
                this.OnPropertyChanged("cap_loadid");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_numcreated")]
        public System.Nullable<int> cap_numcreated
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("cap_numcreated");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_numcreated");
                this.SetAttributeValue("cap_numcreated", value);
                this.OnPropertyChanged("cap_numcreated");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_numdeleted")]
        public System.Nullable<int> cap_numdeleted
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("cap_numdeleted");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_numdeleted");
                this.SetAttributeValue("cap_numdeleted", value);
                this.OnPropertyChanged("cap_numdeleted");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_numerrors")]
        public System.Nullable<int> cap_numerrors
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("cap_numerrors");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_numerrors");
                this.SetAttributeValue("cap_numerrors", value);
                this.OnPropertyChanged("cap_numerrors");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_numfetched")]
        public System.Nullable<int> cap_numfetched
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("cap_numfetched");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_numfetched");
                this.SetAttributeValue("cap_numfetched", value);
                this.OnPropertyChanged("cap_numfetched");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_numignored")]
        public System.Nullable<int> cap_numignored
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("cap_numignored");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_numignored");
                this.SetAttributeValue("cap_numignored", value);
                this.OnPropertyChanged("cap_numignored");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_numupdated")]
        public System.Nullable<int> cap_numupdated
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("cap_numupdated");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_numupdated");
                this.SetAttributeValue("cap_numupdated", value);
                this.OnPropertyChanged("cap_numupdated");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_ondemand_flag")]
        public System.Nullable<bool> cap_ondemand_flag
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("cap_ondemand_flag");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_ondemand_flag");
                this.SetAttributeValue("cap_ondemand_flag", value);
                this.OnPropertyChanged("cap_ondemand_flag");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_retrycount")]
        public System.Nullable<int> cap_retrycount
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("cap_retrycount");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_retrycount");
                this.SetAttributeValue("cap_retrycount", value);
                this.OnPropertyChanged("cap_retrycount");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("cap_toeventid")]
        public string cap_toeventid
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<string>("cap_toeventid");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("cap_toeventid");
                this.SetAttributeValue("cap_toeventid", value);
                this.OnPropertyChanged("cap_toeventid");
            }
        }

        /// <summary>
        /// Unique identifier of the user who created the record.
        /// </summary>
        [AttributeLogicalName("createdby")]
        public EntityReference CreatedBy
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<EntityReference>("createdby");
            }
        }

        /// <summary>
        /// Date and time when the record was created.
        /// </summary>
        [AttributeLogicalName("createdon")]
        public System.Nullable<System.DateTime> CreatedOn
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("createdon");
            }
        }

        /// <summary>
        /// Unique identifier of the delegate user who created the record.
        /// </summary>
        [AttributeLogicalName("createdonbehalfby")]
        public EntityReference CreatedOnBehalfBy
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<EntityReference>("createdonbehalfby");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("CreatedOnBehalfBy");
                this.SetAttributeValue("createdonbehalfby", value);
                this.OnPropertyChanged("CreatedOnBehalfBy");
            }
        }

        /// <summary>
        /// Sequence number of the import that created this record.
        /// </summary>
        [AttributeLogicalName("importsequencenumber")]
        public System.Nullable<int> ImportSequenceNumber
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("importsequencenumber");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("ImportSequenceNumber");
                this.SetAttributeValue("importsequencenumber", value);
                this.OnPropertyChanged("ImportSequenceNumber");
            }
        }

        /// <summary>
        /// Unique identifier of the user who modified the record.
        /// </summary>
        [AttributeLogicalName("modifiedby")]
        public EntityReference ModifiedBy
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<EntityReference>("modifiedby");
            }
        }

        /// <summary>
        /// Date and time when the record was modified.
        /// </summary>
        [AttributeLogicalName("modifiedon")]
        public System.Nullable<System.DateTime> ModifiedOn
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("modifiedon");
            }
        }

        /// <summary>
        /// Unique identifier of the delegate user who modified the record.
        /// </summary>
        [AttributeLogicalName("modifiedonbehalfby")]
        public EntityReference ModifiedOnBehalfBy
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<EntityReference>("modifiedonbehalfby");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("ModifiedOnBehalfBy");
                this.SetAttributeValue("modifiedonbehalfby", value);
                this.OnPropertyChanged("ModifiedOnBehalfBy");
            }
        }

        /// <summary>
        /// Date and time that the record was migrated.
        /// </summary>
        [AttributeLogicalName("overriddencreatedon")]
        public System.Nullable<System.DateTime> OverriddenCreatedOn
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("overriddencreatedon");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("OverriddenCreatedOn");
                this.SetAttributeValue("overriddencreatedon", value);
                this.OnPropertyChanged("OverriddenCreatedOn");
            }
        }

        /// <summary>
        /// Owner Id
        /// </summary>
        [AttributeLogicalName("ownerid")]
        public EntityReference OwnerId
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<EntityReference>("ownerid");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("OwnerId");
                this.SetAttributeValue("ownerid", value);
                this.OnPropertyChanged("OwnerId");
            }
        }

        /// <summary>
        /// Unique identifier for the business unit that owns the record
        /// </summary>
        [AttributeLogicalName("owningbusinessunit")]
        public EntityReference OwningBusinessUnit
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<EntityReference>("owningbusinessunit");
            }
        }

        /// <summary>
        /// Unique identifier for the team that owns the record.
        /// </summary>
        [AttributeLogicalName("owningteam")]
        public EntityReference OwningTeam
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<EntityReference>("owningteam");
            }
        }

        /// <summary>
        /// Unique identifier for the user that owns the record.
        /// </summary>
        [AttributeLogicalName("owninguser")]
        public EntityReference OwningUser
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<EntityReference>("owninguser");
            }
        }

        /// <summary>
        /// Status of the ETL Master
        /// </summary>
        [AttributeLogicalName("statecode")]
        public cap_etlmasterState? statecode
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                OptionSetValue optionSet = this.GetAttributeValue<OptionSetValue>("statecode");
                if ((optionSet != null))
                {
                    return ((cap_etlmasterState)(System.Enum.ToObject(typeof(cap_etlmasterState), optionSet.Value)));
                }
                else
                {
                    return null;
                }
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("statecode");
                if ((value == null))
                {
                    this.SetAttributeValue("statecode", null);
                }
                else
                {
                    this.SetAttributeValue("statecode", new OptionSetValue(((int)(value))));
                }
                this.OnPropertyChanged("statecode");
            }
        }

        /// <summary>
        /// Reason for the status of the ETL Master
        /// </summary>
        [AttributeLogicalName("statuscode")]
        public OptionSetValue statuscode
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<OptionSetValue>("statuscode");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("statuscode");
                this.SetAttributeValue("statuscode", value);
                this.OnPropertyChanged("statuscode");
            }
        }

        /// <summary>
        /// For internal use only.
        /// </summary>
        [AttributeLogicalName("timezoneruleversionnumber")]
        public int? TimeZoneRuleVersionNumber
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("timezoneruleversionnumber");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("TimeZoneRuleVersionNumber");
                this.SetAttributeValue("timezoneruleversionnumber", value);
                this.OnPropertyChanged("TimeZoneRuleVersionNumber");
            }
        }

        /// <summary>
        /// Time zone code that was in use when the record was created.
        /// </summary>
        [AttributeLogicalName("utcconversiontimezonecode")]
        public int? UTCConversionTimeZoneCode
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("utcconversiontimezonecode");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("UTCConversionTimeZoneCode");
                this.SetAttributeValue("utcconversiontimezonecode", value);
                this.OnPropertyChanged("UTCConversionTimeZoneCode");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [AttributeLogicalName("versionnumber")]
        public long? VersionNumber
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<System.Nullable<long>>("versionnumber");
            }
        }

        /// <summary>
        /// Constructor for populating via LINQ queries given a LINQ anonymous type
        /// <param name="anonymousType">LINQ anonymous type.</param>
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode()]
        public cap_etlmaster(object anonymousType) :
                this()
        {
            foreach (var p in anonymousType.GetType().GetProperties())
            {
                var value = p.GetValue(anonymousType, null);
                var name = p.Name.ToLower();

                if (name.EndsWith("enum") && value.GetType().BaseType == typeof(System.Enum))
                {
                    value = new OptionSetValue((int)value);
                    name = name.Remove(name.Length - "enum".Length);
                }

                switch (name)
                {
                    case "id":
                        base.Id = (System.Guid)value;
                        Attributes["cap_etlmasterid"] = base.Id;
                        break;
                    case "cap_etlmasterid":
                        var id = (System.Nullable<System.Guid>)value;
                        if (id == null) { continue; }
                        base.Id = id.Value;
                        Attributes[name] = base.Id;
                        break;
                    case "formattedvalues":
                        // Add Support for FormattedValues
                        FormattedValues.AddRange((FormattedValueCollection)value);
                        break;
                    default:
                        //Saml2MetadataConstants.Attributes[name] = value; // TODO why is this red?
                        break;
                }
            }
        }
        


    }

    [System.Runtime.Serialization.DataContractAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "8.0.1.7297")]
    public enum cap_etlmasterState
    {

        [System.Runtime.Serialization.EnumMemberAttribute()]
        Active = 0,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        Inactive = 1,
    }

}
