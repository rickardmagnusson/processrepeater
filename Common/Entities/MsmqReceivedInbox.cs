﻿using System;

namespace Common.Entities
{
    public partial class MsmqReceivedInbox
    {
        public Guid CrmGuid { get; set; }
        public string EventId { get; set; }
        public string CustomerId { get; set; }
        public string BankId { get; set; }
        public string EventType { get; set; }
        public string SourceName { get; set; }
        public string SourceId { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public int? PersonType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int? AddressType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Owner { get; set; }
        public bool? Shielded { get; set; }
        public string NewCustomerId { get; set; }
        public string NewBankId { get; set; }



        public bool IsProcessed { get; set; }
        public bool InProcess { get; set; }
    }

    public class MsmqReceivedInboxWithMetadata
    {

        /* BigInt ID
         * Timestamp Datetime2
         * BankId  - CHAR(4)
         * CorrId UniqueIDENTIFIER
         * Xml - xml eller varchar(max)
         * Status (1 = mottatt, 2 = ferdig, 3 = ignorert, 4 = feilet) - int
         * Retries - int
         * ProcessedAt - Datetime2
         */
    }


}
