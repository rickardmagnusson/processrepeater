﻿using System;
using System.Xml.Serialization;

namespace Common.Entities
{
    [Serializable]
    [XmlType]
    public class DataWarehouseTempEvent
    {
        [XmlElement("EventId", Order = 0)]
        public long EventId { get; set; }

        [XmlElement("TimeStamp", Order = 1)]
        public DateTime TimeStamp { get; set; }

        [XmlElement("BankCustomerSourceId", Order = 2)]
        public long BankCustomerSourceId { get; set; }

        [XmlElement("CustomerId", Order = 3)]
        public string CustomerId { get; set; }

        [XmlElement("DivisionName", Order = 4)]
        public string DivisionName { get; set; }

        [XmlElement("FirstName", Order = 5)]
        public string FirstName { get; set; }

        [XmlElement("LastName", Order = 6)]
        public string LastName { get; set; }

        [XmlElement("CompanyName", Order = 7)]
        public string CompanyName { get; set; }

        [XmlElement("CustomerAccountManager", Order = 8)]
        public string CustomerAccountManager { get; set; }

        [XmlElement("CustomerDepartment", Order = 9)]
        public string CustomerDepartment { get; set; }

        [XmlElement("CustomerDepartmentName", Order = 10)]
        public string CustomerDepartmentName { get; set; }

        [XmlElement("BusinessAreaCode", Order = 11)]
        public string BusinessAreaCode { get; set; }

        [XmlElement("BusinessAreaCodeName", Order = 12)]
        public string BusinessAreaCodeName { get; set; }

        [XmlElement("SectorCode", Order = 13)]
        public string SectorCode { get; set; }

        [XmlElement("SectorCodeName", Order = 14)]
        public string SectorCodeName { get; set; }

        [XmlElement("CustomerStatus", Order = 15)]
        public string CustomerStatus { get; set; }

        [XmlElement("CustomerFinancialStatus", Order = 16)]
        public string CustomerFinancialStatus { get; set; }

        [XmlElement("DwhAddress", Order = 17)]
        public DwhAddress DwhAddress { get; set; }

        [XmlElement("DwhPhone", Order = 18)]
        public DwhPhone DwhPhone { get; set; }

        [XmlElement("DwhEmail", Order = 19)]
        public DwhEmail DwhEmail { get; set; }
    }

    [Serializable]
    [XmlType]
    public class DwhAddress
    {
        [XmlElement(Order = 0)]
        public string StreetName { get; set; }

        [XmlElement(Order = 1)]
        public string HouseNumber { get; set; }

        [XmlElement(Order = 2)]
        public string PostalCode { get; set; }

        [XmlElement(Order = 3)]
        public string PostalName { get; set; }

        [XmlElement(Order = 4)]
        public string StateOrProvince { get; set; }

        [XmlElement(Order = 5)]
        public string AddressCountryCode { get; set; }
    }

    [Serializable]
    [XmlType]
    public class DwhPhone
    {
        [XmlElement(Order = 0)]
        public string TelephoneMobile { get; set; }

        [XmlElement(Order = 1)]
        public string TelephoneMobileWork { get; set; }

        [XmlElement(Order = 2)]
        public string TelephoneWork { get; set; }

        [XmlElement(Order = 3)]
        public string TelephoneHome { get; set; }
    }

    [Serializable]
    [XmlType]
    public class DwhEmail
    {
        [XmlElement(Order = 0)]
        public string EmailPrivate { get; set; }

        [XmlElement(Order = 1)]
        public string EmailWork { get; set; }
    }
}
