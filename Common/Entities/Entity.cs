﻿


using FluentNHibernate.Mapping;

namespace Common.Entities
{
    /// <summary>
    /// Entity
    /// </summary>
    /// <typeparam name="T">The type to map</typeparam>
    public abstract class Entity<T> : ClassMap<T> where T : class, IEntity
    {
    }
}
