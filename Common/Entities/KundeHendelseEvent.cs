﻿using System;
using System.Xml.Serialization;

namespace Common.Entities
{
    [Serializable]
    [XmlType]
    public class KundeHendelseEvent
    {
        [XmlElement]
        public string Id { get; set; }
        [XmlElement]
        public ProcessArea ProcessArea { get; set; }
        [XmlElement]
        public KundeHendelseApplication Application { get; set; }
        [XmlElement]
        public Fagsystem Source { get; set; }
        [XmlElement]
        public KundeHendelsePhase Phase { get; set; }
        [XmlElement]
        public KundeHendelseAction Action { get; set; }
        [XmlElement]
        public Produkt Produkt { get; set; }
        [XmlElement]
        public KundeHendelseChannel Channel { get; set; }
        [XmlElement]
        public DateTime Timestamp { get; set; }
        [XmlElement]
        public long Value { get; set; }
        [XmlElement]
        public long PreviousValue { get; set; }
        [XmlElement]
        public string Other { get; set; }
        [XmlElement]
        public string Owner { get; set; }
        [XmlElement]
        public string LastModifiedBy { get; set; }
        [XmlElement]
        public Customer Customer { get; set; }
    }


    public class KundeHendelseEvent2
    {
        public string Id { get; set; }
        public ProcessArea ProcessArea { get; set; }
        public KundeHendelseApplication Application { get; set; }
        public Fagsystem Source { get; set; }
        public KundeHendelsePhase Phase { get; set; }
        public KundeHendelseAction Action { get; set; }
        public Produkt Produkt { get; set; }
        public KundeHendelseChannel Channel { get; set; }
        public DateTime Timestamp { get; set; }
        public long Value { get; set; }
        public long PreviousValue { get; set; }
        public string Other { get; set; }
        public string Owner { get; set; }
        public string LastModifiedBy { get; set; }

        public Customer Customer { get; set; }
    }

    [Serializable]
    [XmlType]
    public enum ProcessArea
    {
        Undefined = 0,
        Infra = 1,
        Dagligbank = 2,
        Eiendom = 3,
        Forsikring = 4,
        Kreditt = 5,
        Kredittbank = 6,
        Marked = 7,
        Sparing = 8,
        Virksomhet = 9,
        Felles = 10
    }

    [Serializable]
    [XmlType]
    public enum KundeHendelsePhase
    {
        Undefined = 0,
        Interessert = 1,
        Soknad = 2,
        Salg = 3,
        Endret = 4,
        Avsluttet = 5
    }

    [Serializable]
    [XmlType]
    public enum KundeHendelseAction
    {
        Undefined = 0,
        Enkelthendelse = 1,
        Ny = 2,
        Utvidet = 3,
        Redusert = 4,
        Avsluttet = 5
    }

    [Serializable]
    [XmlType]
    public enum KundeHendelseChannel
    {
        Undefined = 0,
        IntraWeb = 1,
        Mobilbank = 2,
        Nettbank = 3,
        Web = 4,
        Ws = 5
    }

    [Serializable]
    [XmlType]
    public enum KundeHendelseApplication
    {
        Undefined = 0,
        BliKunde = 1,
        Skade = 2,
        Boliglan = 3,
        Salgpantlan = 4,
        Smalan = 5,
        Advent = 6,
        Fond = 7
    }

    [Serializable]
    [XmlType]
    public enum Fagsystem
    {
        UKJENT,
        KERNE,
        NICE_SKADE,
        NICE_PERSON,
        TRADEX,
        BANQSOFT
    }

    [Serializable]
    [XmlType]
    public class Produkt
    {
        [XmlElement]
        public string Name { get; set; }
        [XmlElement]
        public string ProduktKode { get; set; }
    }

    [Serializable]
    [XmlType]
    public class Customer
    {
        [XmlElement]
        public string SourceId { get; set; }
        [XmlElement]
        public string BankNumber { get; set; }
        [XmlElement]
        public string CustomerNumber { get; set; }
        [XmlElement]
        public string Firstname { get; set; }
        [XmlElement]
        public string Lastname { get; set; }
    }
}
