﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Common.Entities
{
    public class CommonCustomerEvent
    {
        [XmlElement]
        public string Uuid { get; set; }
        [XmlElement]
        public DateTime Tidspunkt { get; set; }
        [XmlElement]
        public string Applikasjon { get; set; }
        [XmlElement]
        public string ApplikasjonVersjon { get; set; }
        [XmlElement]
        public string FagSystem { get; set; }
        [XmlElement]
        public ProcessArea Prosessomraade { get; set; }
        [XmlElement]
        public string Channel { get; set; }
        [XmlElement]
        public string ProduktNavn { get; set; }
        [XmlElement]
        public string ProduktKode { get; set; }
        [XmlElement]
        public string Bankregnr { get; set; }
        [XmlElement]
        public string RaadgiverId { get; set; }
        [XmlElement]
        public string FagsystemKundenummer { get; set; }
        [XmlElement]
        public Interessent Interessent { get; set; }
        [XmlElement]
        public string SakUuid { get; set; }
        [XmlElement]
        public string SakId { get; set; }


        [XmlElement]
        public long Verdi { get; set; }
        [XmlElement]
        public long TidligereVerdi { get; set; }
        [XmlElement]
        public List<Attributt> Attributter { get; set; }


        [XmlElement]
        public KundeHendelseType KundeHendelseType { get; set; }
        [XmlElement]
        public SakHendelseType SakHendelseType { get; set; }
    }


    public class KundeHendelseType
    {
        [XmlElement]
        public string Fase { get; set; }
        [XmlElement]
        public string TransaksjonsType { get; set; }
        [XmlElement]
        public string Handling { get; set; }
    }

    public class SakHendelseType
    {

        [XmlElement]
        public string SakHendelsesType { get; set; }
        [XmlElement]
        public string SakStatus { get; set; }
        [XmlElement]
        public string SakType { get; set; }
        [XmlElement]
        public string SakOppdatering { get; set; }
    }
}
