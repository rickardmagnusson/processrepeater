﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Constants
{
    public static class Gender
    {
        public const int Male = 1;
        public const int Female = 2;
    }
}
