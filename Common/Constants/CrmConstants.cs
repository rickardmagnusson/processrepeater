﻿namespace Common.Constants
{
    public static class CrmConstants
    {
        public const string AccountEntityName = "account";
        public const string ContactEntityName = "contact";
        public const string BusinessUnitEntityName = "businessunit";
        public const string TeamEntityName = "team";
        public const string SystemUserEntityName = "systemuser";
        public const string DepartmentEntityName = "cap_department";
        public const string TransactionCurrencyEntityName = "transactioncurrency";
        public const string PrincipalObjectAttributeAccessEntityName = "principalobjectattributeaccess";
        public const string RoleEntityName = "role";
        public const string ConnectionRoleEntityName = "connectionrole";
        public const string ConnectionEntityName = "connection";
        public const string SystemUserRolesAssociation = "systemuserroles_association";
        public const string NoteEntityName = "annotation";

        public const int TeamEntityTypeCode = 9;
        public const int SystemUserEntityTypeCode = 8;
    }
}
