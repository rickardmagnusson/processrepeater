﻿using Microsoft.Azure;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.Azure.Management.ServiceBus.Fluent;
using Microsoft.Azure.Management.ServiceBus.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.ServiceBus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Text;

namespace Azure.Queues.Consumer
{
    public class NameSpaceManagement
    {
        public readonly string KundeRegister = "kunderegister";
        public readonly string KundeHendelse = "kundehendelse";
        public readonly string SaksHendelse = "sakshendelse";
        private static string connectionString;
        private static NameSpaceManagement _instance;

        private NameSpaceManagement() { }

        private static NameSpaceManagement GetInstance()
        {
            if (_instance == null)
                _instance = new NameSpaceManagement();
            return _instance;
        }


        /// <summary>
        /// Create Topics and subscriptions if there not already exists.
        /// </summary>
        public static void CreateRequiredTopics(IConfiguration configuration)
        {
            connectionString = configuration["SbEndpoint"];
            GetInstance().CreateTopicsIfNotExist();
        }


        private void CreateTopicsIfNotExist()
        {
            try {
                var namespaceManager = NamespaceManager.CreateFromConnectionString(connectionString);

                if (!namespaceManager.TopicExists(KundeRegister))
                    namespaceManager.CreateTopic(KundeRegister);
                if (!namespaceManager.SubscriptionExists(KundeRegister, $"{KundeRegister}subscription"))
                    namespaceManager.CreateSubscription(KundeRegister, $"{KundeRegister}subscription");
             

                if (!namespaceManager.TopicExists(KundeHendelse))
                    namespaceManager.CreateTopic(KundeHendelse);
                if (!namespaceManager.SubscriptionExists(KundeHendelse, $"{KundeHendelse}subscription"))
                    namespaceManager.CreateSubscription(KundeHendelse, $"{KundeHendelse}subscription");
           

                if (!namespaceManager.TopicExists(SaksHendelse))
                    namespaceManager.CreateTopic(SaksHendelse);
                if (!namespaceManager.SubscriptionExists(SaksHendelse, $"{SaksHendelse}subscription"))
                    namespaceManager.CreateSubscription(SaksHendelse, $"{SaksHendelse}subscription");

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
