﻿using Newtonsoft.Json;
using System;
using System.Data;

using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.Console;
using static System.Convert;

using Microsoft.Azure.ServiceBus;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Transactions;
using Microsoft.Azure.Amqp.Framing;
using static Microsoft.Azure.Amqp.Serialization.SerializableType;
using Microsoft.Azure.ServiceBus.Core;

namespace Azure.Queues.Consumer
{
    class Program
    {

        private static HttpClient httpClient;
        private static IQueueClient queueClient;
        private static CloudQueue storageAccountQueue;
        private static IConfiguration Configuration;
        private static ITopicClient topicClient;

        static Program()
        {
            httpClient = new HttpClient();
            var services = new ServiceCollection();

            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .SetBasePath(System.IO.Path.GetFullPath(@"..\..\..\..\"))
                .AddJsonFile("local.settings.json");

            Configuration = builder.Build();

        }


        public static void Main(string[] args)
        {
            bool keepGoing = true;
            try
            {
                do
                {
                    var selection = DisplayMenu();
                    switch (selection)
                    {
                        case 0:
                            WriteLine("Enter Auto read Event Grid.(local.settings.json (Endpoint,EventType,Secret))");
                            MainEventGridAutoTriggerAsync(args).GetAwaiter().GetResult();
                            break;
                        case 1:
                            WriteLine("Enter Event Grid.");
                            MainEventGridTriggerAsync(args).GetAwaiter().GetResult();
                            break;
                        case 2:
                            WriteLine("Enter Service Bus.");
                            MainServiceBusAsync(args).GetAwaiter().GetResult();
                            break;
                        case 3:
                            WriteLine("Enter Service Bus with random topic.(Unreliable)");
                            MainServiceBusTopicAsync(args).GetAwaiter().GetResult();
                            break;
                        case 4:
                            WriteLine("Enter Servicebus with a Topic.");
                            MainServiceBusSingleTopicAsync(args).GetAwaiter().GetResult();
                            break;
                        case 5:
                            WriteLine("Enter Insert data.");
                            MainInsertDataAsync(args).GetAwaiter().GetResult();
                            break;
                        case 6:
                            WriteLine("Bye.");
                            keepGoing = false;
                            break;
                        default:
                            throw new InvalidOperationException("You entered an invalid option. Bye.");
                    }
                } while (keepGoing);
            }
            catch (Exception ex)
            {
                WriteLine($"Exeption occurred: {ex.Message}");
                Console.ReadLine();
            }
        }


        static public int DisplayMenu()
        {
            WriteLine();
            WriteLine(" 0. Auto from App.config Event Grid");
            WriteLine(" 1. Event Grid");
            WriteLine(" 2. Service Bus Queue");
            WriteLine(" 3. Enter Service Bus with random Topics");
            WriteLine(" 4. Service Bus Topic");
            WriteLine(" 5. Insert data");
            WriteLine(" 6. Exit");
            WriteLine(" Which would you like to trigger?  Enter '4' to exit.");
            var result = ReadLine();
            return ToInt32(result);
        }


        /// <summary>
        /// Insert data into a predefined table
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static async Task MainInsertDataAsync(string[] args)
        {
            WriteLine("Enter number of rows to insert: ");
            int Numrows = 0;
            while (!int.TryParse(ReadLine(), out Numrows))
            {
                WriteLine("Try again, this value must be numeric.");
            }

            if (Numrows == 0) { 
                WriteLine("Nothing to execute");
                DisplayMenu();
            }

            var rows = new StringBuilder();
            var rnd = new Random();

            for (int i=0; i < Numrows; i++)
            {
                int eventType = rnd.Next(1, 4);

                rows.AppendLine($"('Message event {i}','{DateTime.UtcNow}', {eventType})");
                if (i + 1 < Numrows)
                    rows.Append(",");
            }

            var connectionstring = Configuration["ConnectionString"];

            using (var conn = new SqlConnection(connectionstring))
            {
                string sql = $"INSERT INTO MsmqInbox (InboxMessage,Created,EventType) VALUES {rows.ToString()} ";

                conn.Open();

                var cmd = new SqlCommand(sql, conn);
                var result = await cmd.ExecuteNonQueryAsync();
                WriteLine($"Executes {result} rows");
                conn.Close();
            }
        }
        public enum EventType
        {
            KundeRegister = 1,
            KundeHendelse = 2,
            SaksHendelse = 3
        }

        [Serializable]
        public class InboxMessage
        {
            public int Id { get; set; }
            public string Message { get; set; }
            public EventType EventType { get; set; }
            public string Type { get; set; }
        }


        #region EventGrid

        private static async Task MainEventGridAutoTriggerAsync(string[] args)
        {
            httpClient = new HttpClient();

            var endpoint = Configuration["Endpoint"];
            var secret = Configuration["Secret"];
            var topic = Configuration["EventType"];

            WriteLine(endpoint);

            WriteLine("Enter number of events to send: ");
            int NumEvents = 0;
            while (!int.TryParse(ReadLine(), out NumEvents))
            {
                WriteLine("Try again, this value must be numeric.");
            }

            WriteLine($"Key: {secret}");
            WriteLine($"HTTPRequestsToSend is: {NumEvents} events.");

            var eventList = new List<GridEvent<object>>();

            for (int x = 0; x < NumEvents; x++)
            {
                var testEvent = new GridEvent<object>
                {
                    Subject = $"Start",
                    EventType = topic,
                    EventTime = DateTime.UtcNow,
                    Id = Guid.NewGuid().ToString(),
                    DataVersion = "1.0"
                };
                eventList.Add(testEvent);
                await Task.Delay(20);
                WriteLine($"Sending request: {eventList[x].Subject}");
            }

            var json = JsonConvert.SerializeObject(eventList);
            WriteLine(json);
            httpClient.DefaultRequestHeaders.Add("aeg-sas-key", secret);
            httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("democlient");

            var request = new HttpRequestMessage(HttpMethod.Post, endpoint)
            {
                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            var result = await httpClient.SendAsync(request);
            WriteLine(result);

        }



        private static async Task MainEventGridTriggerAsync(string[] args)
        {
            httpClient = new HttpClient();

            WriteLine("Enter your Endpoint Trigger URL:");
            var HTTPTriggerUrl = ReadLine();
            while (HTTPTriggerUrl.Length == 0)
            {
                WriteLine("Try again, this value must have a length > 0");
                WriteLine("Enter your HTTP Trigger URL:");
                HTTPTriggerUrl = ReadLine();
            }

            WriteLine("Enter Topic key:");
            var Topic = ReadLine();
            while (Topic.Length == 0)
            {
                WriteLine("Try again, this value must have a length > 0");
                WriteLine("Enter Endpoint Trigger function key:");
                Topic = ReadLine();
            }

            WriteLine("Enter HTTPsasKey:");
            var HTTPSasKey = ReadLine();
            while (HTTPSasKey.Length == 0)
            {
                WriteLine("Try again, this value must have a length greater than 0");
                WriteLine("Enter HTTPsas key:");
                HTTPSasKey = ReadLine();
            }

            if (Regex.IsMatch(HTTPSasKey, @"^[a-zA-Z0-9\+/]*={0,2}$"))
                WriteLine("HTTPSasKey validated.");

            WriteLine("Enter number of events to send: ");
            int HTTPRequestsToSend = 0;
            while (!int.TryParse(ReadLine(), out HTTPRequestsToSend))
            {
                WriteLine("Try again, this value must be numeric.");
            }

            WriteLine($"Key: {HTTPSasKey}");
            WriteLine($"HTTPRequestsToSend is: {HTTPRequestsToSend} events.");

            var eventList = new List<GridEvent<object>>();

            for (int x = 0; x < HTTPRequestsToSend; x++)
            {
                var testEvent = new GridEvent<object>
                {
                    Subject = $"Start",
                    EventType = Topic,
                    EventTime = DateTime.UtcNow,
                    Id = Guid.NewGuid().ToString(),
                    DataVersion = "1.0"
                };
                eventList.Add(testEvent);
                await Task.Delay(20);
                WriteLine($"Sending request: {eventList[x].Subject}");
            }

            var json = JsonConvert.SerializeObject(eventList);
            WriteLine(json);
            httpClient.DefaultRequestHeaders.Add("aeg-sas-key", HTTPSasKey);
            httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("democlient");

            var request = new HttpRequestMessage(HttpMethod.Post, HTTPTriggerUrl)
            {
                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            var result = await httpClient.SendAsync(request);
            WriteLine(result);

        }
        #endregion

      



        #region Service Bus


        private static async Task MainServiceBusSingleTopicAsync(string[] args)
        {
            string ServiceBusConnectionString = Configuration["SbEndpoint"];

            var retryPolicy = new RetryExponential(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(30), 10) { };
            //NameSpaceManagement.CreateRequiredTopics(Configuration);


            if (string.IsNullOrEmpty(ServiceBusConnectionString))
            {
                WriteLine("Enter your Service Bus connection string:");
                ServiceBusConnectionString = ReadLine();
                while (ServiceBusConnectionString.Length == 0)
                {
                    WriteLine("Try again, this value must have a length > 0");
                    WriteLine("Enter your Service Bus connection string:");
                    ServiceBusConnectionString = ReadLine();
                }
            }
            else
            {
                WriteLine($"Using {ServiceBusConnectionString}");
            }

            WriteLine("Enter Topic name:");
            var Topic = ReadLine();
            while (Topic.Length == 0)
            {
                WriteLine("Try again, this value must have a length > 0");
                WriteLine("Enter yourQueue name:");
                Topic = ReadLine();
            }

           
         
            WriteLine("Sending messages to Servicebus with a Topic");

            WriteLine("Enter number of messages to add: ");
            int ServiceBusMessagesToSend = 1;
            while (!int.TryParse(ReadLine(), out ServiceBusMessagesToSend))
            {
                WriteLine("Try again, this value must be numeric.");
            }

            
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };

            var Col = new List<object>();

            object typeWrapper = null;

            for (var i = 1; i <= ServiceBusMessagesToSend; i++)
            {
                string messageBody = $"Message {i}";

                var message = new InboxMessage
                {
                    Message = messageBody,
                    EventType = FromTopic(Topic),
                    Type = Topic
                };


                switch (Topic)
                {
                    case "kunderegister" :
                        typeWrapper = new { KundeRegister = message };
                        break;
                    case "kundehendelse":
                        typeWrapper = new { KundeHendelse = message };

                        break;
                    case "sakshendelse":
                        typeWrapper = new { SaksHendelse = message };
                        break;
                }

                Col.Add(typeWrapper);
                }

            try
            {
                topicClient = new TopicClient(ServiceBusConnectionString, Topic, retryPolicy);

                foreach (var m in Col)
                { 
                    var json = JsonConvert.SerializeObject(m);
                    WriteLine($"{json}");
                    var msg = new Message(Encoding.UTF8.GetBytes(json));
                    msg.ContentType = @"application/json";
                    
                    await topicClient.SendAsync(msg);
                    await Task.Delay(20);
                }

                await topicClient.CloseAsync();


            }
            catch (ServiceBusTimeoutException sbte)
            {
                WriteLine($"ServiceBusTimeoutException: {sbte.Message}");
            }
            catch (ServiceBusException sbe)
            {
                WriteLine($"ServiceBusException: {sbe.Message}");
            }
            catch (Exception ex)
            {
                WriteLine($"{DateTime.Now} > Exception: {ex.Message}");
            }

            WriteLine($"{ServiceBusMessagesToSend} messages sent.");
        }


        private static EventType FromTopic(string topic)
        {
            topic = topic.ToLower();
            switch(topic)
            {
                case "kunderegister":
                    return EventType.KundeRegister;
                case "kundehendelse":
                    return EventType.KundeHendelse;
                default:
                    return EventType.SaksHendelse;
            }
        }




        private static async Task MainServiceBusTopicAsync(string[] args)
        {
            string ServiceBusConnectionString = Configuration["SbEndpoint"];

            NameSpaceManagement.CreateRequiredTopics(Configuration);

            var retryPolicy = new RetryExponential(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(30), 10) { };

            var krClient = new TopicClient(ServiceBusConnectionString, EventType.KundeRegister.ToString().ToLower(), retryPolicy);
            var khClient = new TopicClient(ServiceBusConnectionString, EventType.KundeHendelse.ToString().ToLower(), retryPolicy);
            var shClient = new TopicClient(ServiceBusConnectionString, EventType.SaksHendelse.ToString().ToLower(), retryPolicy);

            if (string.IsNullOrEmpty(ServiceBusConnectionString)) { 
                WriteLine("Enter your Service Bus connection string:");
                ServiceBusConnectionString = ReadLine();
                while (ServiceBusConnectionString.Length == 0)
                {
                    WriteLine("Try again, this value must have a length > 0");
                    WriteLine("Enter your Service Bus connection string:");
                    ServiceBusConnectionString = ReadLine();
                }
            }
            else
            {
                WriteLine($"Using {ServiceBusConnectionString}");
            }

            WriteLine("Sending random eventtype messages to Servicebus");

            WriteLine("Enter number of messages to add: ");
            int ServiceBusMessagesToSend = 0;
            while (!int.TryParse(ReadLine(), out ServiceBusMessagesToSend))
            {
                WriteLine("Try again, this value must be numeric.");
            }

            try
            {
                JsonConvert.DefaultSettings = () => new JsonSerializerSettings
                {
                    Formatting = Formatting.Indented
                };

                object typeWrapper = null;



                for (var i = 0; i < ServiceBusMessagesToSend; i++)
                {
                    string messageBody = $"Message {i}";
                    var rnd = new Random();

                    var eventType = (EventType)rnd.Next(1, 4);

                    //Allow us to send random event types
                    string topic = eventType.ToString().ToLower();
                    WriteLine($"Topic: {topic}");

                    //topicClient = new TopicClient(ServiceBusConnectionString, topic);

                    var sType = eventType.ToString();
                    var message = new InboxMessage {
                        Message = messageBody,
                        EventType = eventType,
                        Type = sType
                    };

                    switch (eventType) {
                        case EventType.KundeRegister:
                            typeWrapper = new { KundeRegister = message };
                            var json = JsonConvert.SerializeObject(typeWrapper);
                            var m = new Message(Encoding.UTF8.GetBytes(json));
                            WriteLine($"{json}");
                            await krClient.SendAsync(m);
                            break;
                        case EventType.KundeHendelse:
                            typeWrapper = new { KundeHendelse = message };
                            json = JsonConvert.SerializeObject(typeWrapper);
                            m = new Message(Encoding.UTF8.GetBytes(json));
                            WriteLine($"{json}");
                            await khClient.SendAsync(m);
                            break;
                        case EventType.SaksHendelse:
                            typeWrapper = new { SaksHendelse = message };
                            json = JsonConvert.SerializeObject(typeWrapper);
                            m = new Message(Encoding.UTF8.GetBytes(json));
                            WriteLine($"{json}");
                            await shClient.SendAsync(m);
                            break;
                    }
                }
            }
            catch (ServiceBusTimeoutException sbte)
            {
                WriteLine($"ServiceBusTimeoutException: {sbte.Message}");
            }
            catch (ServiceBusException sbe)
            {
                WriteLine($"ServiceBusException: {sbe.Message}");
            }
            catch (Exception ex)
            {
                WriteLine($"{DateTime.Now} > Exception: {ex.Message}");
            }

            WriteLine($"{ServiceBusMessagesToSend} messages sent.");
        }

        private static void PrintJsonObject(string json)
        {
            var o = (InboxMessage)JsonConvert.DeserializeObject(json, typeof(InboxMessage));
            o.Message = JsonConvert.DeserializeXmlNode(json).OuterXml;
            WriteLine(o.Message);
        }



        private static async Task MainServiceBusAsync(string[] args)
        {
            string ServiceBusConnectionString = Configuration["SbEndpoint"];
            if (string.IsNullOrEmpty(ServiceBusConnectionString))
            { 
                WriteLine("Enter your Service Bus connection string:");

                ServiceBusConnectionString = ReadLine();
                while (ServiceBusConnectionString.Length == 0)
                {
                    WriteLine("Try again, this value must have a length > 0");
                    WriteLine("Enter your Service Bus connection string:");
                    ServiceBusConnectionString = ReadLine();
                }
            }

            WriteLine($"Using: {ServiceBusConnectionString}");

            WriteLine("Enter your Queue name:");
            var QueueName = ReadLine();
            while (QueueName.Length == 0)
            {
                WriteLine("Try again, this value must have a length > 0");
                WriteLine("Enter yourQueue name:");
                QueueName = ReadLine();
            }

            WriteLine("Enter number of messages to add: ");
            int ServiceBusMessagesToSend = 0;
            while (!int.TryParse(ReadLine(), out ServiceBusMessagesToSend))
            {
                WriteLine("Try again, this value must be numeric.");
            }

            queueClient = new QueueClient(ServiceBusConnectionString, QueueName);

            await SendMessagesToServicebus(ServiceBusMessagesToSend);
            await queueClient.CloseAsync();
        }


        private static async Task SendMessagesToServicebus(int numMessagesToSend)
        {
            for (var i = 0; i < numMessagesToSend; i++)
            {
                try
                {
                    var messageBody = $"Message-{i}-{Guid.NewGuid().ToString("N")}-{DateTime.Now.Minute}";
                    WriteLine($"Sending message: {messageBody}");
                    var message = new Message(Encoding.UTF8.GetBytes(messageBody));
                    await queueClient.SendAsync(message);
                }
                catch (ServiceBusTimeoutException sbte)
                {
                    WriteLine($"ServiceBusTimeoutException: {sbte.Message}");
                }
                catch (ServiceBusException sbe)
                {
                    WriteLine($"ServiceBusException: {sbe.Message}");
                }
                catch (Exception ex)
                {
                    WriteLine($"{DateTime.Now} > Exception: {ex.Message}");
                }

                await Task.Delay(10);
            }

            WriteLine($"{numMessagesToSend} messages sent.");
        }




        #endregion

        #region Azure Queue
        private static async Task MainStorageQueueAsync(string[] args)
        {
            WriteLine("Enter your Storage Queue connection string:");
            var StorageQueueConnectionString = ReadLine();
            while (StorageQueueConnectionString.Length == 0)
            {
                WriteLine("Try again, this value must have a length > 0");
                WriteLine("Enter your Storage Queue connection string:");
                StorageQueueConnectionString = ReadLine();
            }
            WriteLine("Enter your Storage Queue name:");
            var StorageQueueName = ReadLine();
            while (StorageQueueName.Length == 0)
            {
                WriteLine("Try again, this value must have a length > 0");
                WriteLine("Enter your Storage Queue name:");
                StorageQueueName = ReadLine();
            }
            WriteLine("Enter number of messages to add: ");
            int StorageMessagesToSend = 0;
            while (!int.TryParse(ReadLine(), out StorageMessagesToSend))
            {
                WriteLine("Try again, this value must be numeric.");
            }
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(StorageQueueConnectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            storageAccountQueue = queueClient.GetQueueReference(StorageQueueName);
            if (await storageAccountQueue.CreateIfNotExistsAsync())
            {
                WriteLine($"Queue '{storageAccountQueue.Name}' Created.");
            }
            else
            {
                WriteLine($"Queue '{storageAccountQueue.Name}' Exists.");
            }
            await SendMessagesToStorageAccount(StorageMessagesToSend);
        }
        private static async Task SendMessagesToStorageAccount(int numMessagesToSend)
        {
            for (var i = 0; i < numMessagesToSend; i++)
            {
                try
                {
                    var message = $"Message {i}";
                    WriteLine($"Sending message: {message}");
                    await storageAccountQueue.AddMessageAsync(new CloudQueueMessage(message));
                }
                catch (StorageException se)
                {
                    WriteLine($"StorageException: {se.Message}");
                }
                catch (Exception ex)
                {
                    WriteLine($"{DateTime.Now} > Exception: {ex.Message}");
                }

                await Task.Delay(10);
            }

            WriteLine($"{numMessagesToSend} messages sent.");
        }
        #endregion
    }
}
