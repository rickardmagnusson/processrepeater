﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Azure.Queues.Consumer
{
    public class GridEvent<T> where T : class
    {
        [JsonProperty(PropertyName = "dataVersion")]
        public string DataVersion { get; set; }

        [JsonProperty(PropertyName = "subject")]
        public string Subject { get; set; }

        [JsonProperty(PropertyName = "eventType")]
        public string EventType { get; set; }

        [JsonProperty(PropertyName = "eventTime")]
        public DateTime EventTime { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}
