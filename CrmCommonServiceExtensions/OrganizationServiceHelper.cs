﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.WebServiceClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crm.Services.Extensions
{
    public class OrganizationServiceHelper
    {

        /// <summary>
        /// Creates an IOrganizationService connection.
        /// </summary>
        /// <returns>IOrganizationService. Return null if no instance was made (Check the application settings if so).</returns>
        public static IOrganizationService GetIOrganizationService()
        {
            return GetInstance().CreateCrmOrganizationService();
        }


        #region Private Methods


        private static OrganizationServiceHelper _instance;


        private OrganizationServiceHelper() { }


        private static OrganizationServiceHelper GetInstance()
        {
            if (_instance == null)
                _instance = new OrganizationServiceHelper();
            return _instance;
        }


        private IOrganizationService CreateCrmOrganizationService()
        {
            try
            {
                var _accessToken = TokenHelper.GetToken();
                var _crmclient = new OrganizationWebProxyClient(new Uri(CreateOrganizationUri()), false);
                _crmclient.HeaderToken = _accessToken;
                return _crmclient;
            }
            catch
            {
                return null;
            }
        }


        private string CreateOrganizationUri()
        {
            string _crmUrl = ConfigurationManager.AppSettings["ida:ResourceToCRM"];
            string _tempcrmServiceUrl = "XRMServices/2011/Organization.svc";
            string _sdk_version = "9.0";
            string _resultUrl = $"{_crmUrl}/{_tempcrmServiceUrl}/web?SdkClientVersion={_sdk_version}";

            return _resultUrl;
        }

        #endregion
    }
}
